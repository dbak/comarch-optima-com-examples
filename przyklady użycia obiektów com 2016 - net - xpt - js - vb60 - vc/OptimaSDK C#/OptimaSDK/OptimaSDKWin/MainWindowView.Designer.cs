﻿namespace OptimaSDKWin
{
	partial class MainWindowView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.uxRWButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// uxRWButton
			// 
			this.uxRWButton.Location = new System.Drawing.Point(38, 12);
			this.uxRWButton.Name = "uxRWButton";
			this.uxRWButton.Size = new System.Drawing.Size(181, 23);
			this.uxRWButton.TabIndex = 0;
			this.uxRWButton.Text = "RW";
			this.uxRWButton.UseVisualStyleBackColor = true;
			this.uxRWButton.Click += new System.EventHandler(this.RWButton_Click);
			// 
			// MainWindowView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(681, 262);
			this.Controls.Add(this.uxRWButton);
			this.Name = "MainWindowView";
			this.Text = "Opt!ma SDK";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button uxRWButton;
	}
}

