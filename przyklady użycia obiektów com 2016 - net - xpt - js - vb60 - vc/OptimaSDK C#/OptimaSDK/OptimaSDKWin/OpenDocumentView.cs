﻿using System;
using Optima.Common;
using Optima.Common.Logic;
using Optima.Common.View.WinForms;
using OptimaSDKWin.Base;

namespace OptimaSDKWin
{
	class OpenDocumentView
	{
		public void OpenDocument(int documentClass, int documentId)
		{
			var login = new Login();
			login.LogIn();
			try
			{
				OpenView(documentClass, documentId);
			}
			finally
			{

				login.LogOut();
			}
		}

		private void OpenView(int documentClass, int documentId)
		{
			var optima = OptimaContext.Instance;
			var sessionFa = optima.OptimaAccess.CreateSession();
			Optima.Common.View.ViewId viewId;
			var item = new Optima.Common.View.Common.CreateObjectService(sessionFa).Document(documentClass, documentId, out viewId);
			var show = new Optima.Slowniki.View.WinForms.Common.ShowEditFormHelper(sessionFa);
			var context = new ViewContext();
			show.ShowEditForm(viewId, item, ActionMode.ChangeRecord, context);
			GC.KeepAlive(sessionFa);

		}


	}
}
