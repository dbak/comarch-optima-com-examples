// ECRTaxRate_NazwaSterownika.cpp : Implementation of CECRTaxRate_NazwaSterownika

#include "stdafx.h"
#include "ECRTaxRate_NazwaSterownika.h"


// CECRTaxRate_NazwaSterownika

STDMETHODIMP CECRTaxRate_NazwaSterownika::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IECRTaxRate_NazwaSterownika
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CECRTaxRate_NazwaSterownika::get_Id(int *){return S_OK;}
STDMETHODIMP CECRTaxRate_NazwaSterownika::put_Id(int){return S_OK;}
STDMETHODIMP CECRTaxRate_NazwaSterownika::get_Rate(int *){return S_OK;}
STDMETHODIMP CECRTaxRate_NazwaSterownika::put_Rate(int){return S_OK;}
STDMETHODIMP CECRTaxRate_NazwaSterownika::get_Type(TaxRateType *){return S_OK;}
STDMETHODIMP CECRTaxRate_NazwaSterownika::put_Type(TaxRateType){return S_OK;}
