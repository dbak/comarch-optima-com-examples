//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ECR_NazwaSterownika.rc
//
#define IDS_PROJNAME                    100
#define IDR_ECR_NazwaSterownika         101
#define IDR_ECRDriver_NazwaSterownika   102
#define IDR_ECRTaxRate_NazwaSterownika  103
#define IDR_ECRItem_NazwaSterownika     104
#define IDD_CONFIGDIALOG                105
#define IDC_EDIT1                       203
#define IDC_REMOTE                      212
#define IDC_BITRATE                     213

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         204
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
