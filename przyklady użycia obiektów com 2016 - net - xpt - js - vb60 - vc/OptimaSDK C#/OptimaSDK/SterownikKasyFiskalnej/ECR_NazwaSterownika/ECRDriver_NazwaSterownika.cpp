// ECRDriver_NazwaSterownika.cpp : Implementation of CECRDriver_NazwaSterownika

#include "stdafx.h"
#include "ECRDriver_NazwaSterownika.h"
#include "ConfigDialog.h"
#include <string>
#include <comutil.h>
#include "Exception.h"
#include <vector>
#include <WtsApi32.h>
#include <math.h>
#include <time.h>

using namespace std;


STDMETHODIMP CECRDriver_NazwaSterownika::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IECRDriver_NazwaSterownika,
        &IID_IECRDriver
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

CECRDriver_NazwaSterownika::CECRDriver_NazwaSterownika()
{
    put_Config(_bstr_t(L"bitrate=9600").GetBSTR());
}

CECRDriver_NazwaSterownika::~CECRDriver_NazwaSterownika()
{
}


HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::clear(void)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::displayConfigDialog(void)
{
    CConfigDialog dialog;
    dialog.setConfig(&configMap);
    dialog.DoModal();
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::get_Config(BSTR *pVal)
{
    wstring cfg;
    for(map<wstring, wstring>::iterator it=configMap.begin();it!=configMap.end();it++)
        cfg+=it->first+L"="+it->second+L",";
    cfg=cfg.substr(0, cfg.length()-1);
    *pVal=SysAllocString(_bstr_t(cfg.c_str()).GetBSTR());
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::put_Config(BSTR val)
{
    wstring config(_bstr_t(val, true));

    for(size_t pos=0, lastComma=0;pos!=wstring::npos;lastComma=pos+1)
    {
        pos=config.find(L",", lastComma);
        wstring part=config.substr(lastComma, pos-lastComma);
        configMap[part.substr(0, part.find(L"="))]=part.substr(part.find(L"=")+1);
    }
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::put_Port(BSTR val)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::get_PrettyConfig(BSTR *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::put_Id(int){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::downloadTaxRates(void){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::getTaxRate(int,IECRTaxRate **){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::setTaxRate(IECRTaxRate *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::uploadTaxRates(void){return S_OK;}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::downloadAllItems(void)
{
    return S_OK;
}

HRESULT CECRDriver_NazwaSterownika::importItems(ItemType itype)
{
    return S_OK;
}

HRESULT CECRDriver_NazwaSterownika::importSales(ItemType itype)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::downloadItemsByType(ItemType itype)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::getItem(BSTR uid,IECRItem **item)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::setItem(IECRItem *item)
{
    return S_OK;
}


HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::uploadItemsByType(ItemType itype)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::uploadAllItems(void)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::get_ItemNumbers(SAFEARRAY **arr)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::createDailyReport(void)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::readParameters(void)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::getNumber(ItemType,int *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::getMaxId(ItemType,int *){return S_OK;}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::getNameLength(ItemType,int *pVal)
{
    return S_OK; 
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::get_Date(unsigned long* pVal)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRDriver_NazwaSterownika::supportsFeature(FeatureType ft, int* pVal)
{
    return S_OK; 
}
