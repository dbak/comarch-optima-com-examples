// ECRItem_NazwaSterownika.h : Declaration of the CECRItem_NazwaSterownika

#pragma once
#include "resource.h"       // main symbols

#include "ECR_NazwaSterownika.h"
#include <comutil.h>
#include <vector>
#include <string>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CECRItem_NazwaSterownika

class ATL_NO_VTABLE CECRItem_NazwaSterownika :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CECRItem_NazwaSterownika, &CLSID_ECRItem_NazwaSterownika>,
	public ISupportErrorInfo,
	public IDispatchImpl<IECRItem_NazwaSterownika, &IID_IECRItem_NazwaSterownika, &LIBID_ECR_NazwaSterownikaLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CECRItem_NazwaSterownika();

DECLARE_REGISTRY_RESOURCEID(IDR_ECRItem_NazwaSterownika)


BEGIN_COM_MAP(CECRItem_NazwaSterownika)
	COM_INTERFACE_ENTRY(IECRItem)
	COM_INTERFACE_ENTRY(IECRItem_NazwaSterownika)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
    HRESULT STDMETHODCALLTYPE get_Id(BSTR *pVal);
    HRESULT STDMETHODCALLTYPE put_Id(BSTR val);
    HRESULT STDMETHODCALLTYPE get_Name(BSTR *pVal);
    HRESULT STDMETHODCALLTYPE put_Name(BSTR val);
    HRESULT STDMETHODCALLTYPE get_Price(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Price(int val);
    HRESULT STDMETHODCALLTYPE get_ManualPrice(boolean *);
    HRESULT STDMETHODCALLTYPE put_ManualPrice(boolean);
    HRESULT STDMETHODCALLTYPE get_ProgrammedPrice(boolean *);
    HRESULT STDMETHODCALLTYPE put_ProgrammedPrice(boolean);
    HRESULT STDMETHODCALLTYPE get_Group(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Group(int val);
    HRESULT STDMETHODCALLTYPE get_Items(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Items(int val);
    HRESULT STDMETHODCALLTYPE get_Amount(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Amount(int val);
    HRESULT STDMETHODCALLTYPE get_Value(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Value(int val);
    HRESULT STDMETHODCALLTYPE get_Type(ItemType *pVal);
    HRESULT STDMETHODCALLTYPE put_Type(ItemType val);
    HRESULT STDMETHODCALLTYPE get_SaleMode(SaleType *);
    HRESULT STDMETHODCALLTYPE put_SaleMode(SaleType);
    HRESULT STDMETHODCALLTYPE get_Vat(int *pVal);
    HRESULT STDMETHODCALLTYPE put_Vat(int val);
    HRESULT STDMETHODCALLTYPE get_Ean(BSTR *pVal);
    HRESULT STDMETHODCALLTYPE put_Ean(BSTR val);
    HRESULT STDMETHODCALLTYPE put_Driver(IECRDriver* val);
    HRESULT STDMETHODCALLTYPE get_Text(BSTR* pVal);
    HRESULT STDMETHODCALLTYPE put_Text(BSTR val);

private:
    _bstr_t id, name, ean;
    ItemType type;
    int price, items, group, amount, value, vat;
    IECRDriver* driver;
    std::vector<std::wstring> fields(std::wstring data);
    std::wstring sep;
};

OBJECT_ENTRY_AUTO(__uuidof(ECRItem_NazwaSterownika), CECRItem_NazwaSterownika)
