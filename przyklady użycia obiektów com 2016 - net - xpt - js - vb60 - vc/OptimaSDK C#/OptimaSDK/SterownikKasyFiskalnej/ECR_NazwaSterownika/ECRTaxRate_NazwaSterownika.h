// ECRTaxRate_NazwaSterownika.h : Declaration of the CECRTaxRate_NazwaSterownika

#pragma once
#include "resource.h"       // main symbols

#include "ECR_NazwaSterownika.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CECRTaxRate_NazwaSterownika

class ATL_NO_VTABLE CECRTaxRate_NazwaSterownika :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CECRTaxRate_NazwaSterownika, &CLSID_ECRTaxRate_NazwaSterownika>,
	public ISupportErrorInfo,
	public IECRTaxRate_NazwaSterownika
{
public:
	CECRTaxRate_NazwaSterownika()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ECRTaxRate_NazwaSterownika)


BEGIN_COM_MAP(CECRTaxRate_NazwaSterownika)
	COM_INTERFACE_ENTRY(IECRTaxRate)
	COM_INTERFACE_ENTRY(IECRTaxRate_NazwaSterownika)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
    STDMETHODIMP get_Id(int *);
    STDMETHODIMP put_Id(int);
    STDMETHODIMP get_Rate(int *);
    STDMETHODIMP put_Rate(int);
    STDMETHODIMP get_Type(TaxRateType *);
    STDMETHODIMP put_Type(TaxRateType);
};

OBJECT_ENTRY_AUTO(__uuidof(ECRTaxRate_NazwaSterownika), CECRTaxRate_NazwaSterownika)
