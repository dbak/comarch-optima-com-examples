﻿using System;
using System.Runtime.InteropServices;
using CDNBase;
using CDNSrtb;

namespace OptimaSDKLib.SrodkiTrwale
{
	public class UlepszenieST
	{
		public UlepszenieST()
		{
		}

        public void DodajUL(ILogin _rLogin)
		{
			try
			{
                IAdoSession Sesja = _rLogin.CreateSession();
				var nazwaSrodkaTrwalego = "Zestaw komputerowy";
				var st = (SrodekTrwaly)(Sesja.CreateObject("CDN.SrodkiTrwale", string.Format("SrT_Nazwa = '{0}'", nazwaSrodkaTrwalego)));
				var collDokST = (IAdoCollection)Sesja.CreateObject("CDN.DokumentyST", null);
				var dokST = (IDokumentST)(collDokST.AddNew(null));
				dokST.SrodekTrwaly = st;
				dokST.Grupa = st.Grupa;
				dokST.Rodzaj = st.Rodzaj;
				dokST.DataOpe = DateTime.Today;
				dokST.TypDokumentu = (int)(SrodekTrwalyDokumentTyp.ZmianaWartosci);
				dokST.KwotaBilan = 100;
				dokST.KwotaKoszt = 100;

				Sesja.Save();
                Console.WriteLine("Dodano ulepszenie");
			}
			catch (COMException comError)
			{
                Console.WriteLine("Dodanie ulepszenia nie powiodło się! {0}", comError);
			}
		}
	}
}
