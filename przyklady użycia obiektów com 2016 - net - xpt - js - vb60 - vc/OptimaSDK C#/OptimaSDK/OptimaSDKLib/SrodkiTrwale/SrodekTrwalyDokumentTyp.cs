﻿namespace OptimaSDKLib.SrodkiTrwale
{
	public enum SrodekTrwalyDokumentTyp
	{
		Brak = 0,
		Otrzymanie = 4,
		ZmianaWartosci = 1,
		Amortyzacja = 3,
		Przeszacowanie = 2,
		Likwidacja = 5
	}
}
