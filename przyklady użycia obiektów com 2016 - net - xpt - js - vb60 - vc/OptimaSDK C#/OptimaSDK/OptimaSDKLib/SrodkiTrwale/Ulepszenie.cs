﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using CDNBase;
using CDNSrtb;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.SrodkiTrwale
{
	public class Ulepszenie : OptimaObjBase
	{
		public Ulepszenie(ILogin login) : base(login)
		{
		}

		public void UlepszenieSTNaPodstawieZleceniaSerwisowego(int zlecenieId)
		{
			try
			{
				ShowInfo("##### ulepszenie środka trwałego #####");
				ShowInfo("Kreuję kolekcję zleceń serwisowych ...");
				var zlecenia = (OP_CSRSLib.SrsZlecenia)Sesja.CreateObject("CDN.SrsZlecenia", null);
				ShowInfo("I pobieram z niej obiekt zlecenia o zadanym Id");
				var zlecenie = (OP_CSRSLib.ISrsZlecenie)zlecenia[String.Format("SrZ_SrZId={0}", zlecenieId)];
				var urzadzenie = zlecenie.SrsUrzadzenie;
				if( urzadzenie != null )
				{
					var nazwaSrodkaTrwalego = urzadzenie.Kod;
					var st = (SrodekTrwaly)(Sesja.CreateObject("CDN.SrodkiTrwale", string.Format("SrT_Nazwa = '{0}'", nazwaSrodkaTrwalego)));
					var collDokST = (IAdoCollection)Sesja.CreateObject("CDN.DokumentyST", null);
					var dokST = (IDokumentST)(collDokST.AddNew(null));
					dokST.SrodekTrwaly = st;
					dokST.Grupa = st.Grupa;
					dokST.Rodzaj = st.Rodzaj;
					dokST.DataOpe = DateTime.Today;
					dokST.TypDokumentu = (int)(SrodekTrwalyDokumentTyp.ZmianaWartosci);
					dokST.KwotaBilan = zlecenie.WartoscNetto;
					dokST.KwotaKoszt = zlecenie.WartoscNetto;
				}

				ShowInfo("Zapisuję sesję...");
				Sesja.Save();
				ShowInfo(string.Format("Dodano ulepszenie do ST"));
			}
			catch (COMException comError)
			{
				ShowError("Dodanie ulepszenia nie powiodło się!", comError);
			}
		}

	}
}
