﻿using System;
using System.Text;
using CDNBase;

namespace OptimaSDKLib.Utils
{
	public class GetValue
	{
		public static int GetTowarID(IAdoSession sesja, string kod)
		{
			if (String.IsNullOrEmpty(kod)) return 0;
		    return GetInt(sesja, 
                          string.Format("Select IsNull(Twr_TwrId, 0) From cdn.Towary Where Twr_Kod = '{0}'", kod),
		                  false);
		}

		public static int GetInt(IAdoSession sesja, string query, bool configCnn)
		{
            object value = GetSingleValue(sesja, query, configCnn).ToString();
            return Convert.ToInt32(value);
        }

	    public static object GetSingleValue(IAdoSession sesja, string query, bool configCnn)
		{
			ADODB.Connection cn = configCnn ? sesja.ConfigConnection : sesja.Connection;
			ADODB.Recordset rs = new ADODB.RecordsetClass();
			rs.Open(query, cn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);
			return rs.RecordCount > 0 ? rs.Fields[0].Value : null;
		}



	}
}
