﻿using System;
using System.Runtime.InteropServices;
using CDNBase;
using OP_DBIMPLib;
using OptimaSDKLib.Base;
using OptimaSDKLib.Logistyka;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Handel
{
	public class Faktura: OptimaObjBase
	{
		public Faktura(ILogin login)
			: base(login)
		{
		}

		//Dodanie faktury sprzedaży
		public int Dodaj()
		{

			var faktury = (CDNHlmn.DokumentyHaMag)Sesja.CreateObject("CDN.DokumentyHaMag", null);
			var faktura = (CDNHlmn.IDokumentHaMag)faktury.AddNew(null);

			var kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
			var kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];

			var formyPlatnosci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.FormyPlatnosci", null));
			var FPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci[1];

			// e_op_Rdz_FS 			302000
			faktura.Rodzaj = 302000;
			// e_op_KlasaFS			302
			faktura.TypDokumentu = 302;

			//Ustawiamy bufor
			faktura.Bufor = 0;

			//Ustawiamy date
			faktura.DataDok = new DateTime(2007, 06, 04);

			//Ustawiamy formę póatności
			faktura.FormaPlatnosci = FPl;

			//Ustawiamy podmiot
			faktura.Podmiot = (CDNHeal.IPodmiot)kontrahent;

			//Ustawiamy magazyn
			faktura.MagazynZrodlowyID = 1;

			//Dodajemy pozycje
			CDNBase.ICollection pozycje = faktura.Elementy;
			var pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);
			pozycja.TowarKod = "PROJ_ZIELENI";
			pozycja.Ilosc = 2;
			//Pozycja.Cena0WD = 10;
			pozycja.WartoscNetto = 123.13m;
			//zapisujemy
			Sesja.Save();

			Console.WriteLine("Dodano fakturę: {0}", faktura.NumerPelny);
		    return faktura.ID;
		}

		//Korekta FA
		public void DogenerujKorekteFA()
		{

			var faktury = (CDNHlmn.DokumentyHaMag)Sesja.CreateObject("CDN.DokumentyHaMag", null);
			var faktura = (CDNHlmn.IDokumentHaMag)faktury.AddNew(null);

			// FAKI 302001; 
			// FAKW 302002; 
			// FAKV 302003;

			//ustawiamy rodzaj i typ dokumentu
			faktura.Rodzaj = 302001;
			faktura.TypDokumentu = 302;

			//Rodzaj korekty
			faktura.Korekta = 1;
			//Bufor
			faktura.Bufor = 1;
			//Id korygowanej faktury
			faktura.OryginalID = 1;

			//Zmieniamy ilosc
			CDNBase.ICollection pozycje = faktura.Elementy;
			var pozycja = (CDNHlmn.IElementHaMag)pozycje["Tre_TwrKod='NOWY_C#'"];
			pozycja.Ilosc = 1;

			//Zapisujemy
			Sesja.Save();

			Console.WriteLine("Dogenerowano korektę: " + faktura.NumerPelny);
		}
		//Dogenerowanie WZ do FA
		public void Dogenerowanie_WZ_Do_FA()
		{

			ADODB.Connection cnn = Sesja.Connection;
			ADODB.Recordset rRs = new ADODB.RecordsetClass();
			const string select = "select top 1 TrN_TrNId as ID from cdn.TraNag where TrN_Rodzaj = 302000 ";
			rRs.Open(select, cnn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);

			// Wykreowanie obiektu serwisu:
			var serwis = (CDNHlmn.SerwisHaMag)Sesja.CreateObject("Cdn.SerwisHaMag", null);

			// Samo gemerowanie dok. FA
			const int magazynID = 1;
			serwis.AgregujDokumenty(302, rRs, magazynID, 306, null);

			//Zapisujemy
			Sesja.Save();
		}


		public void ImportujXML(string fileName, string transformataFileName)
		{

			//const string fileName = @"d:\Optima\Support\FA_1_2012.xml";
			//const string transformataFileName = @"d:\Optima\Ecod\EcodZam2OptimaRO.xsl";
			try
			{
				Console.WriteLine("##### importowanie dokumentu #####");
				var expXml = (IDokumentHaMagXml)Sesja.CreateObject("DBImp.DokumentHaMagXml", null);
				expXml.Login = Sesja.Login;
				expXml.CzytajXml(fileName, transformataFileName);
				const string query = "select top 1 TrN_NumerPelny from cdn.TraNag WHERE TrN_TrNId = (select MAX(TrN_TrNId) from cdn.TraNag where TrN_Rodzaj = 308000)";
				var numerDok = GetValue.GetSingleValue(Sesja, query, false);
				Console.WriteLine("##### dodano dokument: " + numerDok + " #####");
			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodanie dokumentu  nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}



		}

       //Dodanie faktury sprzedaży
        public void DodajFaPotemWyslijEMail()
        {
            int nowaFaId = Dodaj();
            var faktury = (CDNHlmn.DokumentyHaMag)Sesja.CreateObject("CDN.DokumentyHaMag", null);
            var faktura = (CDNHlmn.IDokumentHaMag)faktury["TrN_TrNId=" + nowaFaId];


            string adresaci = "biuro@mojaFirma.pl;marketing@mojaFirma.pl";
            string tytul = "Faktura numer: " + faktura.NumerPelny;
            string tresc = "Informujemy, że dnia: " + faktura.DataDok.ToShortDateString() +
                           " została wystawiona faktura dla kontrahenta: " + faktura.PodNazwa1;
            tresc += "\n" + faktura.Opis;

            new EMail(Sesja.Login).Utworz(adresaci, tytul, tresc);
        }

	}
}
