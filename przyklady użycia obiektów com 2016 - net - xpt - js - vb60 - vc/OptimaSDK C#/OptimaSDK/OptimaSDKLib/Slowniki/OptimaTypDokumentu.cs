﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OptimaSDKLib.Slowniki
{
	public enum OptimaTypDokumentu
	{
		FakturaZakupu = 301,
		FakturaSprzedazy = 302,
		PrzyjecieWewnetrzne = 303,
		RozchodWewnwtrzny = 304,
		Paragon = 305,
		WydanieZewnetrzne = 306,
		PrzyjecieZewnetrzne = 307,
		RezerwacjaOdbiorcy = 308,
		ZamowienieUDostawcy = 309,
		BilansOtwarcia = 310,
		PodkladkaInwentaryzacyjna = 311,
		PrzesumiecieMM = 312,
		PrzyjecieKaucji = 313,
		WydanieKaucji = 314,
		PrzyjecieWewnetrzneProduktow = 317,
		RozchodWewnetrznySkladnikow = 318,
		FakturaProForma = 320,
		FakturaWewnetrznaZakupu = 321,
		FakturaWewnetrznaSprzedazy = 322,
		TaxFree = 345,
		FakturaRolnikaRyczaltowego = 350,

		RejestrVat = 999,
		ZapisyKasowe = 1000,
		Preliminarz = 1001,
		Ponaglenie = 1002,
		EwidencjaDodatkowa = 1003,
		SrodkiTrwale = 1004,
		Wyposazenie = 1005,
		PolecenieKsiegowania = 1006,
		DokumentSrodkaTrwalego = 1007,
		ZapisKPiR = 1008,
		ZapisEwidencjiRyczaltowej = 1009,

	}

}
