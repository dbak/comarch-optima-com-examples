﻿using System;
using CDNBase;
using OptimaSDKLib.Base;

namespace OptimaSDKLib.Slowniki
{
	public class Kontrahenci : OptimaObjBase
	{
		public Kontrahenci(ILogin login) : base(login)
		{
		}

		public void Dodaj()
		{

			var banki = (OP_KASBOLib.Banki)Sesja.CreateObject("CDN.Banki", null);
			var kategorie = (CDNHeal.Kategorie)Sesja.CreateObject("CDN.Kategorie", null);

			var kontrahenci = (CDNHeal.Kontrahenci)Sesja.CreateObject("CDN.Kontrahenci", null);
			var kontrahent = (CDNHeal.IKontrahent)kontrahenci.AddNew(null);

			CDNHeal.IAdres adres = kontrahent.Adres;
			CDNHeal.INumerNIP numerNIP = kontrahent.NumerNIP;

			adres.Gmina = "Kozia Wólka";
			adres.KodPocztowy = "20-835";
			adres.Kraj = "Polska";
			adres.NrDomu = "17";
			adres.Miasto = "Kozia Wólka";
			adres.Powiat = "Lukrecjowo";
			adres.Ulica = "Śliczna";
			adres.Wojewodztwo = "Kujawsko-Pomorskie";

			numerNIP.UstawNIP("PL", "281-949-89-45", 1);

			kontrahent.Akronim = "NOWY";
			kontrahent.Email = "nowy@nowy.eu";
			kontrahent.Medialny = 0;
			kontrahent.Nazwa1 = "Nowy kontrahent";
			kontrahent.Nazwa2 = "dodany z C#";
			string nazwa = kontrahent.NazwaPelna;

			kontrahent.Bank = (OP_KASBOLib.Bank)banki["BNa_BNaID=2"];
			kontrahent.Kategoria = (CDNHeal.Kategoria)kategorie["Kat_KodSzczegol='ENERGIA'"];

			Sesja.Save();

			Console.WriteLine("Dodano kontrahenta: {0}", nazwa);
		}

	}
}
