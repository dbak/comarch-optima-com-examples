﻿using System;
using System.Runtime.InteropServices;
using CDNBase;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;
using OP_KASBOLib;
using CDNHeal;

namespace OptimaSDKLib.KasaBank
{
	public class ZapisKB: OptimaObjBase
	{
        public ZapisKB(ILogin login)
			: base(login)
		{
		}

		    //Dodanie zapisu KB
		public void Dodaj()
		{
                //Wykreowanie kolekcji serwerowej zapisów KB
			ZapisyKB zapisyKB = (ZapisyKB)(Sesja.CreateObject("CDN.ZapisyKB", null));
                //Dodanie nowego zapisu KB do kolekcji serwerowej
            IZapisKB zapisKB = (IZapisKB)(zapisyKB.AddNew(null));

                //Ustawienie rejestru KASA
            IRachunek rejestrKB = (IRachunek)(Sesja.CreateObject("CDN.Rachunki", "BRa_BRaID = 1")); //rejestr KASA
            zapisKB.Rachunek = (Rachunek)rejestrKB;

                //Ustawienie raportu
            IRaportKB raportKB = (IRaportKB)(Sesja.CreateObject("CDN.RaportyKB", "BRp_BRpID = 13"));
            zapisKB.RaportKB = (RaportKB)raportKB;

                //Ustawienie definicji dokumentu KW
            DefinicjaDokumentu defDok = (DefinicjaDokumentu)(Sesja.CreateObject("CDN.DefinicjeDokumentow", "DDf_Symbol = 'KW'"));
            zapisKB.DefinicjaDokumentu = (DefinicjaDokumentu)defDok;

                //Ustawienie kontrahenta ALOZA
            Kontrahenci kontrahenci = (Kontrahenci)(Sesja.CreateObject("CDN.Kontrahenci", null));
            IKontrahent kontrahent = (IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];
            zapisKB.Podmiot = ((IPodmiot)kontrahent);            
            
                //Ustawienie daty i kwoty zapisu
			zapisKB.DataDok = DateTime.Today;                
            zapisKB.Kwota = 100;

                //Zapis sesji
			Sesja.Save();

			Console.WriteLine("Dodano zapis: {0}", zapisKB.NumerPelny);
		}
	}
}
