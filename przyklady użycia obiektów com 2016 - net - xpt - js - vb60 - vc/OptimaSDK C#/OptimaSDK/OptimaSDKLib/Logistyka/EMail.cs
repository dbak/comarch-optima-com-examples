﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using CDNBase;
using CDNMail;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;


namespace OptimaSDKLib.Logistyka
{
    class EMail : OptimaObjBase
    {
        public EMail(ILogin login)
            : base(login)
        {
        }


        public void Utworz(string adresaci, string temat, string tresc)
        {
            Utworz(adresaci, temat, tresc, string.Empty);
        }

        public void Utworz(string adresaci, string temat, string tresc, string sciezkaDoPlikuZalacznika)
        {
            Utworz(adresaci, temat, tresc, new[] { sciezkaDoPlikuZalacznika }, 0, false);
        }

        public void Utworz(string adresaci, string temat, string tresc, string[] zalaczniki, int kontoId, bool wyslijNatychmiast)
        {
            try
            {
                Console.WriteLine("Wysyłanie e-maila");

                var kolekcja = (CDNBase.IAdoCollection)Sesja.CreateObject("CDN.Wiadomosci", null);
                IWiadomosc wiadomoscDoWyslania = (CDNMail.IWiadomosc)kolekcja.AddNew(null);
                if (kontoId == 0) kontoId = UstalKontoId();
                wiadomoscDoWyslania.MKEID = kontoId;
                wiadomoscDoWyslania.Adresaci = adresaci;
                wiadomoscDoWyslania.MFLID =  UstalFolderId(wyslijNatychmiast);
                wiadomoscDoWyslania.Status = UstalStatus(wyslijNatychmiast);
                wiadomoscDoWyslania.Format = 2;  //2 - format tekstowy; 1 - format HTML
                wiadomoscDoWyslania.Temat = temat;
                if( wiadomoscDoWyslania.Format == 1 )
                    wiadomoscDoWyslania.TrescHTML = tresc;
                else
                    wiadomoscDoWyslania.Tresc = tresc;

                DodajZalaczniki(wiadomoscDoWyslania, zalaczniki);
                Zapisz();
                if (wyslijNatychmiast)
                {
                    wiadomoscDoWyslania.Wyslij();
                    Sesja.Save();
                }

                Console.WriteLine("Zapisano e-mail");
            }
            catch (COMException comError)
            {
                Console.WriteLine("###ERROR### Dodawanie nie powiodło się!\n{0}", ErrorInfo.Message(comError));
            }


        }

        private void DodajZalaczniki(IWiadomosc wiadomoscDoWyslania, string[] zalaczniki)
        {
            foreach (string zalacznik in zalaczniki)
            {
                DodajZalacznik(wiadomoscDoWyslania, zalacznik);
            }
        }

        private void DodajZalacznik(IWiadomosc wiadomoscDoWyslania, string sciezkaDoPliku)
        {
            const int equDanaBinarnaZalacznikWiadomosci = 9;
            if (!string.IsNullOrEmpty(sciezkaDoPliku)) wiadomoscDoWyslania.DodajZalacznik(sciezkaDoPliku, equDanaBinarnaZalacznikWiadomosci);
        }

        private int UstalKontoId()
        {
            int opeId = Sesja.Login.OperatorID;
            string query = "SELECT TOP 1 MKE_MKEID FROM CDN.MailKontaEMail INNER JOIN CDN.MailUzytkownicyKont ON MKE_MKEID=MUK_MKEID WHERE MKE_Nieaktywne = 0 AND MUK_OpeID = " + opeId;
            return GetValue.GetInt(Sesja, query, true);
        }


        private int UstalFolderId(bool wyslijNatychmiast)
        {
            int typFolderu = 7; //kopie robocze
            if (wyslijNatychmiast) typFolderu = 2;  //skrzynka nadawcza
            string query = "SELECT TOP 1 MFL_MFLID FROM CDN.MailFoldery WHERE MFL_Systemowy=1 AND MFL_Typ = " + typFolderu;
            return GetValue.GetInt(Sesja, query, true);
        }

        private int UstalStatus(bool wyslijNatychmiast)
        {
            return wyslijNatychmiast ? (int)CDNMail.MailStatusWiadomosci.e_op_WiadomoscDoWyslania
                                        : (int)CDNMail.MailStatusWiadomosci.e_op_WiadomoscRobocza;
        }


        /// <summary>
        /// Przy zapisywaniu seryjnym maila z wielu stanowisk może pobrać się ten sam numer w numeratorze i zostanie wyrzucony wyjątek, trzeba ponowić zapis
        /// </summary>
        private void Zapisz()
        {
            int tryCount = 0;
            while (true)
            {
                try
                {
                    tryCount++;
                    Sesja.Save();
                }
                catch (System.Runtime.InteropServices.COMException ex)
                {
                    if (tryCount > 3)
                        throw;
                    if (ex.ErrorCode == -2147217873 &&//DB_E_INTEGRITYVIOLATION -2147217873 = (80040e2f) 
                        ex.Message.ToLower().Contains("mailwaidomosci_numerdok"))//Cannot insert duplicate key row in object 'CDN.MailWiadomosci' with unique index 'MailWaidomosci_NumerDok'
                    {
                        continue;
                    }
                    throw;
                }
                break;
            }
            Sesja.Resync();
        }



    }
}
