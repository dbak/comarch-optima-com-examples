﻿using System;
using CDNBase;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Base
{
	public class Logowanie
	{

		public ApplicationClass Application { get; set; }
		public ILogin Login  { get; set; }

		public void Zaloguj(string user, string haslo, string firma)
		{

			var hPar = new object[] { 
						 1,  1,   0,  0,  1,   1,  0,    0,   0,   0,   0,   0,   1,   1,  1,   0,  0 };	// do jakich modułów się logujemy
			/* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP
			 */

			// katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
			//System.Environment.CurrentDirectory = @"C:\Program Files\OPTIMA.NET";	
			SetOptimaDirectory();
			// tworzymy nowy obiekt apliakcji
			Application = new ApplicationClass();
			// blokujemy
			Login = Application.LockApp(256, 5000, null, null, null, null);

			// logujemy się do podanej Firmy, na danego operatora, do podanych modułów
			Login = Application.Login(user, haslo, firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);

			// tu jesteśmy zalogowani do O!
			Console.WriteLine("Jesteśmy zalogowani do O!");
		}

		// wylogowanie z O!
		public void Wyloguj()
		{
			Login = null;
			// odblokowanie (wylogowanie) O!
			Application.UnlockApp();
			// niszczymy obiekt Aplikacji
			Application = null;
		}

		private  void SetOptimaDirectory()
		{
			//Odczytanie katalogu O! następuje przez odwołanie do klucza w rejestrach dla zarejestrowanego obiektu sesji ADOSession
			var ru = new RegUtil();
			string comguid = ru.ReadFromRegistry(RegutilKeys.HKCR, "CDNBase.AdoSession\\CLSID", "", "").ToString();
			string optimaDir = ru.ReadFromRegistry(RegutilKeys.HKCR, "CLSID\\" + comguid + "\\InprocServer32", "", "").ToString();
			optimaDir = optimaDir.Substring(0, optimaDir.Length - 12);
			System.Environment.CurrentDirectory = optimaDir;
		}

	}
}
