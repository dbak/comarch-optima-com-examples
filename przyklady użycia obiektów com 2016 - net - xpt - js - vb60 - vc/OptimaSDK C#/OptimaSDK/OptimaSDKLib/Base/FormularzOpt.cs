﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

namespace OptimaSDKLib.Base
{
    class FormularzOpt
    {
        static void Main(string[] args)
        {
            IntPtr hWnd = IntPtr.Zero;
            Process optima = null;
            bool newWnd = false;

            const uint TrNId = 6;
            // nazwa bazy
            const string dbName = "firma_20125_05_24";
            try
            {
                Process[] procs = Process.GetProcessesByName("Comarch OPT!MA");

                //moze byc uruchomionych kilka O! jednoczesnie
                if (procs.Length > 0)
                {
                    foreach (Process p in procs)
                    {
                        if (p.MainWindowTitle.Contains("[" + dbName + "]"))
                        {
                            optima = p;
                            break;
                        }
                    }
                }

                if (optima == null)
                {
                    optima = CreateOptimaProcess(dbName);
                    newWnd = true;
                }
                //waiting for Optima to open window, load licenses and connect to database
                while (optima.MainWindowHandle == IntPtr.Zero ||
                      !optima.MainWindowTitle.Contains("[" + dbName + "]"))
                {
                    Thread.Sleep(200);
                    optima.Refresh();
                }
                hWnd = optima.MainWindowHandle;

                if (User32Dll.IsIconic(hWnd))
                {
                    User32Dll.ShowWindow(hWnd, User32Dll.SW_RESTORE);
                }
                else if (newWnd)
                {
                    User32Dll.ShowWindow(hWnd, User32Dll.SW_SHOWMAXIMIZED);
                }
                else
                {
                    User32Dll.SetForegroundWindow(hWnd);
                }
                // id okna
                const int idokna = 25039;
                User32Dll.PostMessage(hWnd, 4096, TrNId, (uint) idokna);
            }
            catch
            {
                //MessageBox.Show("Błąd podczas komunikacji z Comarch OPT!MA");
            }
            finally
            {
                if (optima != null)
                    optima.Dispose();
            }
        }
        private static Process CreateOptimaProcess(string dbName)
        {
            if (string.IsNullOrEmpty(dbName))
                throw new ArgumentNullException("dbName");

            Process process = new Process();
            process.StartInfo.WorkingDirectory = "d:\\optima";
            process.StartInfo.FileName = "Comarch OPT!MA.exe";
            string pwd = "";
            string user = "ADMIN";

            if (pwd != "")
                process.StartInfo.Arguments = "U=\"" + user.Trim() + "\" H=\"" + pwd + "\" F=\"" + dbName + "\"";
            else
                process.StartInfo.Arguments = "U=\"" + user.Trim() + "\" H= F=\"" + dbName + "\"";
            process.Start();

            return process;
        }
        public class User32Dll
        {
            [DllImport("user32.dll")]
            public extern static IntPtr GetTopWindow(IntPtr hwWdParent);

            [DllImport("user32.dll")]
            public extern static IntPtr GetWindow(IntPtr hWnd, uint wCmd);

            public const int HWND_TOPMOST = -1;
            public const int HWND_TOP = 0;
            public const uint SWP_NOSIZE = 0x0001;
            public const uint SWP_NOMOVE = 0x0002;
            public const uint SWP_SHOWWINDOW = 0x0040;
            public const int SW_RESTORE = 9;
            public const int SW_SHOW = 5;
            public const int SW_SHOWMAXIMIZED = 3;

            public const int GW_HWNDNEXT = 2;

            [DllImport("User32.Dll")]
            public static extern void GetWindowText(IntPtr hWnd, StringBuilder s, int nMaxCount);

            [DllImport("user32.dll")]
            public static extern int PostMessage(IntPtr hWnd, uint wMsg, uint wParam, uint lParam);

            [DllImport("user32.dll")]
            public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

            [DllImport("user32.dll")]
            public static extern int SetWindowPos(IntPtr hWnd, int hWnndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

            [DllImport("user32.dll")]
            public static extern int ShowWindow(IntPtr hWnd, int nCmdShow);

            [DllImport("user32.dll")]
            public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint uFlags);

            [DllImport("user32.dll")]
            public static extern bool IsIconic(IntPtr hwnd);

            [DllImport("user32.dll", CharSet = CharSet.Ansi, EntryPoint = "GetSystemMetrics")]
            public static extern int GetSystemMetrics(int nIndex);

            [DllImport("user32.dll")]
            public extern static int SetForegroundWindow(IntPtr hWnd);
        }
    }
}
