﻿namespace OptimaNETSDK.Wydruki
{
    public enum TypOkna
    {
        Brak = 0,
        ProstyFormularz = 1,
        ZlozonyFormularz = 2,
        ProstaLista = 3,
        ZlozonaLista = 4
    }

    public enum TypWyboruWydruku
    {
        Brak = 0,
        Domyslny = 1,
        Podany = 2
    }

    public enum TypCGXWydruku
    {
        Brak = 0,
        Crystal = 1,
        GenRap = 2,
        XML = 3
    }

    public enum TypUrzadzenia
    {
        Brak = 1,
        Domyslne = 1,
        Podglad = 2,
        PodgladZKopiami = 3,
        DrukarkaDomyslna = 4,
        DrukarkaDomyslnaIloscKopii = 5,
        DrukarkaInna = 6,
        DrukarkaInnaIloscKopii = 7,
        DrukarkaInnaOpcje = 8,
        Eksport = 9,
        EksportOpcje = 10,
        Mail = 11,
        MailNaAdres = 12,
        MailOpcje = 13
    }
}