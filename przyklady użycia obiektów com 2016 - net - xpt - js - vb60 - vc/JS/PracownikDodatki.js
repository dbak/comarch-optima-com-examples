/*<COM_DOK>
<OPIS>
 Skrypt prezuentujacy prac� na kalendarzu pracownika
 Dodawane s� i odczytwyane nieobecno�ci w kalendarzu pracownika.  
</OPIS>
<Uruchomienie>W kodzie skryptu podane s�: Kod pracownika, daty i warto�ci danych historycznych</Uruchomienie>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IPracownik </Interfejs>
<Interfejs> IHistoria </Interfejs>
<Interfejs> ITypSkladnika </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	Dodatki()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function Dodatki() {
try{

// utworzenie obiektu Pracownik 

var rPracownik = rSesja.CreateObject("CDN.Pracownicy").Item("PRA_KOD = '003/E'")
WScript.Echo( rPracownik.Kod )


var rDodatek = rPracownik.Historia.Item(0).Pracownik.Dodatki.AddNew()

// Wyb�r sk�adnika
var rSkladnik = rSesja.CreateObject("CDNPlcfg.TypySkladnikow").Item("TWP_TwpId=53")

rDodatek.Historia.Item(0).Skladnik.TypSkladnika = rSkladnik
rDodatek.Historia.Item(0).Wazny.Od = "2007-01-01"
rDodatek.Historia.Item(0).Nazwa = rSkladnik.NazwaPelna
rDodatek.Historia.Item(0).Wartosc1= 45.00
rDodatek.Nazwa = rSkladnik.NazwaPelna

// Zapisanie obiektow 
rSesja.Save()
}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas dodawanie Nieobecnosci" + e.description)
	}
}
