
/*<COM_DOK>
<OPIS>
  Skrypt wykonuje wydruk faktury dla dokumentu podanego przez filtr SQL.
  

</OPIS>
<Uruchomienie>
Skrypt musi by� wywo�any w katalogu g��wnym Optimy 
przez przegrany w t� lokalizacj� program WScript.exe lub CScript.exe 
(oryginalnie lokalizacja pliku to np. c:Windows\System32\ )

Podanie operatora oraz firmy w kt�rej b�d� wykonywa�y sie wydruki
Podanie filtra do wydruku i podanie wydruku jaki ma by� drukowany
</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> WydrFormat</Interfejs> 
<Interfejs> ZmiennaDyn</Interfejs> 

<Osoba></Osoba>
<OPT_VER>2014</OPT_VER>
</COM_DOK>*/

// Tworzymy obiekt aplikacji aby zalogowa� si� do bazy.
var app = WScript.CreateObject("CDNBase.Application");

var Login	= app.LockApp(1)	

var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'cdn_firma'

	// Logowanie z podanim bazy, modu�u z karty operatora

Login=   app.Login(OPERATOR,HASLO,BAZA)


var WyswietlajKomunikaty = 1;


function NowyWM(_ProcID, _KontekstID)
{
  //Utworzenie obiektu
  var WM = new ActiveXObject("CDN.WydrMgr");

  //Inicjalizacja
  WM.ProcID     = _ProcID;
  WM.KontekstID = _KontekstID;

  return WM;
}

/*
  Funkcja tworz�ca, inicjalizuj�ca i zwracaj�ca obiekt formatu wydruku na podstawie podanego wywo�ania.
*/
function NowyFormatWydruku( _ZrodloID, _FormatID)
{
  //Utworzenie obiektu
  var Format = new ActiveXObject("CDN.WydrFormat");
 
  //Inicjalizacja
  Format.ZrodloID = 0;
  //Format.WydrukID = _WydrukID; 
  Format.ID       = _FormatID;

  return Format;
}


/*
  Funkcje pomocnicze.
*/

function ms(_Text)
{
  if(WyswietlajKomunikaty)
    WScript.Echo(_Text);
}

function dm(_Text)
{
  WScript.Echo(_Text)
}


/////////////////////////////////////////////////////////////////////////
/*
  Kod g��wny.
  Wydruk FS
*/
try
{
  WyswietlajKomunikaty = 0


/////////////////////////////////////////////////////////////////////////
//Utworzenie tablicy zmiennych dynamicznych przekazywanych 'z aplikacji'


  var ZmienneDyn = new ActiveXObject("CDNLib.Dictionary");


  //Wype�nienie tablicy zmiennych dynamicznych - symulacja dzia�ania aplikacji (konkretnie procedury BRaLista)

  var ZmiennaDyn1 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn1.Nazwa   = 'CDN_Tekst1';
  ZmiennaDyn1.Wartosc = 'Lista rejestr�w kasowych/bankowych';
  ZmienneDyn.item(ZmiennaDyn1.Nazwa) = ZmiennaDyn1;

  var ZmiennaDyn2 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn2.Nazwa   = 'CDN_Tekst2';
  ZmiennaDyn2.Wartosc = 'Lista rejestr�w kasowych/bankowych wg symbolu';
  ZmienneDyn.item(ZmiennaDyn2.Nazwa) = ZmiennaDyn2;

  var ZmiennaDyn3 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn3.Nazwa   = 'CDN_Data1';
  ZmiennaDyn3.Wartosc = 72500;
  ZmienneDyn.item(ZmiennaDyn3.Nazwa) = ZmiennaDyn3;

  var ZmiennaDyn4 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn4.Nazwa   = 'CDN_Firma4'
  ZmiennaDyn4.Wartosc = 'Nazwa Frimy 1';
  ZmienneDyn.item(ZmiennaDyn4.Nazwa) = ZmiennaDyn4;

  var ZmiennaDyn5 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn5.Nazwa   = 'CDN_Firma5'
  ZmiennaDyn5.Wartosc = 'Nazwa Firmy 2';
  ZmienneDyn.item(ZmiennaDyn5.Nazwa) = ZmiennaDyn5;

  var ZmiennaDyn6 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn6.Nazwa   = 'CDN_FMiasto2'
  ZmiennaDyn6.Wartosc = 'Miasto';
  ZmienneDyn.item(ZmiennaDyn6.Nazwa) = ZmiennaDyn6;

  var ZmiennaDyn7 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn7.Nazwa   = 'CDN_FUlica2'
  ZmiennaDyn7.Wartosc = 'Ulica';
  ZmienneDyn.item(ZmiennaDyn7.Nazwa) = ZmiennaDyn7;

  var ZmiennaDyn8 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn8.Nazwa   = 'CDN_FNip'
  ZmiennaDyn8.Wartosc = '111-111-11-11';
  ZmienneDyn.item(ZmiennaDyn8.Nazwa) = ZmiennaDyn8;

  var ZmiennaDyn9 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn9.Nazwa   = 'CDN_Operator'
  ZmiennaDyn9.Wartosc = 'Operator';
  ZmienneDyn.item(ZmiennaDyn9.Nazwa) = ZmiennaDyn9;

  var ZmiennaDyn10 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn10.Nazwa   = 'CDN_nrKopii'
  ZmiennaDyn10.Wartosc = 2;
  ZmienneDyn.item(ZmiennaDyn10.Nazwa) = ZmiennaDyn10;

  var ZmiennaDyn11 = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn11.Nazwa   = 'CDN_Aplikacja'
  ZmiennaDyn11.Wartosc = 'CDN_Aplikacja';
  ZmienneDyn.item(ZmiennaDyn11.Nazwa) = ZmiennaDyn11;

////////////////////////////////////////////////////////////////////
//Ustawmy id kontekstu wywo�ywanego wydruku:


var FormatID = 1834

  var Format = NowyFormatWydruku( 0, FormatID);


////////////////////////////////////////////////////////////////////
//Ustawmy jeszcze nast�puj�ce parametry wydruku:

 
  Format.FiltrSQL   = 'TrN_TRNID = 1'

  Format.Urzadzenie = 1 //Ekran
  
  //Format.Urzadzenie = 2 //Drukarka domy�lna
  //Format.Urzadzenie = 3 //Drukarka inna
  //Format.Drukarka   = '\\\\eol\\HP DeskJet 600'
  //Format.IloscKopii = 1
  //Format.MarginesL = 500
  //Format.MarginesG = 500


  //Wykonanie wydruku do pliku
  //Format.Urzadzenie = 4 //Eksport do pliku
  //Format.FormatPlikuDocelowego = 2 //PDF
  //Format.PlikDocelowy = "d:\\Faktura o ID " +  rDokumentHaMag.ID + ".pdf"

  //Mo�na ju� wykona� wydruk zgodnie z ustawieniami.
  Format.Wykonaj(ZmienneDyn)

  ms('OK, to by by�o na tyle.');

}

/*
  Obs�uga wyj�tk�w.
*/
catch (e)
{
  dm('Wyj�tek ' + e.number);
  dm('Wyj�tek ' + e.description);
}


/*
  Wylogowanie.
*/
app.UnLockApp();


