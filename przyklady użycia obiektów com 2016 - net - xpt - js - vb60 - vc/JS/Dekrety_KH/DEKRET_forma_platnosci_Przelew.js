/*<COM_DOK>
<OPIS>
	Import dekret�w ksi�gowy z pliku txt o okreslonym formacie z ustawieniem formy p�atno�ci na "przelew"
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IKontrahent </Interfejs>
<Interfejs> IDekret </Interfejs>
<Interfejs> IKonto </Interfejs>
<Interfejs> IOkres </Interfejs>
<Interfejs> IFormaPlatnosci </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'PrezentacjaKH'

// Wstempne okre�lenie okresu obrachunkowego - poni�ej skrypt pobiera domy�lny okres obrachunkowy
var ID_OkresuObr = 1


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	DodajDekret() 
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function DodajDekret() {

	var fso = new ActiveXObject("Scripting.FileSystemObject")
	var f = fso.OpenTextFile("Dekret.TXT") // Plik tekstowy
	var l		// Linia czytana z pliku tekstowego


	var rOtwartyOkres = rSesja.CreateObject("CDN.Okresy").Item("OOb_Stan=0")
	ID_OkresuObr = rOtwartyOkres.ID 

	rDekret = rSesja.CreateObject("CDN.DEKRETY").AddNew()

// Id dziennika 
	rDekret.OObID = ID_OkresuObr 
	ID_Dziennik = UsunKomentarz(f.ReadLine())	
	rDekret.DziID = ID_Dziennik	 
	rDekret.Bufor = 1
// Dokumnet
	l = UsunKomentarz(f.ReadLine())

	rDekret.Dokument	= l


// Druga linia w pliku zawiera dat�
	l = UsunKomentarz(f.ReadLine())


	rDekret.DataDok	= l


// Kontrahent

	l = UsunKomentarz(f.ReadLine())
// Kontrahent 

try{
	var rKontrahent = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_Kod='" + l + "'") 

	rDekret.PodmiotTyp	=1 
	rDekret.PodmiotID	=rKontrahent.ID

	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d wyboru kontrahenta " + e.description)	
	}



// Teraz b�d� dodawane pozycje 
	rPozycje = rDekret.Elementy
	while (!f.AtEndOfStream) {

		l = UsunKomentarz(f.ReadLine())

		DodajPozycje(l)
		}
// Zapisuj� zmiany
	rSesja.Save()

try {
	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas wystawiania faktury " + e.description)
	}

}
//-----------------------------------------
function DodajPozycje(linia) {
var i, j, KntWN, KntMA, Kwota
i = linia.search(",")
if (i!=-1) {
	// Je�li w lini by� przecinek, to oddzielam kod od ilo�ci
	KntWN = linia.substr(0,i)
	j = linia.substr(i+1).search(",")
	if (j!=-1) {
		KntMA = linia.substr(i+1,j)
		Kwota  = linia.substr(i+j+2)
		}
	else {
		KntMA = linia.substr(i+1)
		Kwota  = 0
		}
	}
else {
	KntWN = linia
	KntMA = ''
	Kwota = 0
	}

// Wyswietlenie element�w dekretu
// WScript.Echo(KntWN + ' ' + KntMA + ' ' + Kwota)
var rPozycja = rPozycje.AddNew()

// Sprawdzam, czy jest w og�le takie konto

if ( KntWN.length > 0 ) try {
	var rKontaWN = rSesja.CreateObject("CDN.Konta").Item("ACC_OOBid =" +ID_OkresuObr +"and  ACC_Numer='" + KntWN + "'")
	rPozycja.KontoWN = rKontaWN
	}
catch (e) {
	WScript.Echo("Nie uda�o si� znale�� konta [" + KntWN + "]. " + e.description)
	}


if ( KntMA.length > 0 ) try {
	var rKontaMA = rSesja.CreateObject("CDN.Konta").Item("ACC_OOBid ="+ ID_OkresuObr +"and ACC_Numer='" + KntMA + "'")
	rPozycja.KontoMA = rKontaMA
	}
catch (e) {
	WScript.Echo("Nie uda�o si� znale�� konta [" + KntMA + "]. " + e.description)
	}


// Podstawiamy wartosc
if (Kwota)
	{
	Kwota = Kwota.replace(',','')
	Kwota = Kwota.replace('\.',',')
	rPozycja.Kwota = Kwota
	}

//Za�cz generowanie p�atnosci
rPozycja.Platnosci=1 

// Utw�rz obiekt p�atnosci - przelew
var rFPp    = rSesja.CreateObject('CDN.FormyPlatnosci').Item('FPl_FPlId = 3')

// Podstawienie formy platnosci do elementu
rPozycja.FormaPlatnosci = rFPp




}

//-----------------------------------------
function UsunKomentarz(s) {
if (s.search(";")!=-1)
	s = s.substr(0,s.search(";"))
return s
}
