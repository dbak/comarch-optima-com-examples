

/*<COM_DOK>
<OPIS>
   Import faktury zakupu

// Poni�szy przyk�ad loguje si� do aplikacji, 
// nast�pnie otwiera plik tekstowy FZ.TXT, w kt�rm:
// pierwsza linia to symbol dokumentu
// druga linia to data dokumentu (dla uproszczenia dat jest przypisywana do daty wysatiwnia)
// trzecia linia to kod kontrahenta
// a w nast�pnych liniach znajduj� si� pozycje faktury w postaci: KOD TOWARU,ILO��,CENA (Ilo�� i Cena s� omitowalne)
// po znaku ; znajduj� si� komentarze


</OPIS>
<Uruchomienie>Do importu konieczny jest plik importu, nazw� pliku podaje si� w kodzie skryptu</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IDefinicjaDokumentu</Interfejs>
<Interfejs> IMagazyn</Interfejs>
<Interfejs> IKontrahent</Interfejs>
<Interfejs> ITowar</Interfejs>
<Interfejs> IDokumentHaMag</Interfejs>
<Interfejs> IElementHaMag</Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rDokumentHaMag	// Obiekt dokumentu FZ
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	DodajFakture ()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function DodajFakture() {
try {
	var fso = new ActiveXObject("Scripting.FileSystemObject")
	var f = fso.OpenTextFile("FZ.TXT") // Plik tekstowy
	var l		// Linia czytana z pliku tekstowego

// Dodajemy now� faktur�. 
// "CDN.DokumentyHaMag" jest to kolekcja wszystkich dokument�w handlowo/magazynowych sk�adowanych w tabeli TraNag
// Do tej kolekcji dodajemy nowy dokument
	rDokumentHaMag = rSesja.CreateObject("CDN.DokumentyHaMag").AddNew()

// Poni�ej ustawiam rodzaj i typ dokumentu - ich warto�ci opisano w dokumentacji bazy danych.
// Wa�ne jest, aby by�y ustawione na pocz�tku w tej w�a�nie kolejno�ci
	rDokumentHaMag.Rodzaj			= 301000
	rDokumentHaMag.TypDokumentu	= 301

// Pierwsza linia w pliku zawiera symbol dokumentu (DokDefinicje)
// Definicj� tworz� jako element kolekcji CDN.DefinicjeDokumentow.
// Definicja dokumentu, podobnie jak inne parametry numerowania (np. seria) znajduj� si� w Numerator'ze
// Metoda Item jako parametr zawiera filtr SQL wg kt�rego z tabeli CDN.DokDefinicje zostanie znaleziony rekord
// Je�li nie zosta�a poprawnie wype�niona, to zostanie przyj�ty default z konfiguracji
	var rNumerator = rDokumentHaMag.Numerator
	l = UsunKomentarz(f.ReadLine())
	try {
		var rDokDef = rSesja.CreateObject("CDN.DefinicjeDokumentow").Item("DDf_Symbol='" + l + "'")
		rNumerator.DefinicjaDokumentu = rDokDef
		}
	catch (e) {WScript.Echo("DDF " + e.description)}

// Druga linia w pliku zawiera dat�
	l = UsunKomentarz(f.ReadLine())
	rDokumentHaMag.DataDok		= l
	rDokumentHaMag.DataWys		= l

// Trzecia linia zawiera akronim kontrahenta
// Kontrahenta tworz� jako element kolekcji CDN.Kontrahenci. 
// Metoda Item jako parametr zawiera filtr SQL wg kt�rego z tabeli CDN.Kontrahenci zostanie znaleziony rekord kontrahenta
	l = UsunKomentarz(f.ReadLine()) 
	var rKontrahent = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_Kod='" + l + "'") 
	rDokumentHaMag.Podmiot = rKontrahent

// Teraz b�d� dodawane pozycje faktury
	rPozycje = rDokumentHaMag.Elementy
	while (!f.AtEndOfStream) {
		l = UsunKomentarz(f.ReadLine())
		DodajPozycje(l)
		}
// Zapisuj� zmiany
	rSesja.Save()
	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas wystawiania faktury " + e.description)
	}

}
//-----------------------------------------
function DodajPozycje(linia) {
var i, j, Twr, Ilosc, Cena
i = linia.search(",")
if (i!=-1) {
	// Je�li w lini by� przecinek, to oddzielam kod od ilo�ci
	Twr = linia.substr(0,i)
	j = linia.substr(i+1).search(",")
	if (j!=-1) {
		Ilosc = linia.substr(i+1,j)
		Cena  = linia.substr(i+j+2)
		}
	else {
		Ilosc = linia.substr(i+1)
		Cena  = 0
		}
	}
else {
	Twr = linia
	Ilosc = 1
	Cena = 0
	}

// Sprawdzam, czy jest w og�le taki towar
try {
	var rTowar = rSesja.CreateObject("CDN.Towary").Item("Twr_Kod='" + Twr + "'")
	}
catch (e) {
	WScript.Echo("Nie uda�o si� pobra� towaru [" + Twr + "]. " + e.description)
	return
	}

// Dodaj� pozycj� do faktury
var rPozycja = rPozycje.AddNew()

// Podstawiam towar
rPozycja.Towar = rTowar

// Podstawiam ilo��
rPozycja.IloscJM = Ilosc

// Podstawiam cen�
if (Cena)
	{
	Cena = Cena.replace(',','')
	Cena = Cena.replace('\.',',')
	rPozycja.CenaT = Cena
	}
}

//-----------------------------------------
function UsunKomentarz(s) {
if (s.search(";")!=-1)
	s = s.substr(0,s.search(";"))
return s
}
