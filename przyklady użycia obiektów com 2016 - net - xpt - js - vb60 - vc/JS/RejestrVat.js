////////////////////////////////////////////////////
//
// Dodawanie zapis�w w RVAT 
//
// To jest logowanie z wywo�aniem okna logowania
// Na kt�rym wybiera si� operatora i baz�

/*<COM_DOK>
<OPIS>
	Dodawanie zapis�w w RVAT
</OPIS>
<Uruchomienie>W kodzie skryptu nale�y poda�: kody kontrahent�w, konta w planie kont</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IVAT </Interfejs> 
<Interfejs> IKontrahent </Interfejs> 
<Interfejs> IRachunek </Interfejs> 
<Interfejs> IVATElement </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

var rApp		
var rLogin		
var rSesja		

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

rApp	= new ActiveXObject("CDNBASE.Application") 

	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

rSesja	= rLogin.CreateSession()

//Najpierw sprawdz� istnienie kontrahenta
var rKontrahent = SprawdzKontrahenta ("AA1234")

// TU DODAJ� ZAPIS W REJESTRZE:

var rVat = rSesja.CreateObject("CDN.RejestryVat").AddNew()

rVat.Dokument	= "FV 123/45"
rVat.Rejestr	= "ZAKUP"
rVat.Typ	= 1	//1 - oznacza rejestr zakupu; 2 - sprzeda�y
rVat.DataZap	= "2008/10/21"
rVat.Podmiot	= rKontrahent
rVat.FormaPlatnosci = rSesja.CreateObject("CDN.FormyPlatnosci").Item("FPl_Nazwa='przelew'")

var rElement

// Tu dodaj� pozycj� w stawce 22%
rElement	= rVat.Elementy.AddNew()
rElement.Stawka	= 22.00
rElement.Flaga	= 2	//stawka VAT: 1-zwolniona, 2-opodatkowana, 4-nie podlega
rElement.Netto	= 123.45

// Tu dodaj� drug� pozycj� w stawce zwolniona
rElement	= rVat.Elementy.AddNew()
rElement.Stawka	= 0.00
rElement.Flaga	= 1	//stawka VAT: 1-zwolniona, 2-opodatkowana, 4-nie podlega
rElement.Netto	= 67.89

var rKwotaDod
//Tu dodaj� kwoty dodatkowe - a wi�c pozycje, kt�re potem mog� by� wykorzystane przy ksi�gowaniu
rKwotaDod	= rVat.KwotyDodatkowe.AddNew()
rKwotaDod.Segment1	= "401-1"
rKwotaDod.Segment2	= "301-1"
rKwotaDod.Kwota		= 34.56

rKwotaDod	= rVat.KwotyDodatkowe.AddNew()
rKwotaDod.Segment1	= "401-2"
rKwotaDod.Segment2	= "301-2"
rKwotaDod.Kwota		= 98.21


rSesja.Save()

rApp.UnlockApp() // na zako�czenie trzeba zrobi� UnlockApp() - inaczej GPF

new ActiveXObject("WScript.Shell").Popup("KONIEC")


function SprawdzKontrahenta (_kod) {
 var rKnt
 try {
  rKnt = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_Kod='" + _kod + "'")
 }
 catch(e) {
  // jak nie ma to dodaj�
  rKnt = rSesja.CreateObject("CDN.Kontrahenci").AddNew()
  rKnt.Akronim = _kod
  rSesja.Save()
 }
 return rKnt
}