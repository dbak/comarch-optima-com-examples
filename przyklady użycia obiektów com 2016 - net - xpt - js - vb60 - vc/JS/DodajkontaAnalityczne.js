/*<COM_DOK>
<OPIS>
	Dodanie kont slownikowych
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IKonto </Interfejs>


<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/



var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


var sqlConn	= new ActiveXObject('ADODB.Connection')
var sqlRs  	= new ActiveXObject ('ADODB.Recordset')



try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	WScript.Echo("Zalogowany do bazy :"+rLogin.Firm.Name )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}


if (!rLogin){
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {

////////////////////////////////////////////////////////////////////////
// dodanie kont:

// Dodanie analitycznego konta s�wnikowewgo
ADDKonto('201.1.1','Kntrahent','Kontrahent',1,1,16)

// Dodanie innego konta analitycznego 
ADDKonto('404','03','Pozosta�e podatki i op�aty',0,0,0)

// UWAGA!
// Konta: 404 i 201.1.1 musza istnie� w planie kont


	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}


//f.close()
//-----------------------------------------
function ADDKonto(Ojciec,Syn,Nazwa,Slownikowe,STyp,SlowID) 
{

// Ojciec - Numer konkta nadrzendnego.
// Syn    - Numer dodawanego konata.
// Nazwa  - Nazwa konta dodawanego.
// Slownikowe - czy slownikowe 
// STyp   - Typ slownika 
// SlowID - ID w slowniku.

	rKonto   = rSesja.CreateObject("CDN.Konta").AddNew()
	rKontoOj = rSesja.CreateObject("CDN.Konta").Item("ACC_NumerIDx ='"+ Ojciec +"'")

//WScript.Echo(rKontoOj.Id)
//WScript.Echo(rKontoOj.CzyMaKsiegowania)
//WScript.Echo()

if (!rKontoOj.CzyMaKsiegowania){
	rKonto.ParId     = rKontoOj.Id
	rKonto.Nazwa     = Nazwa;
	rKonto.Segment   = Syn
	rKonto.Numer     = Syn
	rKonto.NumerIdx  = Syn
	rKonto.OkresId   = 2     	// UWAGA! Podaj prawid�owy ID okresu obrachunkowego //

if ( Slownikowe == 1 )
	{
         rKonto.Slownik = 1
	 rKonto.SlownikTyp = STyp
	 rKonto.SlownikId  = SlowID
	}

//WScript.Echo(rKonto.NumerIdx)
//WScript.Echo(rKonto.Segment)
//WScript.Echo(rKonto.Poziom)
//rKonto.ParId = rKontoOj.Id
//        rKonto.ParentACC

        rSesja.Save()
	}

//try{
//} catch( e ) { f.WriteLine("Raport: " + e.descripton ) }

}