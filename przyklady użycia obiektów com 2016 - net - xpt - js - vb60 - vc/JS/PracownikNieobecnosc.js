//
// Skrypt prezuentujacy prac� na kalendarzu pracownika
// Dodawane s� i odczytwyane nieobecno�ci w kalendarzu prac.
//

/*<COM_DOK>
<OPIS>
	Skrypt prezuentujacy prac� na kalendarzu pracownika.
	Dodawane s� i odczytwyane nieobecno�ci w kalendarzu pracy.
</OPIS>
<Uruchomienie> Operacje wykonywane s� dla pracownika o kodzie 002/E </Uruchomienie>
<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IPracownik </Interfejs> 
<Interfejs> ITypNieobecnosci </Interfejs>
<Interfejs> ICzasPracy </Interfejs> 
<Interfejs> INieobecnosc </Interfejs> 

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	Nieobecnosci()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function Nieobecnosci() {
try{

// utworzenie obiektu Pracownik o kodzie 002/e

var rPracownik = rSesja.CreateObject("CDN.Pracownicy").Item("PRA_KOD = '002/E'")
WScript.Echo( rPracownik.Kod )

// Odczytanie pierwszej nieobecno�ci z listy nieobecnoci pracownika - Item(0)
var rCzasPracy = rPracownik.CzasPracy
var Nieobecnosci = rCzasPracy.Nieobecnosci

WScript.Echo( Nieobecnosci.Item(0).Opis )
WScript.Echo( Nieobecnosci.Item(0).Okres.Do )
WScript.Echo( Nieobecnosci.Item(0).Okres.Od )
WScript.Echo( Nieobecnosci.Item(0).Typ.Nazwa )
WScript.Echo( Nieobecnosci.Item(0).Typ.Akronim )


// Dodanie nieobecno�ci url.wyp.
var Nieobecnosc =  Nieobecnosci.AddNew()
var Typ = rSesja.CreateObject("CDNKalend.TypyNieobecnosci").Item("TNB_Akronim  = 'url.wyp.'")
Nieobecnosc.Typ = Typ
Nieobecnosc.Okres.Od = '2010-10-11'
Nieobecnosc.Okres.Do = '2010-10-14'

// Zapisanie obiektow 
rSesja.Save()
}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas dodawanie Nieobecnosci" + e.description)
	}
}
