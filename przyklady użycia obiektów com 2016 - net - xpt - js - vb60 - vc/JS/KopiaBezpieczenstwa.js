/*<COM_DOK>
<OPIS>
   Utworzenie kopii bezpiecze�stwa wszystkich baz systemu.
   
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

// Urzytkonik z serwera SQL w razie logowania na autentykacje NT wymagana jest do ustawienia flaga Trusted_Connection 
var KopiaUser = 'sa'
var KopiaPass = 'zaqwsx!@'


if ( WScript.Arguments.length == 0 ) 
	WScript.Echo('Podaj jako argument wywo�ania sciezk� dost�pu dla katalog�w plik�w kopii \r\n  NP \r\n  KopiaBezpieczenstwa.js    c:\\backup\\ ')
else
{
var FolderBackupu = WScript.Arguments.Item(0)
var Id, Nazwa, Baza, Status

try
{
// Otwarcie pliku tekstowego do zapisu loga operacji
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var f = fso.CreateTextFile(FolderBackupu+"Raport.txt", true);

// Utworzenie obiektu aplikacji
	try
	{
		f.WriteLine("----------------------------");
		f.WriteBlankLines(1)
		f.WriteLine("Kopie bezpieczenstwa baz danych CDN OPT!MA.");
		var app      = WScript.CreateObject("CDNBase.Application");
		var rsBazy   = WScript.CreateObject("ADODB.Recordset");
		var KonfDB   = app.Configuration;

		var KonfConn = KonfDB.Connection;
		rsBazy.Open("SELECT Baz_BazId, Baz_Nazwa , +REPLACE(REPLACE(REPLACE(CONVERT(CHAR(16), GETDATE(), 120), ' ', ''), '-', ''), ':', '') as Time FROM CDN.Bazy WHERE Baz_NieAktywna = 0 ORDER BY Baz_Nazwa", KonfConn)
		rsBazy.MoveFirst()

		while (!rsBazy.EOF)
		{
			try
			{
				f.WriteBlankLines(1);
				f.WriteLine("----------------------------");
				f.WriteBlankLines(1);

				Nazwa = rsBazy.Fields("Baz_Nazwa")

				f.WriteLine("BAZA: "+Nazwa+ " PLIK: "+Nazwa+rsBazy.Fields("Time")+".BAC");

				Id    = rsBazy.Fields("Baz_BazId")
				Baza  = app.Item("#"+Id)
				Baza.User     = KopiaUser
				Baza.Password = KopiaPass
				Baza.Backup(FolderBackupu+Nazwa+rsBazy.Fields("Time")+".BAC", 1, "");

			}
			catch (e)
			{
				f.WriteLine("B��D KOPII: " + e.description);
			}
	        	rsBazy.MoveNext()


		}
		rsBazy.Close();

		KonfDB.Backup(FolderBackupu+"Konfiguracyjna"+ ".BAC", 1, "");
		f.WriteLine( "BAZA: Konfiguracyjna ");


	}
	catch (e)
	{
		f.WriteLine("B��d inicjalizacji: " + e.description);
	}
	f.Close();
	WScript.echo('Zako�czono wykonywanie zadania. Log w pliku Raport.TXT')
}
catch (e)
{
	WScript.Echo("Nie uda�o si� otworzy� loga operacji: " + e.description);
}
}