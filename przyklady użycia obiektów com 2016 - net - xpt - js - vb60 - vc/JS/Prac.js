////////////////////////////////////////////////////
// Poni�szy przyk�ad loguje si� do aplikacji, 
// nast�pnie twoprzy obiekt pracownika i podaje 
// jego podstawowe dane.


/*<COM_DOK>
<OPIS>
	Przyk�ad tworzy obiekt pracownika i podaje jego podstawowe dane.
</OPIS>
<Uruchomienie> W kodzie skryptu nale�y poda� ID korygowanego dokumentu FA</Uruchomienie>
<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IPracownik </Interfejs> 
<Interfejs> IHistoria </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

var rPracownik	// Obiekt pracownika
var rHistoria	// Obiekt historii
var rListaHist  // Obiekt listy historii

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	DodajPracownika()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function DodajPracownika() {
try {


// Kreowanie obiektu pracownika
	rPracownik = rSesja.CreateObject("CDN.Pracownicy").AddNew()
	rListaHist  = rPracownik.Historia 

//Tworzenie historii		

	rHistoria = rListaHist.AddNew()

// Wype�niania p�l	
	rHistoria.Kod = "Kod"
	rHistoria.Nazwisko ="Nazwisko"
	rHistoria.Imie1 = "Imie"

// Zapis operacji do bazy
	rSesja.Save()
	}
catch (e) {
//	Przechwycenie b��d�w.

	WScript.Echo("Wyst�pi� b��d  " + e.description)
	}

}
//-----------------------------------------

