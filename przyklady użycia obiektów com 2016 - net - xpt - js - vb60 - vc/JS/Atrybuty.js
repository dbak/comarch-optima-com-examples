

/*<COM_DOK>
<OPIS>
	Przyk�ad dodaje do s�wnika atrybut�w atrybut dla kontrahenta i towaru.
	 plik interfejsu op_twrb1.idl 
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> ITowar </Interfejs>
<Interfejs> IDefAtrybut </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

 	

// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury
// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


// Przeprowadzamy logowanie
if ( LogowanieSkrypt()==1 ) 
	{


	WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )


// Wykonujemy dodawania atrybut�w
	DodajAtrybutKontrahent()
	DodajAtrybutTowar()

	WScript.Echo("KONIEC")

        WylogowanieSkrypt ()

	}

//-----------------------------------------
function DodajAtrybutKontrahent() {
try {

	rAtrybut = rSesja.CreateObject("CDN.DefAtrybuty").AddNew()
	rAtrybut.Kod = 'Kontrahent'
	rAtrybut.Nazwa = 'Kontrahent'
	rAtrybut.Typ = 2 // Atrybut dla kontrahenta

	rSesja.Save()

	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d  " + e.description)
	}
}

//-----------------------------------------
function DodajAtrybutTowar() {
try {

	rAtrybut = rSesja.CreateObject("CDN.DefAtrybuty").AddNew()
	rAtrybut.Kod = 'Towar'
	rAtrybut.Nazwa = 'Towar'
	rAtrybut.Typ = 1 // Atrybut dla towaru

	rSesja.Save()

	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d  " + e.description)
	}
}

//----------------------------------------

function WylogowanieSkrypt ()
{
 	rApp.UnLockApp()
}

function LogowanieSkrypt ()
{

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	

	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu

	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	return 0
	}
else 	return 1 
}