// Poni�szy przyk�ad loguje si� do aplikacji, 
// plik interfejsu op_twrb1.idl v8.0

/*<COM_DOK>
<OPIS>
	Generacja dokumentu RO z dokumentu faktura proforma FPR
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> SerwisHaMag </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/



// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

//Inicjalizacja zmiennych dla potrzeb otwierania recordset�w i plik�w
var adOpenStatic		= 3
var adLockBatchOptimistic	= 4
var adCmdText			= 1
var adCmdTable			= 2
var adUseClient			= 3
var ForAppending		= 8



if ( LogowanieSkrypt()==1 ) 
	{

	WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )

	KonwertujFPRdoRO()

	WScript.Echo("KONIEC")

        WylogowanieSkrypt ()

	}


//----------------------------------------
function KonwertujFPRdoRO()
{

//RS 
try {
var sqlRS	= new ActiveXObject("ADODB.Recordset");

	//var rParagon = rSesja.CreateObject("CDN.DefAtrybuty").AddNew()

	var rSerwis  = rSesja.CreateObject("CDN.SerwisHaMag")


	// Do podania id dokumenty FPR z 161 na id dokumentu obecnego w bazie
	sqlRS.Open('select ID= 161 ', rSesja.Connection , adOpenStatic, adLockBatchOptimistic, adCmdText)


	rSerwis.AgregujDokumenty(320,sqlRS,0,308)


	rSesja.Save()


	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d  " + e.description)
	}

}

//----------------------------------------

function WylogowanieSkrypt ()
{
 	rApp.UnLockApp()
}

function LogowanieSkrypt ()
{

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	return 0
	}
else 	return 1 
}