//////////////////////////////////////////////////////////////////////
// CdnCurr.h

#ifndef _CDNCURR_H
#define _CDNCURR_H

#include <comdef.h>
#include <stdio.h>

//--MC6.5
#pragma warning( disable : 4244 )
//////////////////////////////////////////////////////////////////////
// _currency_t


class _currency_t
{
private:
	enum{
		fraction	= 10000,
	};

	CY m_data;

private:
	void _set(double d) 
	{
		if (d>=0)
			m_data.int64 = 100*(__int64)(100*d + 0.500000001);	
		else
			m_data.int64 = 100*(__int64)(100*d - 0.500000001);	
			//Zaokr�glenie plus poprawka na niedok�adno�� double
			//--> 0.05 mo�e by� pamietane jako 0.04999999999,
			//*100 -> przeliczenie do CURRENCY.
	}
	void _set(int i) 
	{
		m_data.int64 = fraction*i;
	}
	void _set(const CY &c) 
	{
		m_data.int64 = 100*((c.int64+50)/100);
		//Zaokr�glamy do dw�ch miejsc po przecinku.
	}

private:
//Operatory prywatne -> por�wnanie currency z double jest zabronione.
	BOOL operator > (double _d) const
	{
		return 0;
	}
	BOOL operator >= (double _d) const
	{
		return 0;
	}
	BOOL operator == (double _d) const
	{
		return 0;
	}
	BOOL operator != (double _d) const
	{
		return 0;
	}
	BOOL operator <= (double _d) const
	{
		return 0;
	}
	BOOL operator < (double _d) const
	{
		return 0;
	}

public:
	_currency_t()
	{
		m_data.int64 = 0;
	}
	_currency_t(const _currency_t &c)
	{
		m_data = c.m_data;
	}
	_currency_t(double d)
	{
		_set(d);
	}
	_currency_t(int i)
	{
		_set(i);
	}
	_currency_t(const _variant_t &v)
	{
		_set((double)v);
	}
	_currency_t(const CY &c)
	{
		_set(c);
	}
	const _currency_t& operator = (double d)
	{
		_set(d);
		return *this;
	}
	const _currency_t& operator = (int i)
	{
		_set(i);
		return *this;
	}
	const _currency_t& operator = (const _variant_t &v)
	{
		_set((double)v);
		return *this;
	}
	const _currency_t& operator += (double d)
	{
		_set(*this + d);
		return *this;
	}
	const _currency_t& operator += (int i)
	{
		m_data.int64 += fraction*i;
		return *this;
	}
	const _currency_t& operator -= (double d)
	{
		_set(*this - d);
		return *this;
	}
	const _currency_t& operator -= (int i)
	{
		m_data.int64 -= fraction*i;
		return *this;
	}
	const _currency_t& operator *= (double _d)
	{
		_set(*this * _d);
		return *this;
	}
	const _currency_t& operator /= (double _d)
	{
		_set(*this / _d);
		return *this;
	}
	inline operator double () const
	{
		return value;
	}
	inline operator CY () const
	{
		return m_data;
	}

	BOOL operator > (int _i) const
	{
		return m_data.int64>fraction*_i;
	}
	BOOL operator > (const _currency_t &_c) const
	{
		return m_data.int64>_c.m_data.int64;
	}

	BOOL operator >= (int _i) const
	{
		return m_data.int64>=fraction*_i;
	}
	BOOL operator >= (const _currency_t &_c) const
	{
		return m_data.int64>_c.m_data.int64;
	}

	BOOL operator == (int _i) const
	{
		return m_data.int64==fraction*_i;
	}
	BOOL operator == (const _currency_t &_c) const
	{
		return m_data.int64==_c.m_data.int64;
	}

	BOOL operator != (int _i) const
	{
		return m_data.int64!=fraction*_i;
	}
	BOOL operator != (const _currency_t &_c) const
	{
		return m_data.int64!=_c.m_data.int64;
	}

	BOOL operator <= (int _i) const
	{
		return m_data.int64<=fraction*_i;
	}
	BOOL operator <= (const _currency_t &_c) const
	{
		return m_data.int64<=_c.m_data.int64;
	}

	BOOL operator < (int _i) const
	{
		return m_data.int64<fraction*_i;
	}
	BOOL operator < (const _currency_t &_c) const
	{
		return m_data.int64<_c.m_data.int64;
	}
	_currency_t Minus(_currency_t & c)
	{
		if (m_data.int64<c.m_data.int64) {
			_currency_t result = *this;
			c.m_data.int64 -= m_data.int64;
			m_data.int64 = 0;
			return result;
		}
		else {
			_currency_t result = c;
			m_data.int64 -= c.m_data.int64;
			c.m_data.int64 = 0;
			return result;
		}
	}
	_currency_t & MustPositive()
	{
		if (m_data.int64<0) m_data.int64 = 0;
		return *this;
	}
	_currency_t Round(const _currency_t cFraction) const
	{
		_currency_t v;
		const LONGLONG & l = cFraction.m_data.int64;
		if (m_data.int64 > 0)
			v.m_data.int64 = l*((m_data.int64+l/2)/l);
		else
			v.m_data.int64 = -1*l*((-1*m_data.int64+l/2)/l);
		return v;
	}
	operator _variant_t() const
	{
		return _variant_t(m_data);
	}


	inline operator _bstr_t() const
	{			
		return _bstr_t(value).copy();
	}

public:

	_bstr_t Format(const char* AFormat)
	// PB, standard format for currency: "%0.2f"
	// Raczej powinni�my u�ywa� poni�szej funkcji ale niestety sieje wyj�tkiem.
	//swprintf(
	//  wchar_t *buffer,
	//   size_t count,
	//   const wchar_t *format [,
	//   argument]...
	//);

	{
		_bstr_t			sResult;
		char			buffer[256];
		//unsigned short	nCount = 255; // Maximum number of characters to store

		double dValue = this->Value();
		sprintf(buffer, AFormat, dValue);
		sResult = buffer;

		return sResult.copy();
	}

public:
	inline __int64 Data() const
	{
		return m_data.int64;
	}
	inline double Value() const
	{
		return (double)m_data.int64/fraction;
	}

	__declspec(property(get=Data)) __int64 data;
	__declspec(property(get=Value)) double value;
};

typedef _currency_t & _currency_r;
typedef const _currency_t & _currency_cr;

#endif //_CDNCURR_H