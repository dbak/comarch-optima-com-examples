
#if !defined(_CDN_H_INCLUDED_)
#define _CDN_H_INCLUDED_

#include "product.h"

#include <cdncurr.h>
#define CURRENCY _currency_t

// Załadowanie biblioteki ADODB
#undef EOF
enum {EOF = -1};
#pragma warning( push )
#pragma warning( disable : 4146 )
#import "msado27.tlb" no_namespace named_guids
#pragma warning( pop ) 
#include <oledberr.h>

// Załadowanie bibliotek CDN
#ifdef PRODUCT_OP
#define PROVIDECLASSINFO_EXCLUDES "IProvideClassInfo", "ITypeInfo", "tagTYPEATTR", "tagTYPEKIND", "tagTYPEDESC", "__MIDL_IOleAutomationTypes_0005", "tagARRAYDESC", "tagSAFEARRAYBOUND", "tagIDLDESC", "DWORD", "ITypeComp", "tagDESCKIND", "tagFUNCDESC", "tagELEMDESC", "tagPARAMDESC", "tagPARAMDESCEX", "tagFUNCKIND", "tagINVOKEKIND", "tagCALLCONV", "tagVARDESC", "__MIDL_IOleAutomationTypes_0006", "tagVARKIND", "ITypeLib", "tagTLIBATTR", "tagSYSKIND", "IProvideClassInfo2"
#define IMPORT_OPTIONS no_namespace named_guids exclude(PROVIDECLASSINFO_EXCLUDES)

#import "cdnlib.tlb" IMPORT_OPTIONS
#import "op_base.tlb" IMPORT_OPTIONS
#import "op_heal.tlb" IMPORT_OPTIONS
#import "op_kasbo.tlb" IMPORT_OPTIONS
#import "rabaty.tlb" IMPORT_OPTIONS
#import "op_hlmn.tlb" IMPORT_OPTIONS
#import "op_dave.tlb" IMPORT_OPTIONS
#import "op_plcfg.tlb" IMPORT_OPTIONS
#import "op_kalb.tlb" IMPORT_OPTIONS
#import "op_slow.tlb" IMPORT_OPTIONS
#import "op_srtb.tlb" IMPORT_OPTIONS
#import "op_kh.tlb" IMPORT_OPTIONS
#import "op_pracb.tlb" IMPORT_OPTIONS
#import "op_twrb1.tlb" IMPORT_OPTIONS
#import "op_kprr.tlb" IMPORT_OPTIONS
#import "op_rvat.tlb" IMPORT_OPTIONS
#import "op_csrs.tlb" IMPORT_OPTIONS
#endif
#import "cdn_ax.tlb" no_namespace named_guids


#include <cdneq.h>
#include <cdnerr.h>
#include <cdncom.h>
#include <cdnstd.h>
#include <cdnatl.h>
#include <cdnbase.h>

#endif // !defined(_CDN_H_INCLUDED_)
