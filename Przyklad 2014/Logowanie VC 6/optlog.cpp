// optlog.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int main(int argc, char* argv[])
{
	HRESULT hr = CoInitialize(NULL); 

	CLSID clsid; 
	hr = CLSIDFromProgID(OLESTR("CDNBase.Application"), &clsid); 

	IApplication* optima; 
	hr = CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER, __uuidof(IApplication), (LPVOID *)&optima); 

	try
	{
		if(hr == S_OK)
			hr = optima->LockApp(255, 10);

		printf("Logowanie ok.\n");

			hr = optima->UnlockApp();
		
	}
	catch(...)
	{
		printf("Blad logowania.\n");
	}
    CoUninitialize();
	return 0;
}

