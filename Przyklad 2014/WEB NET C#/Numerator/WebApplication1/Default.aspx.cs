﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Runtime.InteropServices;
using CDNBase;
using Microsoft.Win32;

namespace Webtest
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [STAThread]
        protected void Button1_Click(object sender, EventArgs e)
        {
            InfoTxt = new StringBuilder();
            // tu będziemy sobie wywoływać nasze przykłady.
            try
            {
                InfoTxt.AppendLine("próba logowania");
                LogowanieAutomatyczne();
                InfoTxt.AppendLine("próba dodania faktury");
                DodanieFaktury(uxKntKod.Text, int.Parse(uxFaNr.Text));
            }
            catch (COMException COMe)
            {
                if (COMe.InnerException != null)
                    InfoTxt.AppendLine("Błąd: " + COMe.InnerException.Message);
                else
                    InfoTxt.AppendLine("Błąd: " + COMe.Message);
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    InfoTxt.AppendLine("Błąd: " + exc.InnerException.Message);
                else
                    InfoTxt.AppendLine("Błąd: " + exc.Message);
            }
            finally
            {
                try
                {
                    Wylogowanie();
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null)
                        InfoTxt.AppendLine("Błąd: " + exc.InnerException.Message);
                    else
                        InfoTxt.AppendLine("Błąd: " + exc.Message);
                }
            }
            uxResultText.Text = InfoTxt.ToString().Replace("\n","<br/>");
        }

        protected ApplicationClass Application = null;
        protected ILogin Login = null;
        protected StringBuilder InfoTxt;

        // Przyklad 1. - Logowanie do O! bez wyświetlania okienka logowania
        protected void LogowanieAutomatyczne()
        {
            string Operator = "ADMIN";
            string Haslo = null;		// operator nie ma hasła
            string Firma = "firma1";	// nazwa firmy

            object[] hPar = new object[] { 
						 1,  0,   0,  0,  1,   1,  0,    0,   0,   0,   0,   0,   0,   0,  1,   0,  0 };	// do jakich modułów się logujemy
            /* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP
             */


            // katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
            System.Environment.CurrentDirectory = @"C:\Program Files (x86)\Comarch OPT!MA";
            
            Application = new ApplicationClass();
            // blokujemy
            Application.LockApp(513, 5000, null, null, null, null);
            // logujemy się do podanej Firmy, na danego operatora, do podanych modułów
            Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);

            // tu jesteśmy zalogowani do O!
            InfoTxt.AppendLine("Jesteśmy zalogowani do O!");
        }
        // wylogowanie z O!
        protected void Wylogowanie()
        {
            // niszczymy Login
            Login = null;
            // odblokowanie (wylogowanie) O!
            Application.UnlockApp();
            // niszczymy obiekt Aplikacji
            Application = null;
        }


        // Przykład 5. - Dodanie faktury sprzedaży
        protected void DodanieFaktury(string kod, int numer)
        {
            CDNBase.AdoSession Sesja = Login.CreateSession();

            CDNHlmn.DokumentyHaMag Faktury = (CDNHlmn.DokumentyHaMag)Sesja.CreateObject("CDN.DokumentyHaMag", null);
            CDNHlmn.IDokumentHaMag Faktura = (CDNHlmn.IDokumentHaMag)Faktury.AddNew(null);

            CDNBase.ICollection Kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
            CDNHeal.IKontrahent Kontrahent = (CDNHeal.IKontrahent)Kontrahenci["Knt_Kod='"+ kod +"'"];

            CDNBase.ICollection FormyPlatnosci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.FormyPlatnosci", null));
            OP_KASBOLib.FormaPlatnosci FPl = (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[1];

            CDNHeal.DefinicjeDokumentow DefinicjeDokumentow = (CDNHeal.DefinicjeDokumentow)Sesja.CreateObject("CDN.DefinicjeDokumentow", null);
            CDNHeal.IDefinicjaDokumentu DefinicjaDokumentu = (CDNHeal.DefinicjaDokumentu)DefinicjeDokumentow["Ddf_Symbol='FA'"];

            // e_op_Rdz_FS 			302000
            Faktura.Rodzaj = 302000;
            // e_op_KlasaFS			302
            Faktura.TypDokumentu = 302;

            //Ustawiamy bufor
            Faktura.Bufor = 1;

            //Ustawiamy date
            Faktura.DataDok = new DateTime(2008, 10, 13);

            //Ustawiamy formę póatności
            Faktura.FormaPlatnosci = FPl;

            //Ustawiamy podmiot
            Faktura.Podmiot = (CDNHeal.IPodmiot)Kontrahent;

            //Ustawiamy magazyn
            Faktura.MagazynZrodlowyID = 1;

            
			//Ustawiamy numerator
            OP_KASBOLib.INumerator numerator = (OP_KASBOLib.INumerator)Faktura.Numerator;

            numerator.DefinicjaDokumentu = definicjaDokumentu;
            numerator.NumerNr = numer;    
            numerator.Rejestr = "A1";//ustawiam serię dla wybranej definicji dokumentów
            //numerator.Rejestr = 133; //jeśli potrzeba ustawić numer

            //zapisujemy
            Sesja.Save();

            InfoTxt.AppendLine("Dodano fakturę: " + Faktura.NumerPelny);
        }
    }
}
