using System;
using CDNBase;
using System.Runtime.InteropServices;

namespace Konsola
{
	// Klasa z przyk�adami dzia�aj�cymi na konsoli
	class Przyklady
	{

		protected static ApplicationClass	Application = null;
		protected static ILogin				Login		= null;

		// Przyklad 1. - Logowanie do O! bez wy�wietlania okienka logowania
		static protected void LogowanieAutomatyczne()
		{
			string			Operator	= "GS";
			string			Haslo		= null;		// operator nie ma has�a
            string          Firma       = "Prezentacja_KH";	// nazwa firmy

			object[] hPar		= new object[] { 
						 1,  0,   0,  0,  1,   1,  0,    0,   0,   0,   0,   0,   0,   0,  1,   0,  0 };	// do jakich modu��w si� logujemy
			/* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP
			 */

			
			// katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadzia�a, chyba �e program odpalimy z katalogu O!)
			System.Environment.CurrentDirectory	= @"C:\Optima";	

			// tworzymy nowy obiekt apliakcji
			Application = new ApplicationClass();
			// blokujemy
			Application.LockApp(513, 5000, null, null, null, null);
			// logujemy si� do podanej Firmy, na danego operatora, do podanych modu��w
            Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);
           
			// tu jeste�my zalogowani do O!
			Console.WriteLine( "Jeste�my zalogowani do O!" ); 
			Console.ReadLine();
		}
		// wylogowanie z O!
		protected static void Wylogowanie()
		{
			// niszczymy Login
			Login = null;
			// odblokowanie (wylogowanie) O!
			Application.UnlockApp();
			// niszczymy obiekt Aplikacji
			Application = null;
		}


    	// dla obiekt�w Clarionowych, kt�rych nie mo�na tak �atwo u�ywa� w C#
		protected static void InvokeMethod( object o, string Name, object[] Params )
		{
			o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.InvokeMethod , null, o, Params, null, null, null );
		}
		// dla obiekt�w Clarionowych, kt�rych nie mo�na tak �atwo u�ywa� w C#
		protected static void SetProperty( object o, string Name, object Value )
		{
			if( o == null )
				return;
			o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.SetProperty,  null, o, new object[] {Value} );
		}
		// dla obiekt�w Clarionowych, kt�rych nie mo�na tak �atwo u�ywa� w C#
		protected static object GetProperty( object o, string Name )
		{
			if (o == null)
				return null;
			return o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.GetProperty, null, o, null );
		}

        // Przyk�ad 5. - Dodanie faktury sprzeda�y
		protected static void DodanieFaktury()
		{
			CDNBase.AdoSession Sesja = Login.CreateSession();

			CDNHlmn.DokumentyHaMag Faktury	= (CDNHlmn.DokumentyHaMag)Sesja.CreateObject( "CDN.DokumentyHaMag", null );
			CDNHlmn.IDokumentHaMag Faktura	= (CDNHlmn.IDokumentHaMag)Faktury.AddNew( null );

			CDNBase.ICollection Kontrahenci	= (CDNBase.ICollection)(Sesja.CreateObject( "CDN.Kontrahenci", null ));
           		CDNHeal.IKontrahent	Kontrahent	= (CDNHeal.IKontrahent)Kontrahenci[ "Knt_Kod='ALOZA'" ];

			CDNBase.ICollection FormyPlatnosci  = (CDNBase.ICollection)(Sesja.CreateObject( "CDN.FormyPlatnosci", null ));
			OP_KASBOLib.FormaPlatnosci FPl	= (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[ 1 ];

            CDNHeal.DefinicjeDokumentow DefinicjeDokumentow = (CDNHeal.DefinicjeDokumentow)Sesja.CreateObject("CDN.DefinicjeDokumentow", null);
            CDNHeal.IDefinicjaDokumentu DefinicjaDokumentu = (CDNHeal.DefinicjaDokumentu)DefinicjeDokumentow["Ddf_Symbol='FA_SE'"];

			
			// e_op_Rdz_FS 			302000
			Faktura.Rodzaj = 302000;
			// e_op_KlasaFS			302
			Faktura.TypDokumentu = 302;

			//Ustawiamy bufor
			Faktura.Bufor = 1;

			//Ustawiamy date
			Faktura.DataDok = new DateTime(2008, 10, 13);

			//Ustawiamy form� p�atno�ci
			Faktura.FormaPlatnosci = FPl;

			//Ustawiamy podmiot
			Faktura.Podmiot = (CDNHeal.IPodmiot)Kontrahent;

			//Ustawiamy magazyn
			Faktura.MagazynZrodlowyID =  1;	

            //Ustawiamy numerator
			OP_KASBOLib.INumerator Numerator = (OP_KASBOLib.INumerator)Faktura.Numerator;
		
			Numerator.DefinicjaDokumentu = (OP_KASBOLib.INumerator)DefinicjaDokumentu;
			Numerator.NumerNr = 133;
			Numerator.Rejestr="A1";
            
			//zapisujemy
			Sesja.Save();
            
			Console.WriteLine( "Dodano faktur�: " + Faktura.NumerPelny );
		}


		[STAThread]
		static void Main(string[] args)
		{
			// tu b�dziemy sobie wywo�ywa� nasze przyk�ady.
			try
			{
				LogowanieAutomatyczne();
				DodanieFaktury();
			}
			catch( COMException e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "B��d: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "B��d: " + e.Message );
			}
			catch( Exception e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "B��d: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "B��d: " + e.Message );
			}
			finally
			{
				Wylogowanie();
			}
			Console.ReadLine();
		}

	}
}

