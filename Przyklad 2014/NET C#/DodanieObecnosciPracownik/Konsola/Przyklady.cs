using System;
using System.Runtime.InteropServices;
using CDNBase;

namespace Konsola
{
	// Klasa z przykładami działającymi na konsoli
	class Przyklady
	{

		protected static ApplicationClass	Application = null;
		protected static ILogin				Login		= null;

		// Przyklad 1. - Logowanie do O! bez wyświetlania okienka logowania
		static protected void LogowanieAutomatyczne()
		{
			string			Operator	= "ADMIN";
			string			Haslo		= null;		// operator nie ma hasła
            string          Firma       = "Prezentacja_KH";	// nazwa firmy

			object[] hPar		= new object[] { 
						 1,  0,   0,  0,  1,   1,  0,    0,   0,   0,   0,   0,   0,   0,  1,   0,  0 };	// do jakich modułów się logujemy
			/* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP
			 */

			
			// katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
			System.Environment.CurrentDirectory	= @"C:\Optima";	

			// tworzymy nowy obiekt apliakcji
			Application = new ApplicationClass();
			// blokujemy
			Application.LockApp(513, 5000, null, null, null, null);
			// logujemy się do podanej Firmy, na danego operatora, do podanych modułów
            Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);
           
			// tu jesteśmy zalogowani do O!
			Console.WriteLine( "Jesteśmy zalogowani do O!" ); 
			Console.ReadLine();
		}
		// wylogowanie z O!
		protected static void Wylogowanie()
		{
			// niszczymy Login
			Login = null;
			// odblokowanie (wylogowanie) O!
			Application.UnlockApp();
			// niszczymy obiekt Aplikacji
			Application = null;
		}


    	// dla obiektów Clarionowych, których nie można tak łatwo używać w C#
		protected static void InvokeMethod( object o, string Name, object[] Params )
		{
			o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.InvokeMethod , null, o, Params, null, null, null );
		}
		// dla obiektów Clarionowych, których nie można tak łatwo używać w C#
		protected static void SetProperty( object o, string Name, object Value )
		{
			if( o == null )
				return;
			o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.SetProperty,  null, o, new object[] {Value} );
		}
		// dla obiektów Clarionowych, których nie można tak łatwo używać w C#
		protected static object GetProperty( object o, string Name )
		{
			if (o == null)
				return null;
			return o.GetType().InvokeMember( Name, System.Reflection.BindingFlags.GetProperty, null, o, null );
		}

        enum SposobOdbieraniaNadg
        {
            Brak = 1,
            OBM = 2,  //Nadgodziny do odbioru w bieżącym miesiącu 
            ONM = 3,  //Nadgodziny odbierane w kolejnym miesiącu
            WPL = 4,   //Wolne za nadgodziny (płatne)
            WNP = 5   //Wolne za nadgodziny (niepłatne)
        }

        protected static void DodanieObecnosciPracownik()
        {
            CDNBase.AdoSession Sesja = Login.CreateSession();
            
            //sprawdzam, czy jest modul PK XL
            Boolean CzyJestModulPKXL = (Sesja.Login.get_Moduly(9) == 1) ? true : false;
            
            //dzien, na który wprowadzamy obecnosc
            DateTime Data = new DateTime(2008, 10, 28);

            //Wykreowanie obiektu pracownika o akronimie 'TEST'
            CDNPrac.Pracownicy Pracownicy = (CDNPrac.Pracownicy)Sesja.CreateObject("CDN.Pracownicy", null);
            CDNPrac.IPracownik Pracownik = (CDNPrac.IPracownik)Pracownicy["Pra_Kod = 'TEST'"];
         

            //sprawdzam, czy pracowanik byl zatrudniony na dzien wprowadzania obecnosci
            CDNPrac.IHistoria Historia = Pracownik.GetHistoriaAktualna(Data, 0);
            if ( Historia.Zatrudnienie.Period.ContainDate(Data) == 0
                || Data > Historia.Zatrudnienie.Period.To.AddDays(-1) )
            {
                throw new System.Exception("Dzień poza okresem zatrudnienia.!");
            }

            //czas pracy pracownika
            OP_KALBLib.CzasPracy CzasPracy = (OP_KALBLib.CzasPracy)Pracownik.CzasPracy;
            OP_KALBLib.IDzienPracy DzienPracy;
            try
            {
                DzienPracy = (OP_KALBLib.IDzienPracy)CzasPracy[Data];
                throw new System.Exception("Na dany dzień wprowadzono już zapis!");
                /* Jesli chcemy taki zapis usunac to */
                //CzasPracy.Delete(Data);
            }
            catch(Exception ex) {  /* nieładnie tak zostawiać wyjątek, ale... :) */ }

            //Dodanie dnia
            DzienPracy = (OP_KALBLib.IDzienPracy)CzasPracy.AddNew(Data);

            #region WEJSCIE - 1
            {
                //Ustawienie godzin wejścia/wyjścia
                DzienPracy.OdGodziny_1 = new DateTime(1899, 12, 30, 8, 12, 0);  //kreujemy datę 1899-12-30 z godzina wejscia
                DzienPracy.DoGodziny_1 = new DateTime(1899, 12, 30, 10, 23, 0); //kreujemy datę 1899-12-30 z godzina wyjscia

                //Ustawienie strefy
                OP_KALBLib.Strefa Strefa = (OP_KALBLib.Strefa)Sesja.CreateObject("CDN.Strefy", "DST_Akronim = 'praca.pdst'");
                //OP_KALBLib.IStrefa Strefa = (OP_KALBLib.IStrefa)Strefy[];
                DzienPracy.Strefa_1 = Strefa;

                //sprawdzic, czy jest licencja na duze place
                if (CzyJestModulPKXL)
                {
                    //Ustawienie dzialu
                    CDNPrac.IDzial Dzial = (CDNPrac.IDzial)Sesja.CreateObject("CDNPrac.Dzialy", "DZL_AdresWezla = '1'");
                    //CDNPrac.IDzial Dzial = (CDNPrac.IDzial)Dzialy[];
                    DzienPracy.Dzial_1 = Dzial;

                    //Ustawienie projektu
                    CDNDave.IProjekt Projekt = (CDNDave.IProjekt)Sesja.CreateObject("CDN.Projekty", "PRJ_Wezel = '  3.  1'");
                    //CDNDave.IProjekt Projekt = (CDNDave.IProjekt)Projekty[];
                    DzienPracy.Projekt_1 = Projekt;

                    //Ustawienie sposobu odbioru nadgodzin
                    DzienPracy.OdbNadg_1 = (int)SposobOdbieraniaNadg.Brak;

                }
            }
            #endregion WEJSCIE - 1


            #region WEJSCIE - 2
            {
                //sprawdzic, czy jest licencja na duze place
                if (CzyJestModulPKXL)
                {
                    //Ustawienie godzin wejścia/wyjścia
                    DzienPracy.OdGodziny_2 = new DateTime(1899, 12, 30, 8, 12, 0);  //kreujemy datę 1899-12-30 z godzina wejscia
                    DzienPracy.DoGodziny_2 = new DateTime(1899, 12, 30, 10, 23, 0); //kreujemy datę 1899-12-30 z godzina wyjscia

                    //Ustawienie strefy
                    OP_KALBLib.Strefa Strefa = (OP_KALBLib.Strefa)Sesja.CreateObject("CDN.Strefy", "DST_Akronim = 'praca.pdst'");
                    //OP_KALBLib.IStrefa Strefa = (OP_KALBLib.IStrefa)Strefy[];
                    DzienPracy.Strefa_2 = Strefa;

                    //Ustawienie dzialu
                    CDNPrac.IDzial Dzial = (CDNPrac.IDzial)Sesja.CreateObject("CDNPrac.Dzialy", "DZL_AdresWezla = '1'");
                    //CDNPrac.IDzial Dzial = (CDNPrac.IDzial)Dzialy[];
                    DzienPracy.Dzial_2 = Dzial;

                    //Ustawienie projektu
                    CDNDave.IProjekt Projekt = (CDNDave.IProjekt)Sesja.CreateObject("CDN.Projekty", "PRJ_Wezel = '  3.  1'");
                    //CDNDave.IProjekt Projekt = (CDNDave.IProjekt)Projekty[];
                    DzienPracy.Projekt_2 = Projekt;

                    //Ustawienie sposobu odbioru nadgodzin
                    DzienPracy.OdbNadg_2 = (int)SposobOdbieraniaNadg.Brak;

                }
            }
            #endregion WEJSCIE - 2
            //Zapis zmian
            Sesja.Save();
        }


		[STAThread]
		static void Main(string[] args)
		{
			// tu będziemy sobie wywoływać nasze przykłady.
			try
			{
				LogowanieAutomatyczne();
                DodanieObecnosciPracownik();
			}
			catch( COMException e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "Błąd: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "Błąd: " + e.Message );
			}
			catch( Exception e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "Błąd: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "Błąd: " + e.Message );
			}
			finally
			{
				Wylogowanie();
			}
			Console.ReadLine();
		}
	}
}

