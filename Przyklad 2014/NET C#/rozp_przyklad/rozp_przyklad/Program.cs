﻿using System;
using System.Text;
using CDNBase;
using System.Runtime.InteropServices;
using OP_IMPEXPLib;
using CDNLib;
using CDNBaseW;

namespace rozp_przyklad
{
    class Przyklad
    {
        protected static ApplicationClass Application = null;
        protected static ILogin Login = null;

        static protected bool LogowanieAutomatyczne(String firma)
        {
            string Operator = "ADMIN";
            string Haslo = null;		// operator nie ma hasła
            string Firma = firma;	// nazwa firmy

            object[] hPar = new object[] { 
						 1,  1,   1,  1,  1,   1,  1,    1,   1,   1,   1,   1,   1,  1,   1,   1,  1 };	// do jakich modułów będzie logowanie
            /* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP  
             */

            // katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
            System.Environment.CurrentDirectory = @"D:\Optima";

            // tworzenie obiektu apliakcji
            Application = new ApplicationClass();
            // blokowanie
            Application.LockApp(513, 5000, null, null, null, null);
            // logowanie do podanej Firmy, na danego operatora, do podanych modułów

            try
            {
                Console.WriteLine("Logowanie...");
                Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);
                Console.WriteLine("Prawidowe zalogowanie");
                return true;
            }
            catch
            {
                Console.WriteLine("Logowanie zakończono niepowodzeniem.");
                return false;
            }
            // tu jestemy zalogowani do O!            
        }

        protected static void Wylogowanie()
        {
            // niszczymy Login
            Login = null;
            // odblokowanie (wylogowanie) O!
            Application.UnlockApp();
            // niszczymy obiekt Aplikacji
            Application = null;
        }

        protected static void Export()
        {
            //Stworzenie nowej sesji            
            CDNBase.AdoSession Sesja = Login.CreateSession();

            //utworzenie obiektu eksportu i import                                   
            IOffLineExpImp OffLineExpImp = (OP_IMPEXPLib.OffLineExpImp)Sesja.CreateObjectEx("CDN.OffLineExpImp", "", "");

            // utworzenie obiektów konfiguracyjnych
            ExpImpConfig ExpImpCfg = (OP_IMPEXPLib.ExpImpConfig)Sesja.CreateObjectEx("CDN.ExpImpConfig", "", "");
            IKonfBazy knf = (OP_IMPEXPLib.KonfBazy)Sesja.CreateObjectEx("CDN.KonfiguracjaBaz", "UKn_BazID = " + Login.Firm.ID.ToString(), "");

            // podstawienie obiektu z konfiguracj exportu/importu
            OffLineExpImp.ExpImpCfg = ExpImpCfg;

            //ustawienie typu bazy w obiekcie eksportu i importu
            ExpImpCfg.TypBazy = knf.RodzajBazy;

            //utworzenie obiektu okienka z logami
            _IProgressEvents Progress = (_IProgressEvents)new ProgressClass();

            //ustawienie okienka z logami w obiekcie importu eksportu
            OffLineExpImp.AdviseProgress(Progress);

            //ustawienie flagi czy uwzględniać zakres dat
            ExpImpCfg.WithDate = 0; // 1 tak , 0 nie;

            //ustawienie daty pocztkowej zakresu
            ExpImpCfg.StartDate = new DateTime(2007, 1, 1);

            //ustawienie daty końcowej zakresu
            ExpImpCfg.EndDate = new DateTime(2007, 12, 1);

            //ustawienie ścieki do pliku xml do którego zostaną wyeksportowane dane
            ExpImpCfg.FileName = "C:\\Documents and Settings\\Przemyslaw.Biernat\\Pulpit\\slow.xml";

            //ustawienie flagi czy eksportować tylko nowe i zmienione
            ExpImpCfg.OnlyNew = 1; // 1 - tak , 0 nie;

            //ustawienie flagi czy tworzyć okienko z logiem
            ExpImpCfg.WithLog = 0;

            //nie eksportujemy do serwera, tylko do pliku
            OffLineExpImp.WithServer = 0;

            // zainicjowanie wewnętrznych struktur obiektu eksportu
            OffLineExpImp.InitDoc();

            /*      Ustawienie poszczególnych skadników eksportu
                    pierwszy parametr:
               
                    0 - Atrybuty , 1 - Definicje dokumentow, 2 - Kategorie, 3 - Banki, 4 = Urzedy, 5 - Pracownicy, 6 - Wspólnicy,  
					7 - Rachunki, 8 - Formy patnosci, 9 - Kontrahenci, 10 - Waluty, 11 - Kursy walut, 12 - Notowania, 
					13 - Faktury sprzedaży, 14 - Faktury zakupu, 15 - Faktury wewnętrzne sprzedazy, 16 - Faktury wewnetrzne zakupu, 
					17 - Rejestry sprzedaży, 18 - Rejestry zakupu, 19 - Dokumenty TaxFree, 20 - Dokumenty inne przychodowe, 21 - Dokumenty inne kosztowe, 
					22 - Ewidencja dodatkowa przychodowa, 23 - Ewidencja dodatkowa kosztowa, 24 - Raporty kasowe, 25 - Płatności, 26 - Rozliczenia 
             
                    drugi parametr:
                    1 - eksportuj
                    0 - nie eksportuj
             */

            OffLineExpImp.SetItem(0, 1);
            OffLineExpImp.SetItem(1, 1);
            OffLineExpImp.SetItem(2, 1);
            OffLineExpImp.SetItem(3, 1);
            //OffLineExpImp.SetItem(4, 0);
            //OffLineExpImp.SetItem(5, 0);
            //OffLineExpImp.SetItem(6, 0);
            //OffLineExpImp.SetItem(7, 0);
            //OffLineExpImp.SetItem(8, 0);
            //OffLineExpImp.SetItem(9, 0);
            //OffLineExpImp.SetItem(10, 0);
            //OffLineExpImp.SetItem(11, 0);
            //OffLineExpImp.SetItem(12, 0);
            //OffLineExpImp.SetItem(17, 0);
            //OffLineExpImp.SetItem(18, 0);
            //OffLineExpImp.SetItem(20, 0);
            //OffLineExpImp.SetItem(21, 0);
            //OffLineExpImp.SetItem(24, 0);
            //OffLineExpImp.SetItem(25, 0);
            //OffLineExpImp.SetItem(26, 0);

            //rozpoczcie eksportu
            OffLineExpImp.Eksport();

           OffLineExpImp.UnAdviseProgress();

        }

        protected static void Import()
        {
            //Stworzenie nowej sesji
            CDNBase.AdoSession Sesja = Login.CreateSession();

            //utworzenie obiektu eksportu i import                                   
            IOffLineExpImp OffLineExpImp = (OP_IMPEXPLib.OffLineExpImp)Sesja.CreateObjectEx("CDN.OffLineExpImp", "", "");

            // utworzenie obiektów konfiguracyjnych
            ExpImpConfig ExpImpCfg = (OP_IMPEXPLib.ExpImpConfig)Sesja.CreateObjectEx("CDN.ExpImpConfig", "", "");
            IKonfBazy knf = (OP_IMPEXPLib.KonfBazy)Sesja.CreateObjectEx("CDN.KonfiguracjaBaz", "UKn_BazID = " + Login.Firm.ID.ToString(), "");

            // podstawienie obiektu z konfiguracj exportu/importu
            OffLineExpImp.ExpImpCfg = ExpImpCfg;

            //ustawienie typu bazy w obiekcie eksportu i importu
            ExpImpCfg.TypBazy = knf.RodzajBazy;

            //utworzenie obiektu okienka z logami
            _IProgressEvents Progress = (_IProgressEvents)new ProgressClass();

            //ustawienie okienka z logami w obiekcie importu eksportu
            OffLineExpImp.AdviseProgress(Progress);

            //ustawienie flagi czy uwzględniać zakres dat
            ExpImpCfg.WithDate = 0; // 1 tak , 0 nie;

            //ustawienie daty początkowej zakresu
            ExpImpCfg.StartDate = new DateTime(2007, 1, 1);

            //ustawienie daty końcowej zakresu
            ExpImpCfg.EndDate = new DateTime(2007, 12, 1);

            //ustawienie cieki do pliku xml z którego zostaną zaimportowane dane
            ExpImpCfg.FileName = "C:\\Documents and Settings\\Przemyslaw.Biernat\\Pulpit\\slow.xml";

            //ustawienie flagi czy eksportować "dotd nie eksportowane i zmienione"
            ExpImpCfg.OnlyNew = 0; // 1 - tak , 0 nie;

            //ustawienie flagi czy tworzyć okienko z logami
            ExpImpCfg.WithLog = 0;

            //nie importujemy z serwera
            OffLineExpImp.WithServer = 0;

            // zainicjowanie wewnętrznych struktur obiektu eksportu
            OffLineExpImp.InitDoc();

            /*      Ustawienie poszczególnych skadników eksportu
                    pierwszy parametr:
               
                    0 - Atrybuty , 1 - Definicje dokumentów, 2 - Kategorie, 3 - Banki, 4 = Urzedy, 5 - Pracownicy, 6 - Wspólnicy,  
					7 - Rachunki, 8 - Formy płatnosci, 9 - Kontrahenci, 10 - Waluty, 11 - Kursy walut, 12 - Notowania, 					
					17 - Rejestry sprzedaży, 18 - Rejestry zakupu, 20 - Dokumenty inne przychodowe, 21 - Dokumenty inne kosztowe, 
					24 - Raporty kasowe, 25 - Płatności, 26 - Rozliczenia 
             
                    drugi parametr:
                    1 - eksportuj
                    0 - nie eksportuj
            */

            OffLineExpImp.SetItem(0, 1);
            OffLineExpImp.SetItem(1, 1);
            OffLineExpImp.SetItem(2, 1);
            OffLineExpImp.SetItem(3, 1);
            //OffLineExpImp.SetItem(4, 0);
            //OffLineExpImp.SetItem(5, 0);
            //OffLineExpImp.SetItem(6, 0);
            //OffLineExpImp.SetItem(7, 0);
            //OffLineExpImp.SetItem(8, 0);
            //OffLineExpImp.SetItem(9, 0);
            //OffLineExpImp.SetItem(10, 0);
            //OffLineExpImp.SetItem(11, 0);
            //OffLineExpImp.SetItem(12, 0);
            //OffLineExpImp.SetItem(17, 0);
            //OffLineExpImp.SetItem(18, 0);
            //OffLineExpImp.SetItem(20, 0);
            //OffLineExpImp.SetItem(21, 0);
            //OffLineExpImp.SetItem(24, 0);
            //OffLineExpImp.SetItem(25, 0);
            //OffLineExpImp.SetItem(26, 0);

            //rozpoczcie eksportu            
            OffLineExpImp.Import();

            OffLineExpImp.UnAdviseProgress();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Wybierz opcję:");
            Console.WriteLine("1 - Export");
            Console.WriteLine("2 - Import");
            ConsoleKeyInfo key;
            key = Console.ReadKey();
            Console.WriteLine("");
            if (key.KeyChar == '1')
            {
                if (LogowanieAutomatyczne("Prezentacja_KH"))
                {
                    Console.WriteLine("Export z firmy Prezentacja_KH");
                    Export();
                    Wylogowanie();
                    Console.WriteLine("Export zakończony");
                }
            }
            if (key.KeyChar == '2')
            {
                if (LogowanieAutomatyczne("Demo"))
                {
                    Console.WriteLine("Import firmy Demo");
                    Import();
                    Wylogowanie();
                    Console.WriteLine("Import zakończony");
                }
            }

            Console.WriteLine("Naciśnij dowolny klawisz");
            key = Console.ReadKey();
        }
    }
}
