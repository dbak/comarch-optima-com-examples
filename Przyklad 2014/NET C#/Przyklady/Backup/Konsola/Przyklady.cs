using System;
using CDNBase;
using OP_CSRSLib;
using System.Runtime.InteropServices;
using System.Text;

namespace Konsola
{
	// Klasa z przyk�adami dzia�aj�cymi na konsoli
	class Przyklady
	{

		protected static ApplicationClass	Application = null;
		protected static ILogin				Login		= null;

#region
		// Przyklad 1. - Logowanie do O! bez wy�wietlania okienka logowania
		static protected void LogowanieAutomatyczne()
		{
			string			Operator	= "ADMIN";
			string			Haslo		= "";		// operator nie ma has�a
			string			Firma		= "DEMO";	// nazwa firmy

			object[] hPar		= new object[] { 
						 1,  0,   0,  0,  1,   1,  0,    0,   0,   0,   0,   0,   1,   1,  1,   0,  0 };	// do jakich modu��w si� logujemy
			/* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP
			 */
			
			// katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadzia�a, chyba �e program odpalimy z katalogu O!)
			System.Environment.CurrentDirectory	= @"D:\Optima";	

			// tworzymy nowy obiekt apliakcji
			Application = new ApplicationClass();
			// blokujemy
            Login =  Application.LockApp(1, 5000, null, null, null, null);

			// logujemy si� do podanej Firmy, na danego operatora, do podanych modu��w

            Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5], hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15], hPar[16]);
           
			// tu jeste�my zalogowani do O!
			Console.WriteLine( "Jeste�my zalogowani do O!" ); 
			Console.ReadLine();
		}
		// wylogowanie z O!
		protected static void Wylogowanie()
		{
			// niszczymy Login
			Login = null;
			// odblokowanie (wylogowanie) O!
			Application.UnlockApp();
			// niszczymy obiekt Aplikacji
			Application = null;
		}

		// Przyk�ad 2. - Dodanie dokumentu rejestru VAT
		protected static void DodanieRejestru()
		{
			// Tworzymy obiekt sesji
			AdoSession Sesja = Login.CreateSession();
			
			// tworzenie potrzebnych kolekcji
			CDNBase.ICollection FormyPlatnosci  = (CDNBase.ICollection)(Sesja.CreateObject( "CDN.FormyPlatnosci", null ));
			CDNBase.ICollection Waluty			= (CDNBase.ICollection)(Sesja.CreateObject( "CDN.Waluty", null ));
			CDNBase.ICollection RejestryVAT		= (CDNBase.ICollection)(Sesja.CreateObject( "CDN.RejestryVAT", null ));
			CDNBase.ICollection Kontrahenci		= (CDNBase.ICollection)(Sesja.CreateObject( "CDN.Kontrahenci", null ));
			// pobieranie kontrahenta, formy platnosci  i waluty
			CDNHeal.IKontrahent	Kontrahent	= (CDNHeal.IKontrahent)Kontrahenci[ "Knt_Kod='ALOZA'" ];
			// w konfiguracji jest tylko jedna waluta (PLN)
			//			CDNHeal.Waluta Waluta			= (CDNHeal.Waluta)Waluty[ "WNa_Symbol='EUR'" ];
			OP_KASBOLib.FormaPlatnosci FPl	= (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[ 1 ];
			// utworzenie nowego obiektu rejestru VAT
			CDNRVAT.VAT	RejestrVAT			= (CDNRVAT.VAT)RejestryVAT.AddNew( null );
			//ustawianie parametr�w rejestru
			RejestrVAT.Typ = 2;//	1 - zakupu; 2 - sprzeda�y
			Console.WriteLine( "Typ ustawiony" );
			
			RejestrVAT.Rejestr = "SPRZEDA�";	// nazwa rejestru
			Console.WriteLine( "Rejestr ustawiony" );
			
			RejestrVAT.Dokument = "DET01/05/2007";	
			Console.WriteLine( "Dokument ustawiony" );
			
			RejestrVAT.IdentKsieg = "2007/05/28-oop";
			Console.WriteLine( "IdentKsieg ustawiony" );

			RejestrVAT.DataZap = new DateTime(2007, 05, 28);
			Console.WriteLine( "DataZap ustawiona" );
			
			RejestrVAT.FormaPlatnosci = FPl;
			Console.WriteLine( "Forma platnosci ustawiona" );
			
			RejestrVAT.Podmiot = (CDNHeal.IPodmiot)Kontrahent;
			Console.WriteLine( "Podmiot ustawiony" );
			// kategoria ustawia si� sama, gdy ustawiany jest kontrahent
			
			// waluty nie ustawiam, bo w konf. jest na razie tylko jedna waluta (PLN)
			//			RejestrVAT.WalutaDoVAT = Waluta;	
			//			Console.WriteLine( "Waluta ustawiona " + RejestrVAT.Waluta.Symbol );
			
			// dodanie element�w rejestru VAT
			DodajElementyDoRejestru( RejestrVAT );

			// zapisanie zmian
			Sesja.Save();
		}

		protected static void DodajElementyDoRejestru( CDNRVAT.VAT RejestrVAT )
		{
			// pobranie kolekcji element�w rejestru
			CDNBase.ICollection Elementy = RejestrVAT.Elementy;
			Console.WriteLine( "Dodawanie elementow: " );
			// dodanie element�w kolejno o stawkach 0%, 3%, 5%, 7%, 22% i zwolnionej
			DodajJeden( 0,  (CDNRVAT.VATElement)Elementy.AddNew( null ) );
			DodajJeden( 3,  (CDNRVAT.VATElement)Elementy.AddNew( null ) );
			DodajJeden( 5,  (CDNRVAT.VATElement)Elementy.AddNew( null ) );
			DodajJeden( 7,  (CDNRVAT.VATElement)Elementy.AddNew( null ) );
			DodajJeden( 22, (CDNRVAT.VATElement)Elementy.AddNew( null ) );
			DodajJeden( -1, (CDNRVAT.VATElement)Elementy.AddNew( null ) );
		}

		protected static void DodajJeden( int Stawka, CDNRVAT.VATElement Element )
		{
			Console.WriteLine( "\tDodaj� element:" );
			if( Stawka == -1 )
			{	
				// dodanie elementu o stawce zwolnionej z VAT
				Element.Flaga = 1;
				Element.Stawka = 0;
			}
			else
				Element.Stawka = Stawka;
			
			// Element.Flaga = 4;	// Flaga=4 oznacza stwk� NP.
			Console.WriteLine( "\tStawka ustawiona" );

			/*
			 * Rodzaje zakup�w:
			 * Towary		 = 1;
			 * Inne			 = 2;
			 * Trwale		 = 3;
			 * Uslugi		 = 4;
			 * NoweSrTran	 = 5;
			 * Nieruchomosci = 6;
			 * Paliwo		 = 7;
			 */
			Element.RodzajZakupu = 1;
			Console.WriteLine( "\tRodzaj zakupu ustawiony" );

			Element.Netto = 23.57m; // m na ko�cu oznacza typ decimal
			Console.WriteLine( "\tNetto ustawione" );
			
			/* Porperty Odliczenia:
			 *  dla rej zakupu(odliczenia):		0-nie, 1-tak, 2-warunkowo
			 *	dla rej sprzed(uwz. w prop):	0-nie uwzgledniaj, 1-Uwzgledniaj w proporcji, 2- tylko w mianowniku
			 */
			Element.Odliczenia = 0;	 
			Console.WriteLine( "\tOdliczenia ustawione\n" );
		}

		public enum WydrukiUrzadzeniaEnum	// rodzaje urz�dze� do wydruku
		{
			e_rpt_Urzadzenie_Ekran = 1,
			e_rpt_Urzadzenie_DrukarkaDomyslna = 2,
			e_rpt_Urzadzenie_DrukarkaInna = 3,
			e_rpt_Urzadzenie_SerwerWydrukow = 4
		}


		// Przyk�ad 3. - Dodanie nowego kontrahenta
		protected static void DodajKontrahenta()
		{
			CDNBase.AdoSession Sesja = Login.CreateSession();

			OP_KASBOLib.Banki Banki			= (OP_KASBOLib.Banki)Sesja.CreateObject( "CDN.Banki", null );
			CDNHeal.Kategorie Kategorie		= (CDNHeal.Kategorie)Sesja.CreateObject( "CDN.Kategorie", null );

			CDNHeal.Kontrahenci Kontrahenci = (CDNHeal.Kontrahenci)Sesja.CreateObject( "CDN.Kontrahenci", null );
			CDNHeal.IKontrahent Kontrahent	= (CDNHeal.IKontrahent)Kontrahenci.AddNew( null );

			CDNHeal.IAdres		Adres		= Kontrahent.Adres;
			CDNHeal.INumerNIP	NumerNIP	= Kontrahent.NumerNIP;

			Adres.Gmina			= "Kozia W�lka";
			Adres.KodPocztowy	= "20-835";
			Adres.Kraj			= "Polska";
			Adres.NrDomu		= "17";
			Adres.Miasto		= "Kozia W�lka";
			Adres.Powiat		= "Lukrecjowo";
			Adres.Ulica			= "�liczna";
			Adres.Wojewodztwo	= "Kujawsko-Pomorskie";

			NumerNIP.UstawNIP( "PL", "281-949-89-45", 1 );

			Kontrahent.Akronim	= "NOWY";
			Kontrahent.Email	= "nowy@nowy.eu";
			Kontrahent.Medialny = 0;
			Kontrahent.Nazwa1	= "Nowy kontrahent";
			Kontrahent.Nazwa2	= "dodany z C#";
			string Nazwa = Kontrahent.NazwaPelna;
			
			Kontrahent.Bank		= Banki[ "BNa_BNaID=2" ];
			Kontrahent.Kategoria = (CDNHeal.Kategoria)Kategorie[ "Kat_KodSzczegol='ENERGIA'" ];
			
			Sesja.Save();

			Console.WriteLine( "Nazwa dodanego kotrahenta: " + Nazwa );
		}

		// Przyk�ad 4. - Dodanie towaru
		protected static void DodanieTowaru()
		{
			CDNBase.AdoSession Sesja = Login.CreateSession();
            
			CDNTwrb1.Towary Towary	= (CDNTwrb1.Towary)Sesja.CreateObject( "CDN.Towary", null );
			CDNTwrb1.ITowar Towar	= (CDNTwrb1.ITowar)Towary.AddNew( null );

			Towar.Nazwa		= "Nowy towar dodany z C#";
			string nazwa = Towar.Nazwa1;
			Towar.Kod		= "NOWY_C#";

			Towar.Stawka	= 22.00m;
			Towar.Flaga 	= 2; // 2- oznacza stawk� opodatkowan�, pozosta�e warto�ci 
					     // pola s� podane w strukturze bazy tabela: [CDN.Towary].[Twr_Flaga]

			Towar.JM		= "SZT";
			
			Sesja.Save();
			Console.WriteLine( "Towar dodany: " + nazwa );
		}

		// Przyk�ad 5. - Dodanie faktury sprzeda�y
		protected static void DodanieFaktury()
		{
			CDNBase.AdoSession Sesja = Login.CreateSession();

			CDNHlmn.DokumentyHaMag Faktury	= (CDNHlmn.DokumentyHaMag)Sesja.CreateObject( "CDN.DokumentyHaMag", null );
			CDNHlmn.IDokumentHaMag Faktura	= (CDNHlmn.IDokumentHaMag)Faktury.AddNew( null );

			CDNBase.ICollection Kontrahenci	= (CDNBase.ICollection)(Sesja.CreateObject( "CDN.Kontrahenci", null ));
           		CDNHeal.IKontrahent	Kontrahent	= (CDNHeal.IKontrahent)Kontrahenci[ "Knt_Kod='ALOZA'" ];

			CDNBase.ICollection FormyPlatnosci  = (CDNBase.ICollection)(Sesja.CreateObject( "CDN.FormyPlatnosci", null ));
			OP_KASBOLib.FormaPlatnosci FPl	= (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[ 1 ];
			
			// e_op_Rdz_FS 			302000
			Faktura.Rodzaj = 302000;
			// e_op_KlasaFS			302
			Faktura.TypDokumentu = 302;

			//Ustawiamy bufor
			Faktura.Bufor = 0;

			//Ustawiamy date
			Faktura.DataDok = new DateTime(2007, 06, 04);

			//Ustawiamy form� p�atno�ci
			Faktura.FormaPlatnosci = FPl;

			//Ustawiamy podmiot
			Faktura.Podmiot = (CDNHeal.IPodmiot)Kontrahent;

			//Ustawiamy magazyn
			Faktura.MagazynZrodlowyID =  1;	

			//Dodajemy pozycje
			CDNBase.ICollection Pozycje = Faktura.Elementy;
			CDNHlmn.IElementHaMag Pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew( null );
			Pozycja.TowarKod = "NOWY_C#";
			Pozycja.Ilosc = 2;
			//Pozycja.Cena0WD = 10;
            Pozycja.WartoscNetto = Convert.ToDecimal("123,13");
			//zapisujemy
			Sesja.Save();

			Console.WriteLine( "Dodano faktur�: " + Faktura.NumerPelny );
		}
		// Przyk�ad 6. - Korekta FA
		protected static void DogenerujKorekteFA()
		{

			CDNBase.AdoSession Sesja = Login.CreateSession();

			CDNHlmn.DokumentyHaMag Faktury	= (CDNHlmn.DokumentyHaMag)Sesja.CreateObject( "CDN.DokumentyHaMag", null );
			CDNHlmn.IDokumentHaMag Faktura	= (CDNHlmn.IDokumentHaMag)Faktury.AddNew( null );

			// FAKI 302001; 
			// FAKW 302002; 
			// FAKV 302003;

			//ustawiamy rodzaj i typ dokumentu
			Faktura.Rodzaj  = 302001;
			Faktura.TypDokumentu = 302;
  
			//Rodzaj korekty
			Faktura.Korekta          = 1;
			//Bufor
			Faktura.Bufor            = 1;
			//Id korygowanej faktury
			Faktura.OryginalID       = 1;

			//Zmieniamy ilosc
			CDNBase.ICollection Pozycje = Faktura.Elementy;
			CDNHlmn.IElementHaMag Pozycja = (CDNHlmn.IElementHaMag)Pozycje[ "Tre_TwrKod='NOWY_C#'" ];
			Pozycja.Ilosc = 1;

			//Zapisujemy
			Sesja.Save();

			Console.WriteLine( "Dogenerowano korekt�: " + Faktura.NumerPelny);
		}
		// Przyk�ad 7. - Dogenerowanie WZ do FA
		protected static void Dogenerowanie_WZ_Do_FA()
		{

			CDNBase.AdoSession Sesja = Login.CreateSession();
			//ADODB.Connection cnn	= new ADODB.ConnectionClass();
			ADODB.Connection cnn = Sesja.Connection;
			//cnn.Open( cnn.ConnectionString, "", "", 0);


			ADODB.Recordset rRs		= new ADODB.RecordsetClass();
			string select			= "select top 1 TrN_TrNId as ID from cdn.TraNag where TrN_Rodzaj = 302000 ";

			rRs.Open( select, cnn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1 );
		
			// Wykreowanie obiektu serwisu:
            		CDNHlmn.SerwisHaMag Serwis =  (CDNHlmn.SerwisHaMag)Sesja.CreateObject("Cdn.SerwisHaMag",null);

			// Samo gemerowanie dok. FA
			int MAGAZYN_ID = 1;
			Serwis.AgregujDokumenty(302, rRs, MAGAZYN_ID, 306,null); 

			//Zapisujemy
			Sesja.Save();
        }
#endregion


        #region Przyk�ad 8 - Dodanie zlecenia serwisowego
        #region COM_DOK
        /*<COM_DOK>
<OPIS>Dodanie zlecenia serwisowego</OPIS> 
<Uruchomienie>Przyk�ad dzia�a na bazie DEMO</Uruchomienie>
<Interfejs> ISrsZlecenie </<Interfejs>
<Interfejs> ISrsCzynnosc </<Interfejs>
<Interfejs> ISrsCzesc </<Interfejs>
<Osoba>WN</Osoba> 
<Jezyk>C#</Jezyk> 
<OPT_VER>17.3.1</OPT_VER>
</COM_DOK>*/
        #endregion


        public static void DodanieZleceniaSerwisowego()
        {
            try
            {
                Console.WriteLine("##### dodawanie zlecenia serwisowego #####");
                Console.WriteLine("Kreuj� sesj� ...");
                CDNBase.AdoSession sesja = Login.CreateSession();
                Console.WriteLine("Kreuj� kolekcj� zlece� serwisowych ...");

                OP_CSRSLib.SrsZlecenia zlecenia = (OP_CSRSLib.SrsZlecenia)sesja.CreateObject("CDN.SrsZlecenia", null);
                Console.WriteLine("Dodaj� nowe zlecenie do kolekcji zlece� serwisowych ...");
                OP_CSRSLib.ISrsZlecenie zlecenie = (OP_CSRSLib.ISrsZlecenie)zlecenia.AddNew(null);

                Console.WriteLine("Kreuj� kolekcje kontrahent�w ...");
                CDNBase.ICollection kontrahenci = (CDNBase.ICollection)(sesja.CreateObject("CDN.Kontrahenci", null));
                Console.WriteLine("I pobieram z niej obiekt kontrahenta o kodzie 'ALOZA' ...");
                CDNHeal.IKontrahent kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];

                Console.WriteLine("Teraz obiekt kontrahenta podstawiam do property Podmiot dla zlecenia ...");
                zlecenie.Podmiot = (CDNHeal.IPodmiot)kontrahent;

                Console.WriteLine("Dzisiejsz� dat� podstawiam jako dat� utworzenia zlecenia...");
                zlecenie.DataDok = DateTime.Now;

                Console.WriteLine("Pobieram kolekcj� czynno�ci przypisanych do zlecenia...");
                CDNBase.ICollection czynnosci = zlecenie.Czynnosci;
                Console.WriteLine("I dodaj� do niej nowy obiekt...");
                OP_CSRSLib.ISrsCzynnosc czynnosc = (OP_CSRSLib.ISrsCzynnosc)czynnosci.AddNew(null);

                Console.WriteLine("Przypisuj� us�ug� o kodzie PROJ_ZIELENI do tej czynno�ci...");
                czynnosc.TwrId = GetIdTowaru(sesja, "PROJ_ZIELENI");
                Console.WriteLine("Ilo�� jednostek ustalam na dwie...");
                czynnosc.Ilosc = 2;
                czynnosc.CenaNetto = 100;
                czynnosc.CzasTrwania = new DateTime(1899, 12, 30, 12, 0, 0);   //12 godzin
                czynnosc.KosztUslugi = 48;

                Console.WriteLine("Teraz dodaj� cz�ci ...");
                CDNBase.ICollection czesci = zlecenie.Czesci;
                Console.WriteLine("I dodaj� do niej nowy obiekt...");
                OP_CSRSLib.ISrsCzesc czesc = (OP_CSRSLib.ISrsCzesc)czesci.AddNew(null);

                Console.WriteLine("Przypisuj� towar o kodzie IGLAKI_CYPRYS ...");
                czesc.TwrId = GetIdTowaru(sesja, "IGLAKI_CYPRYS");
                Console.WriteLine("Ilo�� jednostek ustalam na trzy...");
                czesc.Ilosc = 3;
                czesc.CenaNetto = 99.80m;
                czesc.Fakturowac = 1; //do fakturowania

                Console.WriteLine("Przypisuj� towar o kodzie ZIEMIA_5 ...");
                czesc.TwrId = GetIdTowaru(sesja, "ZIEMIA_5");
                Console.WriteLine("Ilo�� jednostek ustalam na pi��...");
                czesc.Ilosc = 5;
                czesc.CenaNetto = 4.90m;
                czesc.Fakturowac = 1; //do fakturowania


                Console.WriteLine("Zapisuj� sesj�...");
                sesja.Save();
                zlecenie = (OP_CSRSLib.ISrsZlecenie)zlecenia[String.Format("SrZ_SrZId={0}", zlecenie.ID)];
                Console.WriteLine("Dodano zlecenie: {0}\nCzas trwania czynno�ci: {1}:{2}\nKoszt: {3}\nWarto�� netto w cenach sprzeda�y: {4}\nWarto�� netto do zafakturowania : {5}",
                    zlecenie.NumerPelny,
                    zlecenie.CzynnosciCzasTrwania / 100,
                    (zlecenie.CzynnosciCzasTrwania % 100).ToString("00"),
                    zlecenie.Koszt.ToString("#0.00"),
                    zlecenie.WartoscNetto.ToString("#0.00"),
                    zlecenie.WartoscNettoDoFA.ToString("#0.00"));
            }
            catch (COMException comError)
            {
                Console.WriteLine("###ERROR### Dodanie zlecenia nie powiod�o si�!\n{0}", ErrorMessage(comError));
            }
        }

        private static int GetIdTowaru(CDNBase.AdoSession sesja, string kod)
        {
            if (String.IsNullOrEmpty(kod)) return 0;
            int Id = 0;
            Id = Convert.ToInt32(GetSingleValue(sesja, String.Format("Select IsNull(Twr_TwrId, 0) From cdn.Towary Where Twr_Kod = '{0}'", kod), false));
            return Id;
        }

        private static object GetSingleValue(CDNBase.AdoSession sesja, string query, bool configCnn)
        {
            ADODB.Connection cn = configCnn ? sesja.ConfigConnection : sesja.Connection;
            ADODB.Recordset rs = new ADODB.RecordsetClass();
            rs.Open(query, cn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);
            if (rs.RecordCount > 0)
                return rs.Fields[0].Value;
            else
                return null;
        }


        protected static string ErrorMessage(System.Exception e)
        {
            StringBuilder mess = new StringBuilder();
            if (e != null)
            {
                mess.Append(e.Message);
                while (e.InnerException != null)
                {
                    mess.Append(e.InnerException.Message);
                    e = e.InnerException;
                }
            }
            return mess.ToString();
        }
        #endregion Przyk�ad 8 - Dodanie zlecenia serwisowaego

        #region Przyk�ad 9 - Dodanie dokumentu OBD
        #region COM_DOK
        /*<COM_DOK>
<OPIS>Dodanie dokumentu OBD</OPIS> 
<Uruchomienie>Przyk�ad dzia�a na bazie DEMO. Wykorzystano funkcj� SetProperty, kt�ra ju� znajduje sie w pliku przyklady.cs</Uruchomienie>
<Interfejs> IDokNag </<Interfejs>
<Interfejs> IKontrahent </<Interfejs>
<Interfejs> INumerator </<Interfejs>
<Osoba>WN</Osoba> 
<Jezyk>C#</Jezyk> 
<OPT_VER>17.3.1</OPT_VER>
</COM_DOK>*/
        #endregion

        public static void DodanieDokumentuOBD()
        {
            try
            {
                Console.WriteLine("##### dodawanie dokumentu OBD #####");
                Console.WriteLine("Kreuj� sesj� ...");
                CDNBase.AdoSession sesja = Login.CreateSession();
                Console.WriteLine("Kreuj� kolekcj� dokument�w ...");
                OP_SEKLib.DokNagColl dokumenty = (OP_SEKLib.DokNagColl)sesja.CreateObject("CDN.DokNagColl", null);
                Console.WriteLine("Dodaj� nowy dokument do kolekcji ...");
                OP_SEKLib.IDokNag dokument = (OP_SEKLib.IDokNag)dokumenty.AddNew(null);
                dokument.Typ = 1;  //firmowy 

                //je�li seria dokumentu ma by� inna ni� domy�lna
                const string symbolDD = "AUTO";
                CDNHeal.DefinicjeDokumentow definicjeDokumentow = (CDNHeal.DefinicjeDokumentow)sesja.CreateObject("CDN.DefinicjeDokumentow", null);
                CDNHeal.DefinicjaDokumentu definicjaDokumentu = (CDNHeal.DefinicjaDokumentu)definicjeDokumentow[string.Format("Ddf_Symbol='OBD'", symbolDD)];
                //Ustawiamy numerator
				OP_KASBOLib.INumerator numerator = (OP_KASBOLib.INumerator)dokument.Numerator;

				numerator.DefinicjaDokumentu = definicjaDokumentu;
				numerator.NumerNr = numer;    
				numerator.Rejestr = "A1";//ustawiam seri� dla wybranej definicji dokument�w
				//numerator.Rejestr = 133; //je�li potrzeba ustawi� numer

                Console.WriteLine("Dzisiejsz� dat� podstawiam jako dat� utworzenia...");
                //dokument.DataDok = DateTime.Now;     //je�li data dokumentu ma by� inna ni� bie��ca
                dokument.Tytul = "Rozpocz�cie procesu akwizycji";
                dokument.Dotyczy = "Opis krok�w do wykonania. Prosz� zacz�� od dzisiaj!";

                //Dodanie podmiotu
                Console.WriteLine("Kreuj� kolekcj� kontrahent�w ...");
                CDNBase.ICollection kontrahenci = (CDNBase.ICollection)(sesja.CreateObject("CDN.Kontrahenci", null));
                Console.WriteLine("I pobieram z niej obiekt kontrahenta o kodzie 'ALOZA' ...");
                CDNHeal.IKontrahent kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];

                dokument.PodmiotTyp = 1; //TypPodmiotuKontrahent
                dokument.PodId = kontrahent.ID;
                Console.WriteLine("Zapisuj� dokument... " + dokument.Stempel.TSMod);
                sesja.Save();
                dokument = (OP_SEKLib.IDokNag)dokumenty[String.Format("DoN_DoNId={0}", dokument.ID)];
                Console.WriteLine("Dodano dokument numer: {0}", dokument.NumerPelny);


            }
            catch (COMException comError)
            {
                Console.WriteLine("###ERROR### Dodanie dokumentu OBD nie powiod�o si�!\n{0}", ErrorMessage(comError));
            }
        }
        #endregion

        [STAThread]
		static void Main(string[] args)
		{
			// tu b�dziemy sobie wywo�ywa� nasze przyk�ady.
			try
			{
				LogowanieAutomatyczne();
			//	DodanieRejestru();
			//	DodajKontrahenta();

				DodanieTowaru();
				DodanieFaktury();
		                DodanieDokumentuOBD();
		                DodanieZleceniaSerwisowego();            

			//	DogenerujKorekteFA();
			//	Dogenerowanie_WZ_Do_FA();
			//	Wydruk();
			}
			catch( COMException e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "B��d: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "B��d: " + e.Message );
			}
			catch( Exception e )
			{
				if( e.InnerException != null )
					System.Console.WriteLine( "B��d: " + e.InnerException.Message );
				else
					System.Console.WriteLine( "B��d: " + e.Message );
			}
			finally
			{
				Wylogowanie();
			}
			Console.ReadLine();
		}

	}
}

