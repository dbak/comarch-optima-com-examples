<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class Form23
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _Text2_2 As System.Windows.Forms.TextBox
	Public WithEvents _Text2_1 As System.Windows.Forms.TextBox
	Public WithEvents _Text2_0 As System.Windows.Forms.TextBox
	Public WithEvents Command1 As System.Windows.Forms.Button
	Public WithEvents _Label1_2 As System.Windows.Forms.Label
	Public WithEvents _Label1_1 As System.Windows.Forms.Label
	Public WithEvents _Label1_0 As System.Windows.Forms.Label
	Public WithEvents Label1 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents Text2 As Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form23))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me._Text2_2 = New System.Windows.Forms.TextBox
		Me._Text2_1 = New System.Windows.Forms.TextBox
		Me._Text2_0 = New System.Windows.Forms.TextBox
		Me.Command1 = New System.Windows.Forms.Button
		Me._Label1_2 = New System.Windows.Forms.Label
		Me._Label1_1 = New System.Windows.Forms.Label
		Me._Label1_0 = New System.Windows.Forms.Label
		Me.Label1 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.Text2 = New Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Text2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Dodaj Bank"
		Me.ClientSize = New System.Drawing.Size(314, 187)
		Me.Location = New System.Drawing.Point(483, 200)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "Form23"
		Me._Text2_2.AutoSize = False
		Me._Text2_2.Size = New System.Drawing.Size(129, 25)
		Me._Text2_2.Location = New System.Drawing.Point(16, 104)
		Me._Text2_2.TabIndex = 5
		Me._Text2_2.Text = "1313"
		Me._Text2_2.AcceptsReturn = True
		Me._Text2_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._Text2_2.BackColor = System.Drawing.SystemColors.Window
		Me._Text2_2.CausesValidation = True
		Me._Text2_2.Enabled = True
		Me._Text2_2.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Text2_2.HideSelection = True
		Me._Text2_2.ReadOnly = False
		Me._Text2_2.Maxlength = 0
		Me._Text2_2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._Text2_2.MultiLine = False
		Me._Text2_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Text2_2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._Text2_2.TabStop = True
		Me._Text2_2.Visible = True
		Me._Text2_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me._Text2_2.Name = "_Text2_2"
		Me._Text2_1.AutoSize = False
		Me._Text2_1.Size = New System.Drawing.Size(129, 25)
		Me._Text2_1.Location = New System.Drawing.Point(16, 64)
		Me._Text2_1.TabIndex = 3
		Me._Text2_1.Text = "Nazwa Banku"
		Me._Text2_1.AcceptsReturn = True
		Me._Text2_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._Text2_1.BackColor = System.Drawing.SystemColors.Window
		Me._Text2_1.CausesValidation = True
		Me._Text2_1.Enabled = True
		Me._Text2_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Text2_1.HideSelection = True
		Me._Text2_1.ReadOnly = False
		Me._Text2_1.Maxlength = 0
		Me._Text2_1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._Text2_1.MultiLine = False
		Me._Text2_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Text2_1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._Text2_1.TabStop = True
		Me._Text2_1.Visible = True
		Me._Text2_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me._Text2_1.Name = "_Text2_1"
		Me._Text2_0.AutoSize = False
		Me._Text2_0.Size = New System.Drawing.Size(129, 25)
		Me._Text2_0.Location = New System.Drawing.Point(16, 24)
		Me._Text2_0.TabIndex = 1
		Me._Text2_0.Text = "AKRONIM"
		Me._Text2_0.AcceptsReturn = True
		Me._Text2_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me._Text2_0.BackColor = System.Drawing.SystemColors.Window
		Me._Text2_0.CausesValidation = True
		Me._Text2_0.Enabled = True
		Me._Text2_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Text2_0.HideSelection = True
		Me._Text2_0.ReadOnly = False
		Me._Text2_0.Maxlength = 0
		Me._Text2_0.Cursor = System.Windows.Forms.Cursors.IBeam
		Me._Text2_0.MultiLine = False
		Me._Text2_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Text2_0.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me._Text2_0.TabStop = True
		Me._Text2_0.Visible = True
		Me._Text2_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me._Text2_0.Name = "_Text2_0"
		Me.Command1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command1.Text = "Dodaj bank"
		Me.Command1.Size = New System.Drawing.Size(145, 25)
		Me.Command1.Location = New System.Drawing.Point(72, 144)
		Me.Command1.TabIndex = 0
		Me.Command1.BackColor = System.Drawing.SystemColors.Control
		Me.Command1.CausesValidation = True
		Me.Command1.Enabled = True
		Me.Command1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command1.TabStop = True
		Me.Command1.Name = "Command1"
		Me._Label1_2.Text = "Nr. kierunkowy"
		Me._Label1_2.Size = New System.Drawing.Size(121, 17)
		Me._Label1_2.Location = New System.Drawing.Point(160, 112)
		Me._Label1_2.TabIndex = 6
		Me._Label1_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label1_2.BackColor = System.Drawing.SystemColors.Control
		Me._Label1_2.Enabled = True
		Me._Label1_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label1_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_2.UseMnemonic = True
		Me._Label1_2.Visible = True
		Me._Label1_2.AutoSize = False
		Me._Label1_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_2.Name = "_Label1_2"
		Me._Label1_1.Text = "Nazwa"
		Me._Label1_1.Size = New System.Drawing.Size(121, 17)
		Me._Label1_1.Location = New System.Drawing.Point(160, 72)
		Me._Label1_1.TabIndex = 4
		Me._Label1_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label1_1.BackColor = System.Drawing.SystemColors.Control
		Me._Label1_1.Enabled = True
		Me._Label1_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label1_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_1.UseMnemonic = True
		Me._Label1_1.Visible = True
		Me._Label1_1.AutoSize = False
		Me._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_1.Name = "_Label1_1"
		Me._Label1_0.Text = "Akronim"
		Me._Label1_0.Size = New System.Drawing.Size(121, 17)
		Me._Label1_0.Location = New System.Drawing.Point(160, 32)
		Me._Label1_0.TabIndex = 2
		Me._Label1_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label1_0.BackColor = System.Drawing.SystemColors.Control
		Me._Label1_0.Enabled = True
		Me._Label1_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label1_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_0.UseMnemonic = True
		Me._Label1_0.Visible = True
		Me._Label1_0.AutoSize = False
		Me._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_0.Name = "_Label1_0"
		Me.Controls.Add(_Text2_2)
		Me.Controls.Add(_Text2_1)
		Me.Controls.Add(_Text2_0)
		Me.Controls.Add(Command1)
		Me.Controls.Add(_Label1_2)
		Me.Controls.Add(_Label1_1)
		Me.Controls.Add(_Label1_0)
		Me.Label1.SetIndex(_Label1_2, CType(2, Short))
		Me.Label1.SetIndex(_Label1_1, CType(1, Short))
		Me.Label1.SetIndex(_Label1_0, CType(0, Short))
		Me.Text2.SetIndex(_Text2_2, CType(2, Short))
		Me.Text2.SetIndex(_Text2_1, CType(1, Short))
		Me.Text2.SetIndex(_Text2_0, CType(0, Short))
		CType(Me.Text2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class