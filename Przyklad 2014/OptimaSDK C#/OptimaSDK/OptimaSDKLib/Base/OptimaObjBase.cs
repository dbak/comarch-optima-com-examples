﻿using System;
using CDNBase;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Base
{
	public class OptimaObjBase
	{
		protected ILogin Login { get; private set; }
		protected IAdoSession Sesja { get; private set; }

		public OptimaObjBase(ILogin login)
		{
			Login = login;
			Sesja = Login.CreateSession();
		}
		public void ShowInfo(string text)
		{
			System.Console.WriteLine(text);
		}
		public void ShowError(Exception ex)
		{
			ShowError("", ex);
		}
		public void ShowError(string title, Exception ex)
		{
			System.Console.WriteLine("###ERROR### {0}\n{1}", title, ErrorInfo.Message(ex));
		}
	}
}