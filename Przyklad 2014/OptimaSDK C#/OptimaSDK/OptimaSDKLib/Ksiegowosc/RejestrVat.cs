﻿using System;
using CDNBase;
using OptimaSDKLib.Base;

namespace OptimaSDKLib.Ksiegowosc
{
	public class RejestrVat : OptimaObjBase
	{
		public RejestrVat(ILogin login)
			: base(login)
		{
		}
		// Dodanie dokumentu rejestru VAT
		public void DodanieRejestru()
		{
			// Tworzymy obiekt sesji

			// tworzenie potrzebnych kolekcji
			var formyPlatnosci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.FormyPlatnosci", null));
			var waluty = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Waluty", null));
			var rejestryVAT = (CDNBase.ICollection)(Sesja.CreateObject("CDN.RejestryVAT", null));
			var kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
			// pobieranie kontrahenta, formy platnosci  i waluty
			var kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];
			// w konfiguracji jest tylko jedna waluta (PLN)
			//			CDNHeal.Waluta Waluta			= (CDNHeal.Waluta)Waluty[ "WNa_Symbol='EUR'" ];
			var fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci[1];
			// utworzenie nowego obiektu rejestru VAT
			var rejestrVAT = (CDNRVAT.VAT)rejestryVAT.AddNew(null);
			//ustawianie parametrów rejestru
			rejestrVAT.Typ = 2;//	1 - zakupu; 2 - sprzedaży
			Console.WriteLine("Typ ustawiony");

			rejestrVAT.Rejestr = "SPRZEDAŻ";	// nazwa rejestru
			Console.WriteLine("Rejestr ustawiony");

			rejestrVAT.Dokument = "DET01/05/2007";
			Console.WriteLine("Dokument ustawiony");

			rejestrVAT.IdentKsieg = "2007/05/28-oop";
			Console.WriteLine("IdentKsieg ustawiony");

			rejestrVAT.DataZap = new DateTime(2007, 05, 28);
			Console.WriteLine("DataZap ustawiona");

			rejestrVAT.FormaPlatnosci = fPl;
			Console.WriteLine("Forma platnosci ustawiona");

			rejestrVAT.Podmiot = (CDNHeal.IPodmiot)kontrahent;
			Console.WriteLine("Podmiot ustawiony");
			// kategoria ustawia się sama, gdy ustawiany jest kontrahent

			// waluty nie ustawiam, bo w konf. jest na razie tylko jedna waluta (PLN)
			//			RejestrVAT.WalutaDoVAT = Waluta;	
			//			Console.WriteLine( "Waluta ustawiona " + RejestrVAT.Waluta.Symbol );

			// dodanie elementów rejestru VAT
			DodajElementyDoRejestru(rejestrVAT);

			// zapisanie zmian
			Sesja.Save();
		}

		private void DodajElementyDoRejestru(CDNRVAT.VAT RejestrVAT)
		{
			// pobranie kolekcji elementów rejestru
			CDNBase.ICollection Elementy = RejestrVAT.Elementy;
			Console.WriteLine("Dodawanie elementow: ");
			// dodanie elementów kolejno o stawkach 0%, 3%, 5%, 7%, 22% i zwolnionej
			DodajJeden(0, (CDNRVAT.VATElement)Elementy.AddNew(null));
			DodajJeden(3, (CDNRVAT.VATElement)Elementy.AddNew(null));
			DodajJeden(5, (CDNRVAT.VATElement)Elementy.AddNew(null));
			DodajJeden(7, (CDNRVAT.VATElement)Elementy.AddNew(null));
			DodajJeden(22, (CDNRVAT.VATElement)Elementy.AddNew(null));
			DodajJeden(-1, (CDNRVAT.VATElement)Elementy.AddNew(null));
		}

		private void DodajJeden(int Stawka, CDNRVAT.VATElement Element)
		{
			Console.WriteLine("\tDodaję element:");
			if (Stawka == -1)
			{
				// dodanie elementu o stawce zwolnionej z VAT
				Element.Flaga = 1;
				Element.Stawka = 0;
			}
			else
				Element.Stawka = Stawka;

			// Element.Flaga = 4;	// Flaga=4 oznacza stwkę NP.
			Console.WriteLine("\tStawka ustawiona");

			/*
			 * Rodzaje zakupów:
			 * Towary		 = 1;
			 * Inne			 = 2;
			 * Trwale		 = 3;
			 * Uslugi		 = 4;
			 * NoweSrTran	 = 5;
			 * Nieruchomosci = 6;
			 * Paliwo		 = 7;
			 */
			Element.RodzajZakupu = 1;
			Console.WriteLine("\tRodzaj zakupu ustawiony");

			Element.Netto = 23.57m; // m na końcu oznacza typ decimal
			Console.WriteLine("\tNetto ustawione");

			/* Porperty Odliczenia:
			 *  dla rej zakupu(odliczenia):		0-nie, 1-tak, 2-warunkowo
			 *	dla rej sprzed(uwz. w prop):	0-nie uwzgledniaj, 1-Uwzgledniaj w proporcji, 2- tylko w mianowniku
			 */
			Element.Odliczenia = 0;
			Console.WriteLine("\tOdliczenia ustawione\n");
		}


	}
}
