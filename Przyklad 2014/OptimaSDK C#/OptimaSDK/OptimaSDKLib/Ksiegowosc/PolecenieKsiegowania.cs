﻿using System;
using System.Collections.Generic;
using System.Text;
using CDNBase;
using OptimaSDKLib.Base;

namespace OptimaSDKLib.Ksiegowosc
{
	public class PolecenieKsiegowania: OptimaObjBase
	{
		public PolecenieKsiegowania(ILogin login)
			: base(login)
		{
		}
	
		//Przykład 10. - Utworzenie nowego polecenia księgowania
		public  void UtworzeniePK()
		{
			/* 
			 * Pobranie ID bieżącego okresu obrachunkowego z konfigracji Opt!my
			 */
			var knf = (CDNKONFIGLib.IKonfiguracja)Sesja.Login.CreateObject("CDNKonfig.Konfiguracja");
			string val = knf.get_Value("BiezOkresObr");
			int OObID = (val.Length == 0) ? 0 : Convert.ToInt32(val);

			/* stworzenie nowego PK */
			var dekret = (CDNKH.Dekrety)Sesja.CreateObject("CDN.Dekrety", null);
			var idekret = (CDNKH.IDekret)dekret.AddNew(null);

			/* pobranie obiektu okresu obrachunkowego (jeżeli konieczne) – tutaj już mamy okres do jakiego dodajemy PK
			 * ICollection okresy = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Okresy", null));
			 * CDNDave.IOkres iokres = (CDNDave.IOkres)okresy["OOb_OObID=" + OObID];
			 */

			/* 
			 * pobranie dziennika
			 * pobieranie dzinnika po samym ID nie jest dobre: 
			 *      dziennik jest przypisany do okresu obrachunkowego!
			 *      lepiej po symbolu oraz właśnie ID okresu do jakiego będzie dodane PK
			 */
			var dziennik = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Dzienniki", null));
			//CDNDave.IDziennik idzienniki = (CDNDave.IDziennik)dziennik["Dzi_DziID = 26"];
			var idzienniki = (CDNDave.IDziennik)dziennik["Dzi_Symbol = 'BANK' AND Dzi_OObID=" + OObID];

			/* stworzenie kontrahenta */
			var Kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
			var kontrahent = (CDNHeal.IKontrahent)Kontrahenci["Knt_Kod='ADM'"];

			/*
			 * podstawienie OObID nie jest konieczne - dziennik wie, do jakiego okresu należy
			 */
			//idekret.OObId = iokres.ID;
			idekret.DziID = idzienniki.ID;
			idekret.DataDok = new DateTime(2010, 06, 9);

			/*
			 * podstawienie dataOpe i DataWys nie jest konieczne, jeżeli mają być takie same jak DataDok 
			 */
			//idekret.DataOpe = new DateTime(2010, 06, 9);
			//idekret.DataWys = new DateTime(2010, 06, 9);

			idekret.Bufor = 1;
			idekret.Podmiot = (CDNHeal.IPodmiot)kontrahent;
			idekret.Dokument = "A1234";

			// Dodawania pozycji do dokumentu
			ICollection elementy = idekret.Elementy;
			var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

			var kategorie = (ICollection)Sesja.CreateObject("CDN.Kategorie", null);
			var ikategoria = (CDNHeal.IKategoria)kategorie["Kat_KodSzczegol = 'INNE'"];
			ielement.Kategoria = (CDNHeal.Kategoria)ikategoria;

			/*
			 * Te daty przejmowane są z PK
			 */
			// ielement.DataOpe = new DateTime(2010, 06, 9);
			// ielement.DataWys = new DateTime(2010, 06, 9);

			var konta = (ICollection)Sesja.CreateObject("CDN.Konta", null);

			/*
			 * Konta ksiegowe też są przypisane do okresu obrachunkowego!
			 * Wybieranie konta po samym numerze to za mało: trzeba jeszcze wybrać z jakiego okresu ma być konto!
			 */
			var kontoWn = (CDNKH.IKonto)konta["Acc_Numer = '100' AND Acc_OObID=" + OObID];
			var kontoMa = (CDNKH.IKonto)konta["Acc_Numer = '131' AND Acc_OObID=" + OObID];

			ielement.KontoWn = (CDNKH.Konto)kontoWn;
			ielement.KontoMa = (CDNKH.Konto)kontoMa;

			ielement.Kwota = 100;

			// Koniec dodawania pozycji do dokumentu
			Sesja.Save();
		}


	}
}
