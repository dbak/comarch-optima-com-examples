﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using CDNBase;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Logistyka
{
	public class Crm : OptimaObjBase
	{
		public Crm(ILogin login)
			: base(login)
		{
		}
		public void DodanieKontrahentowDoFaCyklicznej()
		{
			try
			{
				Process proc = Process.GetCurrentProcess();
				Console.WriteLine("##### dodawanie kontrahentów do faktury cyklicznej#####");
				Console.WriteLine("Kreuję kolekcję faktur ...");
				var fakturyCykl = (OP_CCRMLib.FaCykliczne)Sesja.CreateObject("CDN.FaCykliczne", null);
				Console.WriteLine("I pobieram z niej obiekt fa o ID = 2");
				var faCykliczna = (OP_CCRMLib.IFaCykliczna)fakturyCykl["FcN_FcNId=2"];

				Console.WriteLine("Pobieram kolekcję kontrahentow...");
				CDNBase.ICollection kontrahenci = faCykliczna.Kontrahenci;
				Console.WriteLine("I dodaję do niej nowy obiekt...");
				const int maxKnt = 1000;
				for (int i = 0; i < maxKnt; i++)
				{
					var kontrahent = (OP_CCRMLib.IFaCyklicznaKnt)kontrahenci.AddNew(null);
					kontrahent.PodmiotTyp = 1;
					kontrahent.PodmiotId = i;
					kontrahent.TypKnt = 2;
				}
				Console.WriteLine("Zapisuję sesję...");
				Sesja.Save();
				Console.WriteLine("Dodano " + maxKnt + " kontrahentów...");
			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodawanie nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}
		}

	}
}
