﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using CDNBase;
using CDNTwrb1;
using OP_CSRSLib;
using OP_KASBOLib;
using OP_SEKLib;
using OptimaSDKLib.Base;
using OptimaSDKLib.Slowniki;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Logistyka
{
	public class ObiegDokumentow: OptimaObjBase
	{
		public ObiegDokumentow(ILogin login)
			: base(login)
		{
		}
		public void DodanieDokumentuOBD()
		{
			try
			{
				Console.WriteLine("##### dodawanie dokumentu OBD #####");
				Console.WriteLine("Kreuję kolekcję dokumentów ...");
				var dokumenty = (OP_SEKLib.DokNagColl)Sesja.CreateObject("CDN.DokNagColl", null);
				Console.WriteLine("Dodaję nowy dokument do kolekcji ...");
				var dokument = (OP_SEKLib.IDokNag)dokumenty.AddNew(null);
				dokument.Typ = 1;  //firmowy 

				//jeśli seria dokumentu ma być inna niż domyślna
				const string symbolDD = "AUTO";
				var definicjeDokumentow = (CDNHeal.DefinicjeDokumentow)Sesja.CreateObject("CDN.DefinicjeDokumentow", null);
				var definicjaDokumentu = (CDNHeal.DefinicjaDokumentu)definicjeDokumentow[string.Format("Ddf_Symbol='OBD'", symbolDD)];
				//Ustawiamy numerator
				var numerator = (INumerator)dokument.Numerator;
				numerator.DefinicjaDokumentu = definicjaDokumentu;
				numerator.Rejestr = "A1";   //ustawiam serię dla wybranej definicji dokumentów
				numerator.NumerNr = 133;   //jeśli potrzeba ustawić numer

				Console.WriteLine("Dzisiejszą datę podstawiam jako datę utworzenia...");
				//dokument.DataDok = DateTime.Now;     //jeśli data dokumentu ma być inna niż bieżąca
				dokument.Tytul = "Rozpoczęcie procesu akwizycji";
				dokument.Dotyczy = "Opis kroków do wykonania. Proszę zacząć od dzisiaj!";

				//Dodanie podmiotu
				Console.WriteLine("Kreuję kolekcję kontrahentów ...");
				var kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
				Console.WriteLine("I pobieram z niej obiekt kontrahenta o kodzie 'ADM' ...");
				var kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ADM'"];

				dokument.PodmiotTyp = 1; //TypPodmiotuKontrahent
				dokument.PodId = kontrahent.ID;

				//dodanie dokumentu skojarzonego: faktura sprzedaży o ID=92
				DodajDokumentDoOBD(dokument, OptimaTypDokumentu.FakturaSprzedazy, 92);
				//dodanie dokumentu skojarzonego: rejestr VAT o ID = 92
				DodajDokumentDoOBD(dokument, OptimaTypDokumentu.RejestrVat, 92);

				//dodanie pliku do OBD
				DodajPlikDoOBD(dokument, "c:\\dane\\lista.pdf", 1, 1);

				Console.WriteLine("Zapisuję dokument... " + dokument.Stempel.TSMod);

				Sesja.Save();
				dokument = (OP_SEKLib.IDokNag)dokumenty[String.Format("DoN_DoNId={0}", dokument.ID)];
				Console.WriteLine("Dodano dokument numer: {0}", dokument.NumerPelny);


			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodanie dokumentu OBD nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}
		}

		private void DodajTylkoNaglowekDokumentu(string tytul)
		{
			try
			{
				CDNBase.AdoSession sesja = Login.CreateSession();
				var dokumenty = (OP_SEKLib.DokNagColl)sesja.CreateObject("CDN.DokNagColl", null);
				IDokNag dokument = (OP_SEKLib.IDokNag)dokumenty.AddNew(null);

				dokument.Typ = 1;
				dokument.Tytul = tytul;
				//var stempel = dokument.Stempel;
				sesja.Save();
				dokument = (OP_SEKLib.IDokNag)dokumenty[String.Format("DoN_DoNId={0}", dokument.ID)];
				Console.WriteLine("Dodano dokument numer: {0}", dokument.NumerPelny);

			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodanie dokumentu OBD nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}

		}


		public  void UstalKatalogDlaDokumentuOBD()
		{
			try
			{
				Console.WriteLine("##### dodawanie dokumentu OBD #####");
				Console.WriteLine("Kreuję kolekcję dokumentów ...");
				var dokumenty = (OP_SEKLib.DokNagColl)Sesja.CreateObject("CDN.DokNagColl", null);
				Console.WriteLine("Dodaję nowy dokument do kolekcji ...");
				const int dokumentID = 15;
				var dokument = (OP_SEKLib.IDokNag)dokumenty[String.Format("DoN_DoNId={0}", dokumentID)];

				const int katalogFirmowyId = 1;
				dokument.Katalog = katalogFirmowyId;

				Console.WriteLine("Zapisuję dokument... " + dokument.Stempel.TSMod);
				Sesja.Save();
				dokument = (OP_SEKLib.IDokNag)dokumenty[String.Format("DoN_DoNId={0}", dokument.ID)];
				Console.WriteLine("Zmodyfikowano dokument numer: {0}", dokument.NumerPelny);
			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodanie dokumentu OBD nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}
		}

		private  void DodajDokumentDoOBD(IDokNag dokument, OptimaTypDokumentu typDokumentu, int idDokumentu)
		{
			var dokRelacja = (IDokRelacja)dokument.Dokumenty.AddNew(null);
			dokRelacja.ParentTyp = 750;
			dokRelacja.ParentId = dokument.ID;
			dokRelacja.DokumentTyp = (int) typDokumentu;
			dokRelacja.DokumentId = idDokumentu;
			dokRelacja.Flaga = 0;

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="dokument"></param>
		/// <param name="sciezkaDoPliku">pełna ścieżka do pliku np. "c:\\dane\\lista.pdf"</param>
		/// <param name="numerWersjiPliku">numer wersji pliku np. 1, 2 lub 3</param>
		/// <param name="mode">Link = 1,Plik = 2,Scan = 3</param>
		private void DodajPlikDoOBD(IDokNag dokument, string sciezkaDoPliku, int numerWersjiPliku, int mode)
		{
			if (!File.Exists(sciezkaDoPliku)) return;
			var sciezka = sciezkaDoPliku;
			var nazwaPliku = Path.GetFileName(sciezkaDoPliku);

			var rDokNagPlik = (IDokNagPlik)dokument.PlikiArchiwalne.AddNew(null);

			rDokNagPlik.UstawStempel();
			rDokNagPlik.FileMode = mode;
			rDokNagPlik.DoNID = dokument.ID;
			if (mode == 1) rDokNagPlik.Sciezka = sciezka;
			rDokNagPlik.NazwaPliku = nazwaPliku;
			rDokNagPlik.WBazie = mode == 1 ? 0 : 1;

			rDokNagPlik.Wersja = numerWersjiPliku;  //musi być po ustawieniu ścieżki, by poprawnie ustalić wersję
			AddToPlikiBinarneCollection(rDokNagPlik, mode, sciezka, nazwaPliku);
		}

		private  void AddToPlikiBinarneCollection(IDokNagPlik rDokNagPlik, int mode, string sciezka, string nazwaPliku)
		{
			rDokNagPlik.UstawStempel();
			var rDaneBinarne = rDokNagPlik.PlikiBinarne;
			for (int i = 0; i < rDaneBinarne.Count; i++)
			{
				rDaneBinarne.Delete(rDaneBinarne[i]);
			}
			if (mode == 2 || mode == 3)    //plik lub scan
			{
				var rDanaBinarna = (IDanaBinarna)rDaneBinarne.AddNew(null);
				rDanaBinarna.TWAID = rDokNagPlik.ID;
				rDanaBinarna.DodajPlik(sciezka);
				rDanaBinarna.Typ = 2;
				rDanaBinarna.NazwaPliku = nazwaPliku;
			}
		}


	}
}
