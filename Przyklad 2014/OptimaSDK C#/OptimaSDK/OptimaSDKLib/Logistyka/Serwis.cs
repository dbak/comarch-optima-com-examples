﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using CDNBase;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Logistyka
{
	public class Serwis : OptimaObjBase
	{
		public Serwis(ILogin login)
			: base(login)
		{
		}
		public void DodanieZleceniaSerwisowego()
		{
			try
			{
				ShowInfo("##### dodawanie zlecenia serwisowego #####");
				ShowInfo("Kreuję kolekcję zleceń serwisowych ...");
				var zlecenia = (OP_CSRSLib.SrsZlecenia)Sesja.CreateObject("CDN.SrsZlecenia", null);
				ShowInfo("Dodaję nowe zlecenie do kolekcji zleceń serwisowych ...");
				var zlecenie = (OP_CSRSLib.ISrsZlecenie)zlecenia.AddNew(null);

				ShowInfo("Kreuję kolekcje kontrahentów ...");
				var kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
				ShowInfo("I pobieram z niej obiekt kontrahenta o kodzie 'ALOZA' ...");
				var kontrahent = (CDNHeal.IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];

				ShowInfo("Teraz obiekt kontrahenta podstawiam do property Podmiot dla zlecenia ...");
				zlecenie.Podmiot = (CDNHeal.IPodmiot)kontrahent;

				ShowInfo("Dzisiejszą datę podstawiam jako datę utworzenia zlecenia...");
				zlecenie.DataDok = DateTime.Now;

				ShowInfo("Pobieram kolekcję czynności przypisanych do zlecenia...");
				CDNBase.ICollection czynnosci = zlecenie.Czynnosci;
				ShowInfo("I dodaję do niej nowy obiekt...");
				var czynnosc = (OP_CSRSLib.ISrsCzynnosc)czynnosci.AddNew(null);

				ShowInfo("Przypisuję usługę o kodzie PROJ_ZIELENI do tej czynności...");
				czynnosc.TwrId = GetValue.GetTowarID(Sesja, "PROJ_ZIELENI");
				ShowInfo("Ilość jednostek ustalam na dwie...");
				czynnosc.Ilosc = 2;
				czynnosc.CenaNetto = 100;
				czynnosc.CzasTrwania = new DateTime(1899, 12, 30, 12, 0, 0);   //12 godzin
				czynnosc.KosztUslugi = 48;

				ShowInfo("Teraz dodaję części ...");
				CDNBase.ICollection czesci = zlecenie.Czesci;
				ShowInfo("I dodaję do niej nowy obiekt...");
				OP_CSRSLib.ISrsCzesc czesc = (OP_CSRSLib.ISrsCzesc)czesci.AddNew(null);

				ShowInfo("Przypisuję towar o kodzie IGLAKI_CYPRYS ...");
				czesc.TwrId = GetValue.GetTowarID(Sesja, "IGLAKI_CYPRYS");
				ShowInfo("Ilość jednostek ustalam na trzy...");
				czesc.Ilosc = 3;
				czesc.CenaNetto = 99.80m;
				czesc.Fakturowac = 1; //do fakturowania

				ShowInfo("Przypisuję towar o kodzie ZIEMIA_5 ...");
				czesc.TwrId = GetValue.GetTowarID(Sesja, "ZIEMIA_5");
				ShowInfo("Ilość jednostek ustalam na pięć...");
				czesc.Ilosc = 5;
				czesc.CenaNetto = 4.90m;
				czesc.Fakturowac = 1; //do fakturowania


				ShowInfo("Zapisuję sesję...");
				Sesja.Save();
				zlecenie = (OP_CSRSLib.ISrsZlecenie)zlecenia[String.Format("SrZ_SrZId={0}", zlecenie.ID)];
				ShowInfo(
					string.Format(
					"Dodano zlecenie: {0}\nCzas trwania czynności: {1}:{2}\nKoszt: {3}\nWartość netto w cenach sprzedaży: {4}\nWartość netto do zafakturowania : {5}",
					zlecenie.NumerPelny,
					zlecenie.CzynnosciCzasTrwania / 100,
					(zlecenie.CzynnosciCzasTrwania % 100).ToString("00"),
					zlecenie.Koszt.ToString("#0.00"),
					zlecenie.WartoscNetto.ToString("#0.00"),
					zlecenie.WartoscNettoDoFA.ToString("#0.00")));
			}
			catch (COMException comError)
			{
				ShowError("Dodanie zlecenia nie powiodło się!", comError);
			}
		}


		public void DodanieUrzadzeniaSerwisowego(string kodUrzadzenia, string nazwaUrzadzenia, string akronimKontrahenta, string akronimOdbiorcy)
		{
			try
			{
				ShowInfo("##### dodawanie urządzenia serwisowego #####");
				ShowInfo("Kreuję kolekcję urządzeń serwisowych ...");
				var urzadzenia = (OP_CSRSLib.SrsUrzadzenia)Sesja.CreateObject("CDN.SrsUrzadzenia", null);
				ShowInfo("Dodaję nowe urządzenie do kolekcji...");
				var urzadzenie = (OP_CSRSLib.ISrsUrzadzenie)urzadzenia.AddNew(null);

				urzadzenie.Kod = kodUrzadzenia;
				urzadzenie.Nazwa = nazwaUrzadzenia;
				//urzadzenie.Opis = "opcjonalny opis urządzenia";


				//ustalanie powiązanego kontrahenta na podstawie akronimu
				ShowInfo("Kreuję kolekcje kontrahentów ...");
				var kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
				ShowInfo("I pobieram z niej obiekt kontrahenta o kodzie 'ALOZA' ...");
				var kontrahent = (CDNHeal.IKontrahent)kontrahenci[string.Format("Knt_Kod='{0}'", akronimKontrahenta)];

				ShowInfo("Teraz obiekt kontrahenta podstawiam do property Podmiot dla zlecenia ...");
				urzadzenie.Podmiot = (CDNHeal.IPodmiot)kontrahent;
				
				//Jeżeli odbiorca jest inny niż kontrahent to ustawiamy też odbiorcę
				if (!akronimKontrahenta.Equals(akronimOdbiorcy) && !string.IsNullOrEmpty(akronimOdbiorcy) )
				{
					var odbiorca = (CDNHeal.IKontrahent) kontrahenci[string.Format("Knt_Kod='{0}'", akronimOdbiorcy)];
					urzadzenie.Odbiorca = odbiorca;
				}
				// Jeżeli znamy Typ i Id powiązanego podmiotu, to powyższy kod mozna uprościć:
				//urzadzenie.PodmiotTyp = 1;  //kontrahent
				//urzadzenie.PodmiotId = 7; 


				//powiązanie urządzenia z kartą cennikową
				//const string akronimKartyCennikowej = "BL_RYS";
				//urzadzenie.TwrId = GetValue.GetTowarID(Sesja, akronimKartyCennikowej);

				ShowInfo("Zapisuję sesję...");
				Sesja.Save();
				urzadzenie = (OP_CSRSLib.ISrsUrzadzenie)urzadzenia[String.Format("SrU_SrUId={0}", urzadzenie.ID)];
				ShowInfo(string.Format("Dodano urządzenie: {0} {1}",urzadzenie.Kod, urzadzenie.Nazwa));
			}
			catch (COMException comError)
			{
				ShowError("Dodanie urządzenia nie powiodło się!",comError);
			}
		}

	}
}
