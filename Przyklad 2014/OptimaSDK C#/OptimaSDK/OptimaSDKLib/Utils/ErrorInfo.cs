﻿using System.Text;

namespace OptimaSDKLib.Utils
{
	public class ErrorInfo
	{
		public static string Message(System.Exception e)
		{
			var mess = new StringBuilder();
			if (e != null)
			{
				mess.Append(e.Message);
				while (e.InnerException != null)
				{
					mess.Append(e.InnerException.Message);
					e = e.InnerException;
				}
			}
			return mess.ToString();
		}

	}
}
