﻿using System;
using System.Collections.Generic;
using System.Text;
using ADODB;
using CDNBase;
using CDNTwrb1;
using OptimaSDKLib.Base;

namespace OptimaSDKLib.Slowniki
{
	public class Towary : OptimaObjBase
	{
		public Towary(ILogin login)
			: base(login)
		{
		}

		public void Dodaj()
		{
			var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);
			var towar = (CDNTwrb1.ITowar)towary.AddNew(null);

			towar.Nazwa = "Nowy towar dodany z C#";
			towar.Kod = "NOWY_C#";
			towar.Stawka = 22.00m;
			towar.Flaga = 2;		// 2- oznacza stawkę opodatkowaną, pozostałe wartości 
									// pola są podane w strukturze bazy tabela: [CDN.Towary].[Twr_Flaga]

			towar.JM = "SZT";
			Sesja.Save();
			Console.WriteLine("Dodano towar: " + towar.Nazwa1);
		}

		public void DodajTowaUstawCeneZakupu()
		{

			var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);
			var towar = (CDNTwrb1.ITowar)towary.AddNew(null);

			towar.Nazwa = "Nowy towar dodany z C#";
			string nazwa = towar.Nazwa1;
			towar.Kod = "NOWY_C#";

			towar.Stawka = 22.00m;
			towar.Flaga = 2; // 2- oznacza stawkę opodatkowaną, pozostałe wartości 
			// pola są podane w strukturze bazy tabela: [CDN.Towary].[Twr_Flaga]

			towar.JM = "SZT";

			foreach (ICena cena in towar.Ceny)
			{
				if (cena.Numer == 1) // cena zakupu ma zawsze nr 1
				{
					if (cena.Typ == CDNHlmn.Const.e_op_nCenaNetto)
					{
						cena.WartoscNetto = 5.00m;
					}
					else if (cena.Typ == CDNHlmn.Const.e_op_nCenaBrutto)
					{
						cena.WartoscBrutto = 5.00m;
					}
				}
			}
			Sesja.Save();
		}
		public string  ImportCennika()
		{
			const string plik = @"D:\optima\import\cennik.xml";
			const string aktualizacja = "2111111000011100000000001";
			return ImportCennika(plik, aktualizacja);
		}

		public string ImportCennika(string plik, string aktualizacja)
		{
			string importDesc;
			try
			{
				Console.WriteLine("Import cennika z pliku: " + plik);

				const string cenyDoAktualizacji = "select DfC_Lp, DfC_Nazwa from cdn.DefCeny where DfC_Lp > 1";
				var cn = Sesja.Connection;
				var listaCenDoAktualizacjiRs = new ADODB.RecordsetClass();
				listaCenDoAktualizacjiRs.Open(cenyDoAktualizacji, cn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);


				var rCennik = new OP_DBIMPLib.CennikImport {Login = Login, PlikXML = plik, OpcjeAktualizacji = aktualizacja};
				rCennik.Start(listaCenDoAktualizacjiRs);
				importDesc =  "Cennik zaimportowany do Optimy pomyślnie";
			}
			catch (Exception e)
			{
				importDesc = "Nie mogłem zaimportować cennika " + e.ToString();

			}
			Console.WriteLine(importDesc);
			return importDesc;
		}
	}

}
