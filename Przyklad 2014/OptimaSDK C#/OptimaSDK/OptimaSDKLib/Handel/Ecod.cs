﻿using System;
using System.Runtime.InteropServices;
using CDNBase;
using OP_DBIMPLib;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Handel
{
	public class Ecod	 : OptimaObjBase
	{
		public Ecod(ILogin login)
			: base(login)
		{
		}
	

		public void EksportujOptimaFSJakoECODFa()
		{
			//TrN_TrNId dokumentu
			const int idFa = 2;  
			//ścieżka do katalogu gdzie ma być umieszczony plik xml
			const string ecodDir = "c:\\ecod\\";
			try
			{
				Console.WriteLine("##### eksportowanie dokumentu #####");
				var ecodService = (IServisAhold)Sesja.CreateObject("DBImp.ServisAhold", null);
				ecodService.Login = Sesja.Login;
				ecodService.MakeEcodFa(ecodDir, idFa);
				string query = "select top 1 TrN_NumerPelny from cdn.TraNag WHERE TrN_TrNId = " + idFa;
				var numerDok = GetValue.GetSingleValue(Sesja, query, false);
				Console.WriteLine("##### wyeksportowano dokument: " + numerDok + " #####");
			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Eksportowanie dokumentu nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}

		}

		public void ImportujECODZamJakoOptimaRO()
		{
			const string file = @"d:\Optima\Ecod\ro.xml";
			const string transformata = @"d:\Optima\Ecod\EcodZam2OptimaRO.xsl";
			try
			{
				Console.WriteLine("##### importowanie dokumentu #####");
				var expXml = (IDokumentHaMagXml)Sesja.CreateObject("DBImp.DokumentHaMagXml", null);
				expXml.Login = Sesja.Login;
				expXml.CzytajXml(file, transformata);
				const string query = "select top 1 TrN_NumerPelny from cdn.TraNag WHERE TrN_TrNId = (select MAX(TrN_TrNId) from cdn.TraNag where TrN_Rodzaj = 308000)";
				var numerDok = GetValue.GetSingleValue(Sesja, query, false);
				Console.WriteLine("##### dodano dokument: " + numerDok + " #####");
			}
			catch (COMException comError)
			{
				Console.WriteLine("###ERROR### Dodanie dokumentu  nie powiodło się!\n{0}", ErrorInfo.Message(comError));
			}

		}


	}
}
