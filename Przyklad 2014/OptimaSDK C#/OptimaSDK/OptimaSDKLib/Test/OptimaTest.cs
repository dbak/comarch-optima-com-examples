﻿using System;
using System.Runtime.InteropServices;
using OptimaSDKLib.Base;
using OptimaSDKLib.Handel;
using OptimaSDKLib.Utils;

namespace OptimaSDKLib.Test
{
	public class OptimaTest
	{
		public void Wykonaj()
		{
			Logowanie logowanie = null;
			try
			{
				const string user = "ADMIN";
				const string haslo = "";
				const string firma = "PREZENTACJA_KH";
				//firma = "I29";

				ShowInfo(string.Format("Logowanie: {0} {1}", firma, user));

				logowanie = new Logowanie();
				logowanie.Zaloguj(user, haslo, firma);

				//new Kontrahenci(logowanie.Login).Dodaj();

				//new Towary(logowanie.Login).Dodaj();
				//new Towary(logowanie.Login).ImportCennika();
				//new Towary(logowanie.Login).DodajTowaUstawCeneZakupu();

				//new Faktura(logowanie.Login).Dodaj();
				//new Faktura(logowanie.Login).DogenerujKorekteFA();
				//new Faktura(logowanie.Login).Dogenerowanie_WZ_Do_FA();
				//new Ecod(logowanie.Login).ImportujECODZamJakoOptimaRO();
				//new Ecod(logowanie.Login).EksportujOptimaFSJakoECODFa();

				//new ObiegDokumentow(logowanie.Login).DodanieDokumentuOBD();

				//new Serwis(logowanie.Login).DodanieZleceniaSerwisowego();
				//new Serwis(logowanie.Login).DodanieUrzadzeniaSerwisowego("Blokada rysika","Blokada rysika wąskotorwego", "AGAT","AGAT");

				//new Crm(logowanie.Login).DodanieKontrahentowDoFaCyklicznej();


				//new RejestrVat(logowanie.Login).DodanieRejestru();
				//new PolecenieKsiegowania(logowanie.Login).UtworzeniePK();

                //(new KasaBank.ZapisKB(logowanie.Login)).Dodaj();
                //(new KasaBank.ZdarzenieKB(logowanie.Login)).Dodaj();
			    new Faktura(logowanie.Login).DodajFaPotemWyslijEMail();

			}
			catch (COMException e)
			{
				ShowError(e);
			}
			catch (Exception e)
			{
				ShowError(e);
			}
			finally
			{
				if (logowanie != null) logowanie.Wyloguj();
			}
			ShowInfo("Naciśnij klawisz Enter...");
			Console.ReadLine();
		}

		void ShowInfo(string text)
		{
			System.Console.WriteLine(text);
		}
		void ShowError(Exception e)
		{
			System.Console.WriteLine("Błąd: " + ErrorInfo.Message(e));
		}

	}
}
