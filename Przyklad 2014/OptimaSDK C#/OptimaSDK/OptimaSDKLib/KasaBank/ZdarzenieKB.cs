﻿using System;
using System.Runtime.InteropServices;
using CDNBase;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;
using OP_KASBOLib;
using CDNHeal;

namespace OptimaSDKLib.KasaBank
{
	public class ZdarzenieKB: OptimaObjBase
	{
        public ZdarzenieKB(ILogin login)
			: base(login)
		{
		}

		    //Dodanie zdarzenia KB
		public void Dodaj()
		{
                //Wykreowanie kolekcji serwerowej zdarzeń KB
            ZdarzeniaKB zdarzeniaKB = (ZdarzeniaKB)(Sesja.CreateObject("CDN.ZdarzeniaKB", null));
                //Dodanie nowego zdarzenia KB do kolekcji serwerowej
            IZdarzenieKB zdarzenieKB = (IZdarzenieKB)(zdarzeniaKB.AddNew(null));

                //Ustawienie rejestru KASA
            IRachunek rejestrKB = (IRachunek)(Sesja.CreateObject("CDN.Rachunki", "BRa_BRaID = 1")); //rejestr KASA
            zdarzenieKB.Rachunek = (Rachunek)rejestrKB;

                //Ustawienie definicji dokumentu KW
            DefinicjaDokumentu defDok = (DefinicjaDokumentu)(Sesja.CreateObject("CDN.DefinicjeDokumentow", "DDf_Symbol = 'KP'"));
            zdarzenieKB.DefinicjaDokumentu = (DefinicjaDokumentu)defDok;

                //Ustawienie kontrahenta ALOZA
            Kontrahenci kontrahenci = (Kontrahenci)(Sesja.CreateObject("CDN.Kontrahenci", null));
            IKontrahent kontrahent = (IKontrahent)kontrahenci["Knt_Kod='ALOZA'"];
            zdarzenieKB.Podmiot = ((IPodmiot)kontrahent);

                //Ustawienie formy płatności gotówka
            FormyPlatnosci formyPlatnosci = (FormyPlatnosci)(Sesja.CreateObject("CDN.FormyPlatnosci", null));
            IFormaPlatnosci formaPlatnosci = (IFormaPlatnosci)formyPlatnosci["FPl_Nazwa='gotówka'"];
            zdarzenieKB.FormaPlatnosci = ((FormaPlatnosci)formaPlatnosci);

            zdarzenieKB.NumerPelny = "moje KP";

                //Ustawienie daty i kwoty zdarzenia
            zdarzenieKB.DataDok = DateTime.Today;
            zdarzenieKB.Kwota = 100;

                //Zapis sesji
			Sesja.Save();

            Console.WriteLine("Dodano płatność: {0}", zdarzenieKB.NumerPelny);
		}
	}
}
