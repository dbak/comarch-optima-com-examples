﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Windows.Forms;
using Optima.Common;
using Optima.Common.Logic.Services;
using Optima.Common.Logic.Wydruki;
using Optima.Common.Logic.DynamicParametersLanguage;
using Optima.Common.View.WinForms;
using CDNHlmn;
using CDNHeal;
using CDNBase;

namespace OptimaNETSDK.Wydruki
{

    public class PrzykladyWykonywaniaWydrukow
    {

        #region Parametry

        #region Parametry logowania

        private string logowanie_NazwaFirmy = "PIRAMIDA";
        private string logowanie_OperatorKod = "ADMIN";
        private string logowanie_OperatorHaslo = "";
        ModuleCollection logowanie_Moduly = new ModuleCollection { Module.Handel, Module.KasaBank, Module.CRM };

        #endregion


        #region Patametry testu

        private TypOkna typOkna = TypOkna.ProstaLista;
        private TypWyboruWydruku typWyboruWydruku = TypWyboruWydruku.Domyslny;
        private TypCGXWydruku typCGXWydruku = TypCGXWydruku.Crystal; //Używane tylko jeśli typWyboruWydruku = TypWyboruWydruku.Podany
        private TypUrzadzenia typUrzadzenia = TypUrzadzenia.PodgladZKopiami;

        #endregion

        #endregion



        #region Konstruktor

        public PrzykladyWykonywaniaWydrukow()
        {
        }

        #endregion



        #region Testy sparametryzowane - sterowanie

        public void WykonajTest(TypOkna _typOkna, TypWyboruWydruku _typWyboruWydruku, TypCGXWydruku _typCGXWydruku, TypUrzadzenia _typUrzadzenia)
        {
            this.typOkna = _typOkna;
            this.typWyboruWydruku = _typWyboruWydruku;
            this.typCGXWydruku = _typCGXWydruku;
            this.typUrzadzenia = _typUrzadzenia;

            switch (typOkna)
            {
                case TypOkna.ProstyFormularz:
                    {
                        if (typWyboruWydruku == TypWyboruWydruku.Podany)
                            WykonajWydrukPodanyRejestruKB(typUrzadzenia);
                        else
                            WykonajWydrukDomyslnyRejestruKB(typUrzadzenia);
                        break;
                    }
                case TypOkna.ZlozonyFormularz:
                    {
                        if (typWyboruWydruku == TypWyboruWydruku.Podany)
                            WykonajWydrukPodanyFaktury(typUrzadzenia);
                        else
                            WykonajWydrukDomyslnyFaktury(typUrzadzenia);
                        break;
                    }
                case TypOkna.ProstaLista:
                    {
                        if (typWyboruWydruku == TypWyboruWydruku.Podany)
                            WykonajWydrukPodanyListyRejestrowKB(typUrzadzenia);
                        else
                            WykonajWydrukDomyslnyListyRejestrowKB(typUrzadzenia);
                        break;
                    }
                case TypOkna.ZlozonaLista:
                    {
                        if (typWyboruWydruku == TypWyboruWydruku.Podany)
                            WykonajWydrukPodanyListyFaktur(typUrzadzenia);
                        else
                            WykonajWydrukDomyslnyListyFaktur(typUrzadzenia);
                        break;
                    }
            }
        }

        #endregion


        #region Testy sparametryzowane - właściwy kod

        private void WykonajWydrukDomyslnyRejestruKB(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.BRaEdycja;
                int kontID = 1;
                int idRejestruKB = 1;

                    //Wykreowanie obiektu wydruku domyślnego
                Wydruk w = PobierzWydrukDomyslny(procID, kontID, RodzajWydruku.RegularnyWydruk, null);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }
                    //Ustawienie filtra
                w.FiltrAplikacji = "BRa_BRaID = " + idRejestruKB;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

                //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukPodanyRejestruKB(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                int idRejestruKB = 1;

                    //Wykreowanie podanego wydruku
                bool czyWydrukStandardowy = true;
                int wydrukID = 22;
                Wydruk w = new Wydruk(czyWydrukStandardowy, wydrukID);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }
                    //Ustawienie filtra
                w.FiltrAplikacji = "BRa_BRaID = " + idRejestruKB;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

                //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukDomyslnyFaktury(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();
            
            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.TrNEdycjaFS;
                int kontID = 1;
                int idFaktury = 92;
                IAdoSession session = loginService.LoginInfo.CreateSession();
                CDNHlmn.DokumentHaMag dokumentHaMag = (CDNHlmn.DokumentHaMag)(session.CreateObject("CDN.DokumentyHaMag", "TrN_TrNID = " + idFaktury));

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("FVMarza", new DynamicVariable("FVMarza", dokumentHaMag.FVMarza, DynamicVariableValueType.IntegerNumber));
                zmienneDyn.Add("SymbolWal", new DynamicVariable("SymbolWal", dokumentHaMag.WalutaSymbol == "PLN" ? String.Empty : dokumentHaMag.WalutaSymbol, DynamicVariableValueType.Text));
                zmienneDyn.Add("TrNID", new DynamicVariable("TrNID", idFaktury, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Faktura sprzedaży", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("EMAIL_TO", new DynamicVariable("EMAIL_TO", dokumentHaMag.Podmiot.Email, DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("EMAIL_ATTACHFILEPREFIX", new DynamicVariable("EMAIL_ATTACHFILEPREFIX", dokumentHaMag.NumerPelny, DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));

                    //Wykreowanie wydruku domyślnego
                Wydruk w = PobierzWydrukDomyslny(procID, kontID, RodzajWydruku.RegularnyWydruk, zmienneDyn);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                    //Ustawienie filtra
                w.FiltrAplikacji = "TrN_TrNID = " + idFaktury;

                    //Ewentualne ustawienie zmiennych GenRapa
                if (w.Typ == TypWydruku.GenRap)
                {
                    w.TypOkna = WydrukTypOkna.Formularz;
                    w.GenRapReportType = 2; //Formularz
                    w.GenRapNewReportInitObjects = "FakturaSpr";
                    w.GenRapContextAttribute = procID.ToString();
                        //Dodajemy najważniejszą zmienną dynamiczną do słownika: IDENTYFIKATOR.
                        //Uwaga: kucz zmiennej IDENTYFIKATOR musi być dodany z wielkimi literami, 
                        //bo inaczej nie będzie zmaczowany z parametrem!
                    zmienneDyn.Add("IDENTYFIKATOR", new DynamicVariable("Identyfikator", idFaktury, DynamicVariableValueType.IntegerNumber));
                }

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn);
            }
            catch(Exception)
            {
                Wyloguj(loginService);
                throw;
            }

                //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukPodanyFaktury(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();
            
            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.TrNEdycjaFS;
                int idFaktury = 92;
                IAdoSession session = loginService.LoginInfo.CreateSession();
                CDNHlmn.DokumentHaMag dokumentHaMag = (CDNHlmn.DokumentHaMag)(session.CreateObject("CDN.DokumentyHaMag", "TrN_TrNID = " + idFaktury));

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("FVMarza", new DynamicVariable("FVMarza", dokumentHaMag.FVMarza, DynamicVariableValueType.IntegerNumber));
                zmienneDyn.Add("SymbolWal", new DynamicVariable("SymbolWal", dokumentHaMag.WalutaSymbol == "PLN" ? String.Empty : dokumentHaMag.WalutaSymbol, DynamicVariableValueType.Text));
                zmienneDyn.Add("TrNID", new DynamicVariable("TrNID", idFaktury, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Faktura sprzedaży", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("EMAIL_TO", new DynamicVariable("EMAIL_TO", dokumentHaMag.Podmiot.Email, DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("EMAIL_ATTACHFILEPREFIX", new DynamicVariable("EMAIL_ATTACHFILEPREFIX", dokumentHaMag.NumerPelny, DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));

                    //Wykreowanie podanego wydruku
                bool czyWydrukStandardowy = true;
                int wydrukID = 0;
                if (typCGXWydruku == TypCGXWydruku.Crystal)
                    wydrukID = 1256; //Faktura VAT / Wzór standard
                else if (typCGXWydruku == TypCGXWydruku.GenRap)
                    wydrukID = 372; //Faktura VAT (GenRap) / Wzór standard
                else if (typCGXWydruku == TypCGXWydruku.XML)
                    wydrukID = 325; //Wydruki Seryjne / FA && WZ && KP
                Wydruk w = new Wydruk(czyWydrukStandardowy, wydrukID);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                //Ewentualne ustawienie zmiennych GenRapa
                if (w.Typ == TypWydruku.GenRap)
                {
                    w.TypOkna = WydrukTypOkna.Formularz;
                    w.GenRapReportType = 2; //Formularz
                    w.GenRapNewReportInitObjects = "FakturaSpr";
                    w.GenRapContextAttribute = procID.ToString();
                        //Dodajemy najważniejszą zmienną dynamiczną do słownika: IDENTYFIKATOR.
                        //Uwaga: kucz zmiennej IDENTYFIKATOR musi być dodany z wielkimi literami, 
                        //bo inaczej nie będzie zmaczowany z parametrem!
                    zmienneDyn.Add("IDENTYFIKATOR", new DynamicVariable("Identyfikator", idFaktury, DynamicVariableValueType.IntegerNumber));
                }

                    //Ustawienie filtra
                w.FiltrAplikacji = "TrN_TrNID = " + idFaktury;

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn);
            }
            catch(Exception)
            {
                Wyloguj(loginService);
                throw;
            }

                //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukDomyslnyListyRejestrowKB(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.BRaLista;
                int kontID = 1;
                string filtrApp = "BRa_BRaID <= 3";

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Lista rejestrów kasowych/bankowych", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("NieAktywne", new DynamicVariable("NieAktywne", false, DynamicVariableValueType.Boolean, DynamicVariableValueRange.LocalPredefined));                

                    //Wykreowanie wydruku domyślnego
                Wydruk w = PobierzWydrukDomyslny(procID, kontID, RodzajWydruku.RegularnyWydruk, null);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                    //Ustawienie filtrowania
                w.FiltrAplikacji = filtrApp;

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

            //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukPodanyListyRejestrowKB(TypUrzadzenia _typUrzadzenia)
        {
            //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                string filtrApp = "BRa_BRaID <= 3";

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Lista rejestrów kasowych/bankowych", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("NieAktywne", new DynamicVariable("NieAktywne", false, DynamicVariableValueType.Boolean, DynamicVariableValueRange.LocalPredefined));

                    //Wykreowanie podanego wydruku
                bool czyWydrukStandardowy = true;
                int wydrukID = 20;
                Wydruk w = new Wydruk(czyWydrukStandardowy, wydrukID);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                    //Ustawienie filtrowania
                w.FiltrAplikacji = filtrApp;

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

            //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukDomyslnyListyFaktur(TypUrzadzenia _typUrzadzenia)
        {
                //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.FaLista;
                int kontID = 1;
                string filtrApp = "TrN_TrNID <= 3";
                string sortowanieApp = "TrN_NumerPelny";

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Lista faktur sprzedaży", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("OpeZakazCenyZak", new DynamicVariable("OpeZakazCenyZak", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("Oddzialowa", new DynamicVariable("Oddzialowa", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("MagZrdID", new DynamicVariable("MagZrdID", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));

                    //Wykreowanie obiektu wydruku domyślnego
                Wydruk w = PobierzWydrukDomyslny(procID, kontID, RodzajWydruku.RegularnyWydruk, zmienneDyn);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                    //Ustawienie filtrowania i sortowania
                w.FiltrAplikacji = filtrApp;
                w.SortowanieAplikacji = sortowanieApp;

                    //Ewentualne ustawienie zaznaczeń
                IList<object> zaznaczenia = null;
                if (w.TypObslugiZaznaczen != TypObslugiZaznaczenWydrukow.Brak)
                {
                        //Wykreowanie i dodanie zaznaczeń                    
                    zaznaczenia = new List<object>();
                    zaznaczenia.Add(1); //Dodajemy identyfikatory wybranych rekordów, np. wartości TrN_TrNID
                    zaznaczenia.Add(2);
                    //w.Zaznaczenia = zaznaczenia;
                }

                    //Ewentualne ustawienie parametrów kontekstu dla GenRap-a
                if (w.Typ == TypWydruku.GenRap)
                {
                    w.TypOkna = WydrukTypOkna.Lista;
                    w.GenRapReportType = 1; //Lista
                    w.GenRapNewReportInitObjects = "FakturaSpr";
                    w.GenRapContextAttribute = procID.ToString();
                }

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn, zaznaczenia);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

            //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        private void WykonajWydrukPodanyListyFaktur(TypUrzadzenia _typUrzadzenia)
        {
            //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                    //Kontekst wywołania
                ProcedureId procID = ProcedureId.FaLista;
                string filtrApp = "TrN_TrNID < 10";
                string sortowanieApp = "TrN_NumerPelny";

                    //Dodanie zmiennych dynamicznych takich, jakby przekazał presenter okna;
                    //robimy to już tutaj bo odwołania do tych zmiennych mogą występować w warunkach podłączeń wydruków
                IDictionary<string, DynamicVariable> zmienneDyn = new Dictionary<string, DynamicVariable>();
                zmienneDyn.Add("CDN_Tekst1", new DynamicVariable("CDN_Tekst1", "Lista faktur sprzedaży", DynamicVariableValueType.Text, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("OpeZakazCenyZak", new DynamicVariable("OpeZakazCenyZak", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("Oddzialowa", new DynamicVariable("Oddzialowa", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));
                zmienneDyn.Add("MagZrdID", new DynamicVariable("MagZrdID", 0, DynamicVariableValueType.IntegerNumber, DynamicVariableValueRange.LocalPredefined));

                    //Wykreowanie podanego wydruku
                bool czyWydrukStandardowy = true;
                int wydrukID = 0;
                if (typCGXWydruku == TypCGXWydruku.Crystal)
                    wydrukID = 1469; //Lista faktur / Wzór standard
                else if (typCGXWydruku == TypCGXWydruku.GenRap)
                    wydrukID = 1202; //Wydruki seryjne (GenRap) / Faktura VAT - Wzór standard
                else if (typCGXWydruku == TypCGXWydruku.XML)
                    wydrukID = 241; //Dokumenty zaznaczone / Wydruk dokumentów zaznaczonych (wybór wzorca wydr)
                //else if (typCGXWydruku == TypCGXWydruku.XML)
                //    wydrukID = 605; //Wydruki seryjne (GenRap) / Faktura VAT - Wzór standard
                Wydruk w = new Wydruk(czyWydrukStandardowy, wydrukID);
                if (w == null)
                {
                    Wyloguj(loginService);
                    return;
                }

                    //Ustawienie filtrowania i sortowania
                w.FiltrAplikacji = filtrApp;
                w.SortowanieAplikacji = sortowanieApp;

                    //Ewentualne ustawienie zaznaczeń
                IList<object> zaznaczenia = null;
                if (w.TypObslugiZaznaczen != TypObslugiZaznaczenWydrukow.Brak)
                {
                    //Wykreowanie i dodanie zaznaczeń                    
                    zaznaczenia = new List<object>();
                    zaznaczenia.Add(92); //Dodajemy identyfikatory wybranych rekordów, np. wartości TrN_TrNID
                    zaznaczenia.Add(102);
                    zaznaczenia.Add(114);
                    w.Zaznaczenia = zaznaczenia;
                }

                    //Ewentualne ustawienie parametrów kontekstu dla GenRap-a
                if (w.Typ == TypWydruku.GenRap)
                {
                    w.TypOkna = WydrukTypOkna.Lista;
                    w.GenRapReportType = 1; //Lista
                    w.GenRapNewReportInitObjects = "FakturaSpr";
                    w.GenRapContextAttribute = procID.ToString();
                }

                    //Ustawienie zmiennych dynamicznych
                //w.ZmienneDynamiczne = zmienneDyn;

                    //Wykonanie wydruku
                WykonajWydruk(w, _typUrzadzenia, zmienneDyn, zaznaczenia);
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

            //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        #endregion


        #region Testy całkowite

        private void WykonajWydrukDomyslnyRejestruKB()
        {
            //Zalogowanie do Optimy
            LoginService loginService = Zaloguj();

            try
            {
                //Kontekst wywołania
                ProcedureId procID = ProcedureId.BRaEdycja;
                int idRejestruKB = 1;

                //Wykreowanie obiektu wydruku domyślnego
                ManagerWydrukow mw = new ManagerWydrukow(procID, RodzajeWydrukow.TylkoRegularneWydruki);
                Wydruk w = mw.DomyslnyWydruk;
                if (w == null)
                {
                    MessageBox.Show("Brak wydruku domyślnego dla wywołania (" + procID + ", " + 1 + ")");
                    Wyloguj(loginService);
                    return;
                }

                //Ustawienie filtra
                w.FiltrAplikacji = "BRa_BRaID = " + idRejestruKB;

                //Wykonanie wydruku
                w.Wykonaj();
            }
            catch (Exception)
            {
                Wyloguj(loginService);
                throw;
            }

            //Wylogowanie z Optimy
            Wyloguj(loginService);
        }

        #endregion



        #region Pobieranie wydruku

        private Wydruk PobierzWydrukDomyslny(ProcedureId _procID, int _kontID, RodzajWydruku _rodzajWydruku)
        {
            return PobierzWydrukDomyslny(_procID, _kontID, _rodzajWydruku, null);
        }

        private Wydruk PobierzWydrukDomyslny(ProcedureId _procID, int _kontID, RodzajWydruku _rodzajWydruku, IDictionary<string, DynamicVariable> _zmienneDyn)
        {
            ManagerWydrukow mw = new ManagerWydrukow(_procID, _kontID,
                _rodzajWydruku == RodzajWydruku.FunkcjaDodatkowa ?
                RodzajeWydrukow.TylkoFunkcjeDodatkowe : RodzajeWydrukow.TylkoRegularneWydruki,
                null);

                //Dodajemy najpierw zmienne dynamiczna przekazane z okna, bo te mmogą być wykorzystywane w warunkach (podłączeń) wydruków
            mw.DodajZmienneDynamiczne(_zmienneDyn);

            Wydruk w = mw.DomyslnyWydruk;
            if(w == null)
                MessageBox.Show("Brak wydruku domyślnego dla wywołania (" + _procID + ", " + _kontID + ")");

            return w;
        }

        #endregion


        #region Wykonanie wydruku

        //Wykonanie na podane urządzenie
        void WykonajWydruk(Wydruk w, TypUrzadzenia _typUrzadzenia)
        {
            //WykonajWydruk(w, _typUrzadzenia, null, null);

            //W celach dydaktycznych zamiast wykomentowanej wyżej linii rozpisujemy najprostsze wywołania:

            if (_typUrzadzenia == TypUrzadzenia.Domyslne)
            {
                if (w.DomyslnyTypUrzadzenia == UrzadzenieWydrukuTyp.Podglad)
                    ZaladujCVWF();
                w.Wykonaj();
            }


            else if (_typUrzadzenia == TypUrzadzenia.Podglad)
            {
                ZaladujCVWF();
                w.Podgladaj();
            }
            else if (_typUrzadzenia == TypUrzadzenia.PodgladZKopiami)
            {
                UrzadzenieWydrukuParametryPodgladu pp = new UrzadzenieWydrukuParametryPodgladu();
                pp.ZKopiami = true;
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.Podglad, new UrzadzenieWydrukuParametry(pp));
                ZaladujCVWF();
                w.Wykonaj(u);
            }


            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaDomyslna)
                w.Drukuj();
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaDomyslnaIloscKopii)
                w.Drukuj(3);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInna)
                w.Drukuj("PDFCreator");
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInnaIloscKopii)
                w.Drukuj("PDFCreator", 3);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInnaOpcje)
            {
                PrinterSettings ps = new PrinterSettings();
                ps.Copies = 3;
                ps.Collate = false;
                ps.PrintRange = PrintRange.SomePages;
                ps.FromPage = 1;
                ps.ToPage = 2;
                ps.Duplex = Duplex.Horizontal;
                UrzadzenieWydrukuParametryDrukarki pd = new UrzadzenieWydrukuParametryDrukarki(ps);
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.DrukarkaInna, new UrzadzenieWydrukuParametry(pd));
                w.Wykonaj(u);
            }


            else if (_typUrzadzenia == TypUrzadzenia.Eksport)
            {
                if(w.Typ == TypWydruku.Crystal)
                    w.EksportujDoPliku(UrzadzenieWydrukuFormatEksportu.Excel, "d:\\1.xls");
                else
                    w.EksportujDoPliku(UrzadzenieWydrukuFormatEksportu.PortableDocFormat, "d:\\1.pdf");
            }
            else if (_typUrzadzenia == TypUrzadzenia.EksportOpcje)
            {
                UrzadzenieWydrukuParametryPlikuEksp pe = new UrzadzenieWydrukuParametryPlikuEksp();
                if (w.Typ == TypWydruku.Crystal)
                    pe.Format = UrzadzenieWydrukuFormatEksportu.WordForWindows;
                else
                    pe.Format = UrzadzenieWydrukuFormatEksportu.PortableDocFormat;
                pe.TypKatalogu = UrzadzenieWydrukuTypKataloguEksp.Tymczasowy;
                pe.NazwaPliku = w.NazwaZestawu + "_" + w.Nazwa;
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.PlikEksp, new UrzadzenieWydrukuParametry(pe));
                w.Wykonaj(u);
            }


            else if (_typUrzadzenia == TypUrzadzenia.Mail)
                w.WyslijMaila();
            else if (_typUrzadzenia == TypUrzadzenia.MailNaAdres)
                w.WyslijMaila("rafal.trolka@comarch.pl");
            else if (_typUrzadzenia == TypUrzadzenia.MailOpcje)
            {
                UrzadzenieWydrukuParametryEMaila pm = new UrzadzenieWydrukuParametryEMaila();
                pm.AdresDO = "piotr.gach@comarch.pl";
                pm.AdresDW = "rafal.trolka@comarch.pl";
                pm.Temat = "Testowy temat";
                pm.Tresc = "Testowa treść";
                pm.Zalaczniki.Add(new ZalacznikEMailWydruku("d:\\test1.txt", false));   //nie kasuj pliku po załączeniu
                pm.Zalaczniki.Add(new ZalacznikEMailWydruku("d:\\test2.txt", true));   //skasuj plik po załączeniu
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.EMail, new UrzadzenieWydrukuParametry(pm));
                w.Wykonaj(u);
            }

                //Czekamy, żeby GenRapowi przy wyjściu z programu nie skasować tempów z definicjami wydruku
                //(PrintOut wykonuje się asynchronicznie).
            if (w.Typ == TypWydruku.GenRap &&
                (_typUrzadzenia == TypUrzadzenia.Domyslne || _typUrzadzenia == TypUrzadzenia.DrukarkaDomyslna ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaDomyslnaIloscKopii ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaInna || _typUrzadzenia == TypUrzadzenia.DrukarkaInnaIloscKopii ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaInnaOpcje))
                PoczekajChwile();
        }

        //Wykonanie na podane urządzenie
        void WykonajWydruk(Wydruk w, TypUrzadzenia _typUrzadzenia, IDictionary<string, DynamicVariable> _zmienneDynamiczne)
        {            
            WykonajWydruk(w, _typUrzadzenia, _zmienneDynamiczne, null);
        }

        //Wykonanie na podane urządzenie
        void WykonajWydruk(Wydruk w, TypUrzadzenia _typUrzadzenia, IList<object> _zaznaczenia)
        {
            WykonajWydruk(w, _typUrzadzenia, null, _zaznaczenia);
        }

        //Wykonanie na podane urządzenie
        void WykonajWydruk(Wydruk w, TypUrzadzenia _typUrzadzenia, IDictionary<string, DynamicVariable> _zmienneDynamiczne, IList<object> _zaznaczenia)
        {
            if (_typUrzadzenia == TypUrzadzenia.Domyslne)
            {
                if (w.DomyslnyTypUrzadzenia == UrzadzenieWydrukuTyp.Podglad)
                    ZaladujCVWF();
                w.Wykonaj(_zmienneDynamiczne, _zaznaczenia);
            }


            else if (_typUrzadzenia == TypUrzadzenia.Podglad)
            {
                ZaladujCVWF();
                w.Podgladaj(_zmienneDynamiczne, _zaznaczenia);
            }
            else if (_typUrzadzenia == TypUrzadzenia.PodgladZKopiami)
            {
                UrzadzenieWydrukuParametryPodgladu pp = new UrzadzenieWydrukuParametryPodgladu();
                pp.ZKopiami = true;
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.Podglad, new UrzadzenieWydrukuParametry(pp));
                ZaladujCVWF();
                w.Wykonaj(u, _zmienneDynamiczne, _zaznaczenia);
            }


            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaDomyslna)
                w.Drukuj(_zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaDomyslnaIloscKopii)
                w.Drukuj(3, _zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInna)
                w.Drukuj("PDFCreator", _zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInnaIloscKopii)
                w.Drukuj("PDFCreator", 3, _zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.DrukarkaInnaOpcje)
            {
                PrinterSettings ps = new PrinterSettings();
                ps.Copies = 3;
                ps.Collate = false;
                ps.PrintRange = PrintRange.SomePages;
                ps.FromPage = 1;
                ps.ToPage = 2;
                ps.Duplex = Duplex.Horizontal;
                UrzadzenieWydrukuParametryDrukarki pd = new UrzadzenieWydrukuParametryDrukarki(ps);
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.DrukarkaInna, new UrzadzenieWydrukuParametry(pd));
                w.Wykonaj(u, _zmienneDynamiczne, _zaznaczenia);
            }


            else if (_typUrzadzenia == TypUrzadzenia.Eksport)
            {
                if (w.Typ == TypWydruku.Crystal)
                    w.EksportujDoPliku(UrzadzenieWydrukuFormatEksportu.Excel, "d:\\1.xls", _zmienneDynamiczne);
                else
                    w.EksportujDoPliku(UrzadzenieWydrukuFormatEksportu.PortableDocFormat, "d:\\1.pdf", _zmienneDynamiczne);
            }
            else if (_typUrzadzenia == TypUrzadzenia.EksportOpcje)
            {
                UrzadzenieWydrukuParametryPlikuEksp pe = new UrzadzenieWydrukuParametryPlikuEksp();
                if(w.Typ == TypWydruku.Crystal)
                    pe.Format = UrzadzenieWydrukuFormatEksportu.WordForWindows;
                else
                    pe.Format = UrzadzenieWydrukuFormatEksportu.PortableDocFormat;
                pe.TypKatalogu = UrzadzenieWydrukuTypKataloguEksp.Tymczasowy;
                pe.NazwaPliku = w.NazwaZestawu + "_" + w.Nazwa;
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.PlikEksp, new UrzadzenieWydrukuParametry(pe));
                w.Wykonaj(u, _zmienneDynamiczne, _zaznaczenia);
            }


            else if (_typUrzadzenia == TypUrzadzenia.Mail)
                w.WyslijMaila(_zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.MailNaAdres)
                w.WyslijMaila("rafal.trolka@comarch.pl", _zmienneDynamiczne, _zaznaczenia);
            else if (_typUrzadzenia == TypUrzadzenia.MailOpcje)
            {
                UrzadzenieWydrukuParametryEMaila pm = new UrzadzenieWydrukuParametryEMaila();
                pm.AdresDO = "piotr.gach@comarch.pl";
                pm.AdresDW = "rafal.trolka@comarch.pl";
                pm.Temat = "Testowy temat";
                pm.Tresc = "Testowa treść";
                pm.Zalaczniki.Add(new ZalacznikEMailWydruku("d:\\login.txt", false));   //nie kasuj pliku po załączeniu
                pm.Zalaczniki.Add(new ZalacznikEMailWydruku("d:\\login2.txt", true));   //skasuj plik po załączeniu
                UrzadzenieWydruku u = new UrzadzenieWydruku(UrzadzenieWydrukuTyp.EMail, new UrzadzenieWydrukuParametry(pm));
                w.Wykonaj(u, _zmienneDynamiczne, _zaznaczenia);
            }

                //Czekamy, żeby GenRapowi przy wyjściu z programu nie skasować tempów z definicjami wydruku
                //(PrintOut wykonuje się asynchronicznie).
            if (w.Typ == TypWydruku.GenRap &&
                (_typUrzadzenia == TypUrzadzenia.Domyslne || _typUrzadzenia == TypUrzadzenia.DrukarkaDomyslna ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaDomyslnaIloscKopii ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaInna || _typUrzadzenia == TypUrzadzenia.DrukarkaInnaIloscKopii ||
                _typUrzadzenia == TypUrzadzenia.DrukarkaInnaOpcje))
                PoczekajChwile();
        }

        #endregion



        #region Logowanie

        private LoginService Zaloguj()
        {
            LoginService loginService = new LoginService();
            loginService.Login(logowanie_OperatorKod, logowanie_OperatorHaslo, logowanie_NazwaFirmy, logowanie_Moduly);
            return loginService;
        }

        private void Wyloguj(LoginService _loginService)
        {
            _loginService.Logout();
        }

        #endregion


        #region Narzędzia

        private void ZaladujCVWF()
        {
            //Poniżej myk żeby załadował się Common.View.WinForms.dll; w przeciwnym przypadku w CreatePrintViewIfNeeded
            //Type type = Type.GetType("Optima.Common.View.WinForms.Wydruki.PrintViewCOMImplementation, Common.View.WinForms");
            //zwróci null-a
            Optima.Common.View.WinForms.Wydruki.PrintViewCOMImplementation pvi = new Optima.Common.View.WinForms.Wydruki.PrintViewCOMImplementation();

            //System.Reflection.Assembly.LoadFrom("D:\\O!interfejs\\Optima\\Interface\\Bin\\Debug\\Common.View.WinForms.dll");
            //System.Reflection.Assembly.Load("D:\\O!interfejs\\Optima\\Interface\\Bin\\Debug\\Common.View.WinForms.dll");
            //System.Reflection.Assembly.LoadFile("D:\\O!interfejs\\Optima\\Interface\\Bin\\Debug\\Common.View.WinForms.dll");

            //IPrintView pv = null;
            //pv = Optima.Common.Logic.Services.AnalizyWydrukiService.CreatePrintViewIfNeeded(pv);
        }

        private void PoczekajChwile()
        {
            System.Threading.Thread.Sleep(10000);
        }

        #endregion

    }
}
