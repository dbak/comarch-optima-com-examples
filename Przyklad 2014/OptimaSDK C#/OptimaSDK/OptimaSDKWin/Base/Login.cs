﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Optima.Common;
using Optima.Common.Logic.Services;
using OptimaSDKWin.Properties;

namespace OptimaSDKWin.Base
{
	internal class Login
	{
		private const string equ_LogowanieNazwaFirmy = "PREZENTACJA_KH";
		private const string equ_LogowanieOperatorKod = "ADMIN";
		private const string equ_LogowanieOperatorHaslo = "";
		private LoginService loginService;

		public LoginService LogIn()
		{
			loginService = new LoginService();
			var logowanieModuly = new ModuleCollection { Module.Handel, Module.KasaBank, Module.CRM };
			var logowanieNazwaFirmy = Settings.Default.NazwaFirmy;	
			var logowanieOperatorKod = Settings.Default.KodOperatora;
			var logowanieOperatorHaslo = Settings.Default.Haslo;
			if (string.IsNullOrEmpty(logowanieNazwaFirmy)) logowanieNazwaFirmy = equ_LogowanieNazwaFirmy;
			if (string.IsNullOrEmpty(logowanieOperatorKod)) logowanieOperatorKod = equ_LogowanieOperatorKod;
			if (string.IsNullOrEmpty(logowanieOperatorHaslo)) logowanieOperatorHaslo = equ_LogowanieOperatorHaslo;
			loginService.Login(logowanieOperatorKod, logowanieOperatorHaslo, logowanieNazwaFirmy, logowanieModuly);
			return loginService;
		}

		public void LogOut()
		{
			if (loginService != null) loginService.Logout();
		}
	}
}
