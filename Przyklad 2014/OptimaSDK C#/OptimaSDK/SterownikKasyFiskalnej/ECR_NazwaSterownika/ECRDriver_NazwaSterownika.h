// ECRDriver_NazwaSterownika.h : Declaration of the CECRDriver_NazwaSterownika

#pragma once
#include "resource.h"       // main symbols

#include "ECR_NazwaSterownika.h"
#include "ECRDriverCategory.h"
#include <string>
#include <map>
#include <vector>


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CECRDriver_NazwaSterownika

class ATL_NO_VTABLE CECRDriver_NazwaSterownika :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CECRDriver_NazwaSterownika, &CLSID_ECRDriver_NazwaSterownika>,
	public ISupportErrorInfo,
	public IECRDriver_NazwaSterownika
{
public:
	CECRDriver_NazwaSterownika();
	~CECRDriver_NazwaSterownika();

DECLARE_REGISTRY_RESOURCEID(IDR_ECRDriver_NazwaSterownika)


BEGIN_COM_MAP(CECRDriver_NazwaSterownika)
	COM_INTERFACE_ENTRY(IECRDriver)
	COM_INTERFACE_ENTRY(IECRDriver2)
	COM_INTERFACE_ENTRY(IECRDriver_NazwaSterownika)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

BEGIN_CATEGORY_MAP(CECRDriver_NazwaSterownika)
    IMPLEMENTED_CATEGORY(CATID_ECRDriverCategory)
END_CATEGORY_MAP() 

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
    HRESULT STDMETHODCALLTYPE clear(void);
    HRESULT STDMETHODCALLTYPE displayConfigDialog(void);
    HRESULT STDMETHODCALLTYPE get_Config(BSTR *);
    HRESULT STDMETHODCALLTYPE put_Config(BSTR);
    HRESULT STDMETHODCALLTYPE put_Port(BSTR);
    HRESULT STDMETHODCALLTYPE get_PrettyConfig(BSTR *);
    HRESULT STDMETHODCALLTYPE put_Id(int);
    HRESULT STDMETHODCALLTYPE downloadTaxRates(void);
    HRESULT STDMETHODCALLTYPE getTaxRate(int,IECRTaxRate **);
    HRESULT STDMETHODCALLTYPE setTaxRate(IECRTaxRate *);
    HRESULT STDMETHODCALLTYPE uploadTaxRates(void);
    HRESULT STDMETHODCALLTYPE downloadAllItems(void);
    HRESULT STDMETHODCALLTYPE downloadItemsByType(ItemType);
    HRESULT STDMETHODCALLTYPE getItem(BSTR,IECRItem **);
    HRESULT STDMETHODCALLTYPE setItem(IECRItem *);
    HRESULT STDMETHODCALLTYPE uploadItemsByType(ItemType);
    HRESULT STDMETHODCALLTYPE uploadAllItems(void);
    HRESULT STDMETHODCALLTYPE get_ItemNumbers(SAFEARRAY **);
    HRESULT STDMETHODCALLTYPE createDailyReport(void);
    HRESULT STDMETHODCALLTYPE readParameters(void);
    HRESULT STDMETHODCALLTYPE getNumber(ItemType,int *);
    HRESULT STDMETHODCALLTYPE getMaxId(ItemType,int *);
    HRESULT STDMETHODCALLTYPE getNameLength(ItemType,int *);
    HRESULT STDMETHODCALLTYPE get_Date(unsigned long* pVal);
    HRESULT STDMETHODCALLTYPE supportsFeature(FeatureType ft, int* pVal);
    HRESULT STDMETHODCALLTYPE importItems(ItemType);
    HRESULT STDMETHODCALLTYPE importSales(ItemType);
private:
	std::map<std::wstring, std::wstring> configMap;
};

OBJECT_ENTRY_AUTO(__uuidof(ECRDriver_NazwaSterownika), CECRDriver_NazwaSterownika)
