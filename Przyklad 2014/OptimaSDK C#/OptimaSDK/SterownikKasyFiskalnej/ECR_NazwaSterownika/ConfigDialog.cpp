// ConfigDialog.cpp : Implementation of CConfigDialog

#include "stdafx.h"
#include "ConfigDialog.h"
#include "ECRDriver_NazwaSterownika.h"
#include "Exception.h"

using namespace std;

// CConfigDialog
CConfigDialog::CConfigDialog()
{
    configMap=NULL;
}

CConfigDialog::~CConfigDialog()
{
}

LRESULT CConfigDialog::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    CAxDialogImpl<CConfigDialog>::OnInitDialog(uMsg, wParam, lParam, bHandled);
    bHandled = TRUE;
    RECT rect;

    if(configMap==NULL)
        return 0;

    CWindow bitrateCombo=GetDlgItem(IDC_BITRATE);
    bitrates.clear();
    bitrates.push_back(L"1200");
    bitrates.push_back(L"2400");
    bitrates.push_back(L"4800");
    bitrates.push_back(L"9600");
    bitrates.push_back(L"19200");
    bitrates.push_back(L"38400");
    bitrates.push_back(L"57600");
    bitrates.push_back(L"115200");
    for(std::vector<std::wstring>::iterator it=bitrates.begin();it!=bitrates.end();it++)
        bitrateCombo.SendMessageW(CB_INSERTSTRING, -1, (LPARAM)it->c_str());
    bitrateCombo.GetWindowRect(&rect);
    bitrateCombo.SetWindowPos(NULL, 0, 0, rect.right-rect.left, 200, SWP_NOMOVE | SWP_NOZORDER);
    
	for(unsigned int i=0;i<bitrates.size();i++)
        if(bitrates[i]==(*configMap)[L"bitrate"])
            bitrateCombo.SendMessage(CB_SETCURSEL, i, 0);

    return 1;  // Let the system set the focus
}

LRESULT CConfigDialog::OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    unsigned int selected;

    CWindow bitrateCombo=GetDlgItem(IDC_BITRATE);
    selected=(int)bitrateCombo.SendMessageW(CB_GETCURSEL, 0, 0);
    if(selected<bitrates.size())
        configMap->operator [](L"bitrate")=bitrates[selected];

    EndDialog(wID);
    return 0;
}

LRESULT CConfigDialog::OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

void CConfigDialog::setConfig(std::map<std::wstring, std::wstring>* cMap)
{
    configMap=cMap;
}
