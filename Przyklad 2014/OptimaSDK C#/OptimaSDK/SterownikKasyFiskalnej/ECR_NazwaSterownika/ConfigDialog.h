// ConfigDialog.h : Declaration of the CConfigDialog

#pragma once

#include "resource.h"       // main symbols

#include <atlhost.h>
#include <map>
#include <string>
#include <vector>

// CConfigDialog

class CConfigDialog : 
	public CAxDialogImpl<CConfigDialog>
{
public:
	CConfigDialog();
	~CConfigDialog();

	enum { IDD = IDD_CONFIGDIALOG };

BEGIN_MSG_MAP(CConfigDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	CHAIN_MSG_MAP(CAxDialogImpl<CConfigDialog>)
END_MSG_MAP()

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    void setConfig(std::map<std::wstring, std::wstring>* cMap);

    std::map<std::wstring, std::wstring>* configMap;
    std::vector<std::wstring> bitrates;
};


