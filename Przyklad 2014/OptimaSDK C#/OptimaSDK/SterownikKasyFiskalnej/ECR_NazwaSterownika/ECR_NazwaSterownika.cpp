// ECR_NazwaSterownika.cpp : Implementation of DLL Exports.


#include "stdafx.h"
#include "resource.h"
#include "ECR_NazwaSterownika.h"
#include "dlldatax.h"
#include <string>
using namespace std;

class CECR_NazwaSterownikaModule : public CAtlDllModuleT< CECR_NazwaSterownikaModule >
{
public :
	DECLARE_LIBID(LIBID_ECR_NazwaSterownikaLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ECR_NazwaSterownika, "{D12EE87D-A1BB-4b85-8CAF-371FBF9DF10E}")
};

CECR_NazwaSterownikaModule _AtlModule;


#ifdef _MANAGED
#pragma managed(push, off)
#endif

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
#ifdef _MERGE_PROXYSTUB
    if (!PrxDllMain(hInstance, dwReason, lpReserved))
        return FALSE;
#endif
	hInstance;
    return _AtlModule.DllMain(dwReason, lpReserved); 
}

#ifdef _MANAGED
#pragma managed(pop)
#endif




// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
#ifdef _MERGE_PROXYSTUB
    HRESULT hr = PrxDllCanUnloadNow();
    if (hr != S_OK)
        return hr;
#endif
    return _AtlModule.DllCanUnloadNow();
}


// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
#ifdef _MERGE_PROXYSTUB
    if (PrxDllGetClassObject(rclsid, riid, ppv) == S_OK)
        return S_OK;
#endif
    return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}

void addEntry(HKEY hKey, wstring key, wstring value)
{
    RegSetValueEx(hKey, key.c_str(), NULL, REG_SZ, (BYTE*)value.c_str(), (DWORD)value.length()*2);
}

// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    HRESULT hr = _AtlModule.DllRegisterServer();
#ifdef _MERGE_PROXYSTUB
    if (FAILED(hr))
        return hr;
    hr = PrxDllRegisterServer();
#endif
    if (FAILED(hr))
        return hr;
	HKEY hKey;
    long lResult = RegOpenKeyEx(HKEY_CLASSES_ROOT, L"CLSID\\{27B4147F-EAA6-46dc-8E7D-50A98FE1F136}", 0, KEY_ALL_ACCESS, &hKey);
	if (lResult != ERROR_SUCCESS)
        return E_FAIL;
    addEntry(hKey, L"Version", L"1.0.0");
    addEntry(hKey, L"FriendlyName", L"_NazwaSterownika");
    addEntry(hKey, L"Description", L"Sterownik kas _NazwaSterownika");
	return hr;
}


// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
	HRESULT hr = _AtlModule.DllUnregisterServer();
#ifdef _MERGE_PROXYSTUB
    if (FAILED(hr))
        return hr;
    hr = PrxDllRegisterServer();
    if (FAILED(hr))
        return hr;
    hr = PrxDllUnregisterServer();
#endif
	return hr;
}

