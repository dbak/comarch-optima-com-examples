// ECRItem_NazwaSterownika.cpp : Implementation of CECRItem_NazwaSterownika

#include "stdafx.h"
#include "ECRItem_NazwaSterownika.h"
#include "Exception.h"
#include <string>
#include <comutil.h>

using namespace std;
// CECRItem_NazwaSterownika

STDMETHODIMP CECRItem_NazwaSterownika::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IECRItem_NazwaSterownika,
		&IID_IECRItem
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

CECRItem_NazwaSterownika::CECRItem_NazwaSterownika()
{

}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Id(BSTR *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Id(BSTR val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Name(BSTR *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Name(BSTR val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Price(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Price(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_ManualPrice(boolean *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_ManualPrice(boolean){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_ProgrammedPrice(boolean *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_ProgrammedPrice(boolean){return S_OK;}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Group(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Group(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Items(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Items(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Amount(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Amount(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Value(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Value(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Type(ItemType *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Type(ItemType val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_SaleMode(SaleType *){return S_OK;}
HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_SaleMode(SaleType){return S_OK;}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Vat(int *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Vat(int val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Ean(BSTR *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Ean(BSTR val)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Driver(IECRDriver* val)
{
	return S_OK;
}


HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::get_Text(BSTR *pVal)
{
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CECRItem_NazwaSterownika::put_Text(BSTR val)
{
	return S_OK;
}
