// Poni�szy przyk�ad loguje si� do aplikacji, 
// nast�pnie otwiera plik tekstowy BO.TXT, w kt�rm:
// pierwsza linia to data
// druga linia to kod kontrahenta ( dla BOM zawsze powinien byc to kontrahent nieokreslony )
// trzecia linia to symbol magazynu
// a w nast�pnych liniach znajduj� si� pozycje dokumentu w postaci: KOD TOWARU,ILO��,CENA (Ilo�� i Cena s� omitowalne)
// po znaku ; znajduj� si� komentarze


/*<COM_DOK>
<OPIS>
   Import bilansu otwarcia magazynu
   Do dodawania dokument�w BO za pomc� skryptu konieczne s� licencje na modu� magazynowy.
</OPIS>
<Uruchomienie>Do importu konieczny jest plik importu naw� pliku podaje si� w kodzie skryptu</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IDefinicjaDokumentu</Interfejs>
<Interfejs> IMagazyn</Interfejs>
<Interfejs> IKontrahent</Interfejs>
<Interfejs> ITowar</Interfejs>
<Interfejs> IDokumentHaMag</Interfejs>
<Interfejs> IElementHaMag</Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/



// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rDokumentHaMag	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury


// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	DodajFakture ()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function DodajFakture() {

	var fso = new ActiveXObject("Scripting.FileSystemObject")
	var f = fso.OpenTextFile("BO.TXT") // Plik tekstowy
	var l		// Linia czytana z pliku tekstowego

 
	rDokumentHaMag = rSesja.CreateObject("CDN.DokumentyHaMag").AddNew()

// Dokument BOM
	rDokumentHaMag.Rodzaj		= 310000
	rDokumentHaMag.TypDokumentu	= 310

	var rNumerator = rDokumentHaMag.Numerator

	var rDokDef = rSesja.CreateObject("CDN.DefinicjeDokumentow").Item("DDf_Symbol='BOM'")
	rNumerator.DefinicjaDokumentu = rDokDef

// Dokument utworzony do bufora 
	rDokumentHaMag.Bufor = 1



// Data
	l = UsunKomentarz(f.ReadLine())
	rDokumentHaMag.DataDok		= l

// Kontrahent 
	l = UsunKomentarz(f.ReadLine()) 
	var rKontrahent = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_Kod='" + l + "'") 
	rDokumentHaMag.Podmiot = rKontrahent

// Magazyn
	l = UsunKomentarz(f.ReadLine()) 
	var 	rMagazyn = rSesja.CreateObject("CDN.MAGAZYNY").Item("Mag_Symbol='"+ l +"'")
	rDokumentHaMag.MagazynZrodlowy = rMagazyn 



	rPozycje = rDokumentHaMag.Elementy
	while (!f.AtEndOfStream) {
		l = UsunKomentarz(f.ReadLine())
		DodajPozycje(l)

		}
// Zapisuj� zmiany
try {	rSesja.Save()

	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas wystawiania faktury " + e.description)
	}

}
//-----------------------------------------
function DodajPozycje(linia) {
var i, j, Twr, Ilosc, Cena
i = linia.search(",")
if (i!=-1) {
	// Je�li w lini by� przecinek, to oddzielam kod od ilo�ci
	Twr = linia.substr(0,i)
	j = linia.substr(i+1).search(",")
	if (j!=-1) {
		Ilosc = linia.substr(i+1,j)
		Cena  = linia.substr(i+j+2)
		}
	else {
		Ilosc = linia.substr(i+1)
		Cena  = 0
		}
	}
else {
	Twr = linia
	Ilosc = 1
	Cena = 0
	}

//WScript.Echo("tw : "+Twr+" ilosc : "+Ilosc)

// Sprawdzam, czy jest w og�le taki towar
try {
	var rTowar = rSesja.CreateObject("CDN.Towary").Item("Twr_Kod='" + Twr + "'")
	}
catch (e) {
	WScript.Echo("Nie uda�o si� pobra� towaru [" + Twr + "]. " + e.description)
	return
	}

//WScript.Echo("tw : "+Twr+" ilosc : "+Ilosc)

// Dodaj� pozycj� do faktury
var rPozycja = rPozycje.AddNew()

//WScript.Echo("TEST 1")

// Podstawiam towar
rPozycja.Towar = rTowar
//WScript.Echo("TEST 2")

// Podstawiam ilo��
rPozycja.IloscJM = Ilosc
//WScript.Echo("TEST 3")

// Podstawiam cen�
if (Cena)
	{
	Cena = Cena.replace(',','')
	Cena = Cena.replace('\.',',')
	rPozycja.CenaT = Cena
	}
//WScript.Echo("TEST 4")
}

//-----------------------------------------
function UsunKomentarz(s) {
if (s.search(";")!=-1)
	s = s.substr(0,s.search(";"))
return s
}
