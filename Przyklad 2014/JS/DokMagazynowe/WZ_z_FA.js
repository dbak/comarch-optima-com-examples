//
//Samo otwarcie po��czenia z optim� nast�puje w standardowy spos�b:
// 


/*<COM_DOK>
<OPIS>
   Wygenerowania do dokumentu WZ faktury sprzeda�y
   Do generacji dokument�w WZ za pomc� skryptu konieczne s� licencje na modu� magazynowy.

</OPIS>
<Uruchomienie>W skrypcie nale�y poda� ID dokumentu WZ k�ry b�dzie przekszta�cany do FA </Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IDefinicjaDokumentu</Interfejs>
<Interfejs> ISerwisHaMag</Interfejs>


<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

var rSet = new ActiveXObject("ADODB.Recordset")

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu

	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )


	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	
	// Przekszta�cenie dokumentu WZ o Trn_TrnID=2837 do dok. FA
	Przeksztalc_WZ_do_FA( 16 ) 

	rApp.UnLockApp()
	WScript.Echo("KONIEC")
}


function Przeksztalc_WZ_do_FA(ID_DOK)
{
try{
	// Utworzenie recordsetu pomocniczego zwieraj�cego ID dokument�w RO dla kt�rych maj� 
	// by� generowana jedna wsp�lna faktura.

	var MAGAZYN_ID =1

  	var fields = rSet.Fields
	fields.Append('ID', 3)

	rSet.open()
        rSet.addNew()
        rSet.Fields('ID').Value = ID_DOK

	// Wykreowanie obiektu serwisu:
        var Serwis = rSesja.CreateObject("CDN.SerwisHaMag")

	// Samo gemerowanie dok. FA
        Serwis.AgregujDokumenty(306, rSet, MAGAZYN_ID, 302) 

}
catch(e)
{
	WScript.Echo("Konwersja nie powioda�a si� : " + e.description)
}
}