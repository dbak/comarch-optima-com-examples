//////////////////////////////////////////////////////////////////////
//
//
//            Dodawanie kontrahenta i kontaktu w module CRM
//	      z lista dokument�w
//
//
//////////////////////////////////////////////////////////////////////


/*<COM_DOK>
<OPIS>
	Dodawanie kontrahenta i kontaktu w module CRM z list� dokument�w
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IKontrahent </<Interfejs>
<Interfejs> IKONTAKT </<Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/



// Standardowe utworzenie Aplikacji 

var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var e			// Wyjatek



// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 



////////////////////////////////////////////////////////////////////
// Wywo�aniem logowania

	// parametry logowania
	var OPERATOR = 'ADMIN'
	var HASLO    = ''
	var BAZA     = 'DEMO'

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	

	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

////////////////////////////////////////////////////////////////////



	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
try {
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {


try {

///////////////////////////////////////////////////////////////
// Utworzenie nowego obiektu kontrahenta w koleckji biznesowej 

 var kontrahenci = rSesja.CreateObject('CDN.KONTRAHENCI');
 var kontrahent =  kontrahenci.AddNew(); 

 kontrahent.Akronim = 'SA'
 kontrahent.Nazwa1  = 'SA';


//////////////////////////////////////////////////////////////
// Zapsi danych do obiektu ADRES

 kontrahent.Adres.Miasto = 'Cz�stochowa'
 kontrahent.Adres.Ulica  = 'Jasnog�rska'
 kontrahent.Adres.NrDomu = '1'

// Zapis zmian do bazy 
rSesja.Save()


///////////////////////////////////////////////////////////////
// Dodanie kontaktu, kontakt jest zapisany do bufora.
// Do kontaktu dodawanya jest lista dokument�w.
// Dodawany dokument na li�cie jest wpisem do RVAT o id 1 . 
// Musi istniec w bazie w kt�rej wybieramy na pocz�tku skryptu taki dokument

 var kontakty   = rSesja.CreateObject('CDN.KONTAKTY');
 var rkontakt    = kontakty.AddNew();

// Przypisanie utworzeonego kontrahenta 


 rkontakt.PodmiotTyp =  kontrahent.PodmiotTyp 
 rkontakt.PodID      =  kontrahent.ID

 var rDokument  = rkontakt.Dokumenty


// Dodanie nowego elementu do kolekcji

 var nr = rDokument.AddNew()

// Dodanie dokumentu z RVAT

 nr.DokID   = 1		
 nr.Rodzaj  = 2		// 1- dok zewnetrzny 2 - dok wewnerzny
 nr.Typ     = 999	// 999 - RVAT; 700 - kontakt; Dokumenty Mag - Typ z tabeli TraNag


// Zapis zmian do bazy 
rSesja.Save()

	}
catch (e) {	WScript.Echo(" B��d : " + e ) }

///////////////////////////////////////////////////////////////////////
// Komunikaty koncowe

	WScript.Echo("Zalogowany")
	Zamknij()
	WScript.Echo("KONIEC")
	}


///////////////////////////////////////////////////////////////////////
function Zamknij() {
	try {
	//Zamykam po��czenie do bazy danych
		rApp.UnLockApp()
	}

	catch (e) {}
	}
///////////////////////////////////////////////////////////////////////


