
/*<COM_DOK>
<OPIS>
	Dodanie nowego Operatora, z ustawieniem pustego has�a
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IOperator </Interfejs>


<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/



// Standardowe utworzenie Aplikacji 
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

var e			// Wyjatek


// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 



////////////////////////////////////////////////////////////////////
//Utworzenie obiektu loginu

	rLogin	= rApp.LockApp(1)	

var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

	// Logowanie z podanim bazy
	rLogin=   rApp.Login(OPERATOR,HASLO,BAZA)

////////////////////////////////////////////////////////////////////

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
try {
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {


	WScript.Echo("Zalogowany")
	
	DodajOperatora()
WScript.Echo("Dodano operatora o kodzie JK")

	UsunOperatora()
WScript.Echo("Usuni�to operatora o kodzie JK")

	Zamknij()
	WScript.Echo("KONIEC")
	}



///////////////////////////////////////////////////////////////////////
function DodajOperatora()
{

try
{


// Otwarcie kolekcji opertor�w

	var Operatorzy			= rSesja.CreateObject("CDN.Operatorzy")

// Kolekcja operator�w mo�e by� przeszukiwana lub iterowana. 


// Dodanie operatora

	var Operator			= Operatorzy.AddNew() 

	Operator.Nieaktywny		= 0;
	Operator.Kod			= "JK";
	Operator.Nazwa1			= "Jan Kowalski";
	Operator.Opis			= "Grupa uczniowie";

//
// Has�a zapisywane w bazie i obiekcie s� szyfrowane. Podne ponizszego has�a i hasla kontrolnego odpowiada 
// pustemu has�u przy logowaniu dla operatora.
//

	Operator.Haslo			= "qHtklN0wNDY"
	Operator.HasloChk		= "Er"

// Ustawienie modu��w
	Operator.PelneMenu =1
	Operator.ModulKB   =1
	Operator.ModulMag  =1

// Zapisanie operatora
	rSesja.Save() 



}
catch (e)
{
	WScript.Echo("Wyj�tek: " + e.description + ", " + e.number);
} 
}

///////////////////////////////////////////////////////////////////////
function UsunOperatora()
{
// Usuniecie operatora 

try
{

var Operatorzy			= rSesja.CreateObject("CDN.Operatorzy")


var Operator = Operatorzy.Item("Ope_Kod = 'JK'") 
Operatorzy.Delete( Operator )

	rSesja.Save() 

}
catch (e)
{
	WScript.Echo("Wyj�tek: " + e.description + ", " + e.number);
}
}

///////////////////////////////////////////////////////////////////////
function Zamknij() {
	try {
	//Zamykam po��czenie do bazy danych
		rApp.UnLockApp()
	}

	catch (e) {}
	}
///////////////////////////////////////////////////////////////////////


