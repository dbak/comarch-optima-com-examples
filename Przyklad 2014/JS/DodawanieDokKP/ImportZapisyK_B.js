/*<COM_DOK>
<OPIS>
Import zapis�w kasowo bankowych z pliku.
</OPIS>
<Uruchomienie>Nazwa pliku z kt�rego wykonywany jest import</Uruchomienie>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> DefinicjeDokumentow</Interfejs> 
<Interfejs> Rachunki</Interfejs> 
<Interfejs> RaportyKB</Interfejs> 
<Interfejs> Kontrahenci</Interfejs> 
<Interfejs> ZapisyKB</Interfejs> 

<Osoba>MB</Osoba>
<OPT_VER>10.0.1</OPT_VER>
</COM_DOK>*/


// Standardowe utworzenie Aplikacji i dokumentu
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rZapis 		// Obiekt zapisu kasowego
var rRejestr		// Obiekt rejestru kasowego
var rRaport		// Obiekt raportu kasowego

var i, j,k,l,  Knt,Data, Kier,Wartosc,Reszta,Seria, Raport,Rachunek

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {

try {
	var fso = new ActiveXObject("Scripting.FileSystemObject")
	var f = fso.OpenTextFile("KP.TXT") // Plik tekstowy
	var l		// Linia czytana z pliku tekstowego

// Rachunek i Raport do kt�rego maj� by� dodawane dokumenty KB

	l 	  = UsunKomentarz(f.ReadLine())
	Rachunek  = l.substr(0,l.search(",") )
	Raport    = l.substr( l.search(",")+1 )

// Odczyt pliku w pentli

while (!f.AtEndOfStream)
{

l = UsunKomentarz(f.ReadLine())
i = l.search(",")

if (i!=-1) {
	// Je�li w lini by� przecinek
	Knt = l.substr(0,i)
	Reszta = l.substr(i+1) 

	i = Reszta.search(",")
	Data  = Reszta.substr(0,i)
	Reszta = Reszta.substr(i+1) 

	i = Reszta.search(",")
	Seria  = Reszta.substr(0,i)
	Reszta = Reszta.substr(i+1) 



	i = Reszta.search(",")
	Kier  = Reszta.substr(0,i)
	Reszta = Reszta.substr(i+1) 	

	Wartosc = Reszta

//WScript.Echo( Knt +"\n"+ Data +"\n"+ Kier +"\n"+ Wartosc +"\n"+ Seria )

	DodajZapisKB ()

	}

}
}
catch (e) {
	WScript.Echo("Wyst�pi� b��d " + e.description)
	}

	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function DodajZapisKB() {
try {
  rZapis = rSesja.CreateObject("CDN.ZapisyKB").AddNew()

  var rNumerator = rZapis.Numerator
	try {

		var rDokDef = rSesja.CreateObject("CDN.DefinicjeDokumentow").Item("DDf_Symbol='"+Seria+"'")   //"DDf_DDfID = 5")
		rNumerator.DefinicjaDokumentu = rDokDef

		}
	catch (e) {WScript.Echo("DDF " + e.description)}

	var rRachunek = rSesja.CreateObject("CDN.Rachunki").Item("Bra_Akronim = '" + Rachunek + "'") 
	rZapis.Rachunek = rRachunek
	rNumerator.Rejestr = rRachunek.Symbol


	var rRaport = rSesja.CreateObject("CDN.RaportyKB").Item("BRp_NumerPelny = '" + Raport +"'") 
	rZapis.RaportKB = rRaport

	rZapis.DataDok = Data

	rZapis.Kwota   = Wartosc


//[MB] Nale�y okreslic kierunek operacji

 	rZapis.Kierunek =  Kier 

	var rKontrahent = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_KOD ='" + Knt + "'" ) 
	rZapis.Podmiot = rKontrahent

// Zapisuj� zmiany
	rSesja.Save()
	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas wystawiania zapisu " + e.description)
	}

}


//-----------------------------------------
function UsunKomentarz(s) {
if (s.search(";")!=-1)
	s = s.substr(0,s.search(";"))
return s
}