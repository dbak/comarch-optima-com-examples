
/*<COM_DOK>
<OPIS>
	2008-10-31 

	Rozrachunki KH
	Przyk�ad wykorzystania obiekt�w COM.
	Rozrachunki na kontach ksi�gowych z walut� systemow�.
	Przyk�ad obejmuje tematy:
		- dodawanie dekret�w
		- rozliczanie dekret�w
		- usuwanie rozlicze�

	Przyk�ad przygotowany jest do pracy na nowo utworzonej bazie danych, 
	skonfigurowanej warto�ciami domy�lnymi dla ksi�gowo�ci kontowej.

</OPIS>
<Interfejs> IKonfiguracja </Interfejs>
<Interfejs> IKonto </Interfejs>
<Osoba>PBoryczka</Osoba>
<OPT_VER>15.0.1</OPT_VER>
</COM_DOK>*/
/*


	var rApp 
	var rLogin 
	// parametry logowania
	var OPERATOR = 'ADMIN'
	var HASLO    = ''
	var BAZA     = 'DEMO'

	var shell = new ActiveXObject("WScript.Shell")

	var nOObID		= 0
	var nElemIDA	= 0 	
	var nElemIDB	= 0

	try
	{

		WScript.Echo("Rozrachunki na kontach ksi�gowych z walut� systemow�.")


		CreateOptimaApplication()

		nOObID = OkresObrachunkowyID()



		// dodanie dekret�w
		{			
			var rSesja				= rLogin.CreateSession();
			
			// tworzenie obiekt�w kont ksi�gowych
			var rKonta				= rSesja.CreateObject("CDN.Konta")

			// tworze obiekt konta, konto musi by� rozrachunkowe 
			var rKontoRozrachunkowe	= rKonta.Item("Acc_NumerIdx='221.1' and Acc_OObID=" + nOObID)

			// tworze obiekt konta, konto nie mo�e by� w tym wypadku rozrachunkowe
			var rKontoPrzeciwstawne	= rKonta.Item("Acc_NumerIdx='011.0' and Acc_OObID=" + nOObID)


			var rDzienniki			= rSesja.CreateObject("CDN.Dzienniki")
			var rDziennik			= rDzienniki.Item("Dzi_Symbol = 'INNE' and Dzi_OObId="+nOObID)


			var rDekrety			= rSesja.CreateObject("CDN.Dekrety")
			var rDekretA 			= rDekrety.AddNew()		
			var rDekretB 			= rDekrety.AddNew()

			rDekretA.Dokument	= 'PK_A'
			rDekretB.Dokument	= 'PK_B'

			rDekretA.DziID		= rDziennik.ID
			rDekretB.DziID		= rDziennik.ID

			rDekretA.DataDok	= '2008/01/15'
			rDekretB.DataDok	= '2008/10/15'

			rDekretA.Bufor		= 1
			rDekretB.Bufor		= 1
						
			var rElementA		= rDekretA.Elementy.AddNew()
			var rElementB		= rDekretB.Elementy.AddNew()


			// Dla kont rozrachunkowych obiekt automatycznie
			// przypisze property IDekretElement::Rozrachunki{0,1} warto�� 1.
			// Property rozrachunek mo�e mie� warto�� 1 w przypadku gdy
			// ustawione jest konto rozrachunkowe ale tylko po stronie wn lub ma (nigdy jednocze�nie).

			rElementA.KontoWN	= rKontoRozrachunkowe	
			rElementA.KontoMA	= rKontoPrzeciwstawne

			rElementB.KontoWN	= rKontoPrzeciwstawne
			rElementB.KontoMA	= rKontoRozrachunkowe

			rElementA.Kwota		= 10000.00
			rElementB.Kwota		= 5000.00

			rSesja.Save()

			nElemIDA	= rElementA.ID
			nElemIDB	= rElementB.ID

		}


		// wczytanie wy�ej dodanych dekret�w oraz ich rozliczenie
		{
			var rSesja = rLogin.CreateSession();

			var rElementy		= rSesja.CreateObject("CDN.DekretElementy")
			var rElementA		= rElementy.Item("DeE_DeEID=" + nElemIDA)
			var rElementB		= rElementy.Item("DeE_DeEID=" + nElemIDB)

			// popranie statusu rozrachunk�w
			// IDekretElement::StatusRozrachunku {0,1,2,3}
			//	0 - nie podlega; 
			//	1 - nie rozliczone; 
			//	2 - cz�ciowo rozliczone; 
			//	3 - ca�kowicie rozliczone

			var nStatusA			= rElementA.StatusRozrachunku
			var nStatusB			= rElementB.StatusRozrachunku

			var nStronaRozrachunkuA	= rElementA.StronaRozrachunku
			var nStronaRozrachunkuB	= rElementB.StronaRozrachunku

			// 0 - nieokre�lona, nie zaznaczono rozrachunku 
			// 1 - Strona Wn
			// 2 - Strona Ma
	
			if (((nStatusA == 1 || nStatusA == 2) && (nStatusB == 1 || nStatusB == 2)) 
			&& nStronaRozrachunkuA != nStronaRozrachunkuB) {

				// Rozrachunek elementu dekretu A rozliczymy rozrachunkiem elementu dekretu B
				var nKRoID = SingleSQLResult(rSesja, "Select KRo_KRoID from CDN.KsiRozrachunki where KRo_RozliczenieID is NULL and KRo_DeEID="+nElemIDB ) 

				if (nKRoID != 0) {

					rElementA.StartRozrachunkiManager()
					var rMenagerRozrachunkow = rElementA.RozrachunkiManager()

					rMenagerRozrachunkow.RozliczRozrachunek( rElementA, nKRoID )

					rSesja.Save()
				}

			}

		}

		// usuwanie/rozpinanie rozlicze�
		/*
		{
			var rSesja = rLogin.CreateSession();

			var rElementy		= rSesja.CreateObject("CDN.DekretElementy")
			var rElementA		= rElementy.Item("DeE_DeEID=" + nElemIDA)


			var rRozrachunki	= rElementA.KSIRozrachunki
			var nCount			= rRozrachunki.Count

			for (var n = nCount; n != 0; --n) {
				rRozrachunek = rRozrachunki.Item(n-1)
				rElementA.UsunRozrachunek(rRozrachunek)	
			}
			rSesja.Save()
		}
		*/

		WScript.Echo("Koniec przyk�adu: Rozrachunki na kontach ksi�gowych z walut� systemow�.")
	}
	catch(e)
	{
		var blad = e.message;
		WScript.Echo(blad)
	}
	rApp.UnlockApp()



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function CreateOptimaApplication()
{
	rApp = new ActiveXObject("CDNBASE.Application");
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function OkresObrachunkowyID()
{	
	var pKonfiguracja	= rLogin.CreateObject('CdnKonfig.Konfiguracja');
	var nResult			= pKonfiguracja.Value("BiezOkresObr");
	return nResult;
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function ExecuteSQL(ASession, ASQL) {

	var connection		= ASession.Connection
	var rs				= new ActiveXObject("ADODB.Recordset");
	rs.Open(ASQL, connection)

return rs
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function SingleSQLResult(ASession, ASQL) {

	var rs = ExecuteSQL(ASession, ASQL)

	var Result = ''
	if (!rs.EOF)
		Result = rs.Fields(0).Value

	rs.Close()

return Result
}
