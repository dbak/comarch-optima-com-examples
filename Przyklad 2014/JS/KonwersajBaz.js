///////////////////////////////////////////////////////////////////////
//
//    Skrypt wykonuje seryjn� konwersj� baz
//
//    v 10.0 [MB]

/*<COM_DOK>
<OPIS>
   Seryjn� konwersj� baz firmowych
   
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IKontrahent </<Interfejs>
<Interfejs> IFirma </<Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

var KonwertUser ='SA' 
var KonwertPass =''

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )
	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	KonwersjaBaz ()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}


function KonwersjaBaz ()
{
var rFirma 




////////////////////////////////////////////////////////////////////
//    Lista baz jest kolekcj� obiektu Application

WScript.Echo ( "Ilosc Baz " + rApp.Count )

//
//    Iteracja listy baz:
//
for ( var a=0; a < rApp.Count; a++ )
{
	rFirma	= rApp.Item(a)
	WScript.Echo ( rFirma.Name )


// Uruchomienie konwersji - kopia bazy wykona sie automarycznie
try {
	rFirma.User=KonwertUser
	rFirma.Password=KonwertPass
	rFirma.Convert()
    }   catch (e) 
	{ WScript.Echo (e.description) }

}

}





