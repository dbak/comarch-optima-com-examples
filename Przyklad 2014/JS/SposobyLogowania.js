//
//            Rozne metody tworzenia obiektu logowania


/*<COM_DOK>
<OPIS>
	Rozne metody tworzenia obiektu logowania dla logowania przez skrypt
</OPIS>
<Uruchomienie>Rozne metody tworzenia obiektu logowania</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

// Standardowe utworzenie Aplikacji 
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

var e			// Wyjatek


// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 




////////////////////////////////////////////////////////////////////
//
//            Metody tworzenia obiektu logowania 
//            W zale�no�ci od parametru uruchomienia funkcji LockApp
//            Parametr to suma bitowa warto�� flag:
//	ApplicationLockFlags  
//		  alFastInit = 0, 
//		  alFullInit = 1, 
//		  alSplash = 2, 
//		  alLoginDialog = 4, // NIE OBECNY TRYB W Wersji 2010
//		  alDetalWorkMode = 256, 
//		  alCheckShowLoginDialogoption = 512, 
//		  alAll = 0xffff 
//
//            Dla kompilowalnych aplikacji zewnetrznych zalecane 
//            jest sotowanie logowania z za��czony� mask� logowania detal.
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
//Z podaniem operatora i firmy:
	//Utworzenie obiektu loginu

	rLogin	= rApp.LockApp(1)	

var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

	// Logowanie z podanim bazy, modu�u pobiere z karty operatora

	rLogin=   rApp.Login(OPERATOR,HASLO,BAZA)

	// Z podaniem modu��w 
	//rLogin=   rApp.Login(OPERATOR,HASLO,BAZA,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0)


////////////////////////////////////////////////////////////////////
// Z wywo�aniem dialogu logowania

// UWAGA Spos�b logowania nie obecny w wersji 2010
/*
	//Utworzenie obiektu loginu
	rLogin	= rApp.LockApp(255)
*/


////////////////////////////////////////////////////////////////////


	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
try {
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {

	WScript.Echo("Zalogowany")
	Zamknij()
	WScript.Echo("KONIEC")
	}


///////////////////////////////////////////////////////////////////////
function Zamknij() {
	try {
	//Zamykam po��czenie do bazy danych
		rApp.UnLockApp()
	}

	catch (e) {}
	}
///////////////////////////////////////////////////////////////////////


