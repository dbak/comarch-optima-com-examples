

/*<COM_DOK>
<OPIS>
   Otwarcie lub utworzenie nowego raportu kasowo bankowego (KB) 
</OPIS>
<Uruchomienie> W skrypcie podana jest data dla jakiej raport kasowy ma zosta� otowrzony. 
W razie braku raportu z dan� dat� skrypt utowrzy nowy raport z datami otwarcia i zamkni�cia r�wnymi tej dacie </Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IRaportKB </Interfejs> 
<Interfejs> IRachunek </Interfejs> 


<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

var sqlConn	= new ActiveXObject('ADODB.Connection')
var sqlRs  	= new ActiveXObject ('ADODB.Recordset')



try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {

sqlConn = rSesja.Connection


        RaportyKB()
	rApp.UnLockApp()

	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function RaportyKB()
{

var RachynekID = 1
var DataDok = "2010-09-30"
// Znajdz raoprt
try{
	rRaportKB = rSesja.CreateObject("CDN.RaportyKB").Item("BRp_BRaID ="+ RachynekID + " and BRp_DataDok <= convert(datetime,'"+DataDok +"',120) and BRp_DataZam >= convert(datetime,'"+DataDok + "',120) and BRp_Zamkniety = 0")
	
}catch(e)
{
	WScript.Echo("B��d inicjalizacji programu " + e.description)

	// Wyst�pi� b��d np niema takiego raportu nale�y doda� nowy.
	var rRaportKB = rSesja.CreateObject("CDN.RaportyKB").AddNew()

	// Ustalenie Rejestr KB dla raportu
	var rRejestr = rSesja.CreateObject("CDN.Rachunki").Item("BRa_BRaID =" + RachynekID )
	rRaportKB.Rachunek = rRejestr

	rRaportKB.DataOtw=DataDok
	rRaportKB.DataZam=DataDok
	

	rSesja.Save()
}

}