
/*<COM_DOK>
<OPIS>
  Skrypt wykonuje wydruk faktury dla dokumentu podanego przez filtr
  
  
  
</OPIS>
<Uruchomienie>
Skrypt musi by� wywo�any w katalogu g��wnym Optimy 
przez przegrany w t� lokalizacj� program WScript.exe lub CScript.exe 
(oryginalnie lokalizacja pliku to np. c:Windows\System32\ )

POdanie filtra do wydruku i podanie wydruku jaki ma by� drukowany
</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> WydrFormat</Interfejs>
<Interfejs> ZmiennaDyn</Interfejs>

<Osoba></Osoba>
<OPT_VER>2014</OPT_VER>
</COM_DOK>*/


var app = WScript.CreateObject("CDNBase.Application");

var Login = app.LockApp(1)

var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'PREZENTACJA_KH'

        // Logowanie z podanim bazy, modu�u pobiere z karty operatora

Login = app.Login(OPERATOR, HASLO, BAZA)

var WyswietlajKomunikaty = 1;


/////////////////////////////////////////////////////////////////////////
/*
  Kod g��wny.
  Wydruk FS
*/

try
{

  WyswietlajKomunikaty = 0

////////////////////////////////////////////////////////////////////
//Ustawmy id kontekstu wywo�ywanego wydruku:

var zrodloID = 0
var wydrukID  = 372  //GenRap
var Format = NowyWydruk(zrodloID, wydrukID);

////////////////////////////////////////////////////////////////////
//Ustawmy jeszcze nast�puj�ce parametry wydruku:

/////////////////////////////////////////////////////////////////////////
//Utworzenie tablicy zmiennych dynamicznych przekazywanych 'z aplikacji'
  var ZmienneDyn = new ActiveXObject("CDNLib.Dictionary");
  var ZmiennaDyn = new ActiveXObject("CDN.ZmiennaDyn");
  ZmiennaDyn.Nazwa   = 'IDENTYFIKATOR'
  ZmiennaDyn.Wartosc = 92; //ID faktury
  ZmienneDyn.item(ZmiennaDyn.Nazwa) = ZmiennaDyn;

  Format.FiltrTPS   = '((TrN_TypDokumentu = 302))'
  Format.FiltrSQL   = 'TrN_TrNID = 92'

  Format.Urzadzenie = 1 //Ekran

  //Format.Urzadzenie = 2 //Drukarka domy�lna
  //Format.Urzadzenie = 3 //Drukarka inna
  //Format.Drukarka   = '\\\\eol\\HP DeskJet 600'
  //Format.IloscKopii = 1
  //Format.MarginesL = 500
  //Format.MarginesG = 500

  //Mo�na ju� wykona� wydruk zgodnie z ustawieniami.
  Format.Wykonaj(ZmienneDyn)

  ms('OK, to by by�o na tyle.');
}
/*
  Obs�uga wyj�tk�w.
*/
catch (e)
{
  dm('Wyj�tek ' + e.number);
  dm('Wyj�tek ' + e.description);
}


/*
  Wylogowanie.
*/

app.UnLockApp();


function NowyWM(_ProcID, _KontekstID)
{
  //Utworzenie obiektu
  var WM = new ActiveXObject("CDN.WydrMgr");

  //Inicjalizacja
  WM.ProcID     = _ProcID;
  WM.KontekstID = _KontekstID;

  return WM;
}

/*
  Funkcja tworz�ca, inicjalizuj�ca i zwracaj�ca obiekt formatu wydruku na podstawie podanego wywo�ania.
*/
function NowyWydruk(_ZrodloID, _WydrukID)
{
  var wydruk = new ActiveXObject("CDN.WydrFormat");
  wydruk.GenRapReportType = 2
  wydruk.GenRapNewReportInitObjects = "FakturaSpr"
  //Inicjalizacja
  wydruk.ZrodloID = _ZrodloID;
  //wydruk.WydrukID = _WydrukID;
  wydruk.ID =  _WydrukID;

  return wydruk;
}


/*
  Funkcje pomocnicze.
*/

function ms(_Text)
{
  if(WyswietlajKomunikaty)
    WScript.Echo(_Text);
}

function dm(_Text)
{
  WScript.Echo(_Text)
}
