// To jest logowanie z wywo�aniem okna logowania
// Na kt�rym wybiera si� operatora i baz�


/*<COM_DOK>
<OPIS>
	Dodawanie zapis�w w ewidencji dodatkowej.
</OPIS>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IEwidencjaDodatkowa </Interfejs>
<Interfejs> IKontrahent </Interfejs>
<Interfejs> IKwotaDodatkowa </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

var rApp		
var rLogin		
var rSesja		

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

rApp	= new ActiveXObject("CDNBASE.Application") 


rApp.LockApp(1)	
rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )


rSesja	= rLogin.CreateSession()

//najpierw sprawdz� istnienie kontrahenta
var rKontrahent = SprawdzKontrahenta ("AA1234")

// TU DODAJ� ZAPIS W REJESTRZE:

var rEwidDod = rSesja.CreateObject("CDN.EwidencjeDodatkowe").AddNew()

rEwidDod.NumerObcy	= "RW 123"
rEwidDod.Rejestr	= "KOSZTY"
rEwidDod.Typ		= 102 //101-przychody, 102-koszty
rEwidDod.Platnosci	= 0 //je�li Platnosci=1, to generowane s� p��tno�ci, trzeba okre�li� form� p�atno�ci itp.
rEwidDod.DataZap	= "2004/09/21"


rEwidDod.Podmiot	= rKontrahent
rEwidDod.KwotaRazem	= 541.12

var rKwotaDod
//Tu dodaj� kwoty dodatkowe - a wi�c pozycje, kt�re potem mog� by� wykorzystane przy ksi�gowaniu
rKwotaDod		= rEwidDod.KwotyDodatkowe.AddNew()
rKwotaDod.Segment1	= "401-1"
rKwotaDod.Segment2	= "301-1"
rKwotaDod.Kwota		= 34.56

rKwotaDod		= rEwidDod.KwotyDodatkowe.AddNew()
rKwotaDod.Segment1	= "401-2"
rKwotaDod.Segment2	= "301-2"
rKwotaDod.Kwota		= 98.21


rSesja.Save()

rApp.UnlockApp() // na zako�czenie trzeba zrobi� UnlockApp() - inaczej GPF

new ActiveXObject("WScript.Shell").Popup("KONIEC")


function SprawdzKontrahenta (_kod) {
 var rKnt
 try {
  rKnt = rSesja.CreateObject("CDN.Kontrahenci").Item("Knt_Kod='" + _kod + "'")
 }
 catch(e) {
  // jak nie ma to dodaj�
  rKnt = rSesja.CreateObject("CDN.Kontrahenci").AddNew()
  rKnt.Akronim = _kod
  rSesja.Save()
 }
 return rKnt
}