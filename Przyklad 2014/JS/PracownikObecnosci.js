//
// Skrypt prezuentuj�cy prac� na kalendarzu pracownika.
// Dodawane s� i odczytwyane dni w kalendarzu prac.
//

/*<COM_DOK>
<OPIS>
	Skrypt prezuentujacy prac� na kalendarzu pracownika.
	Dodawane s� i odczytwyane dni w kalendarzu prac.
</OPIS>
<Uruchomienie> Operacje wykonywane s� dla pracownika o kodzie TEST </Uruchomienie>
<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IPracownik </Interfejs> 
<Interfejs> ITypNieobecnosci </Interfejs>
<Interfejs> ICzasPracy </Interfejs> 
<Interfejs> INieobecnosc </Interfejs> 
<Interfejs> IDzien </Interfejs> 

<Osoba>MBednarek</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


// Na pocz�tku deklaruj� zmienne, kt�re b�d� wykorzystywane "globalnie"
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rFaktura	// Obiekt faktury
var rPozycje	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	Nieobecnosci()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function Nieobecnosci() {
try{

// utworzenie obiektu Pracownik o kodzie TEST

var rPracownik = rSesja.CreateObject("CDN.Pracownicy").Item("PRA_KOD = 'TEST'")
WScript.Echo( rPracownik.Kod )
}
catch (e) {
	WScript.Echo("Wyst�pi� b��d " + e.description)
	}

try{
// Odczytanie godziny pierwszego wejscia w kalendarzu na dzien:
var rCzasPracy = rPracownik.CzasPracy
var rDzien = rCzasPracy.Dzien('2010-03-08')

WScript.Echo(rDzien.OdGodziny_1)

// Ustawienie nowej godziny wejscia na dzie�:
var rNowyDzien = rCzasPracy.AddNew('2010-03-09')
rNowyDzien.OdGodziny_1 = '8:12'
rNowyDzien.DoGodziny_1 = '10:23'

rSesja.Save()


// Zapisanie obiektow 
rSesja.Save()
}
catch (e) {
	WScript.Echo("Wyst�pi� b��d podczas dodawanie Nieobecnosci" + e.description)
	}
}
