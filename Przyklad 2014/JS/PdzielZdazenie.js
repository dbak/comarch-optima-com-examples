////////////////////////////////////////////////////
// 
//  V 10.0 [MB]
//

/*<COM_DOK>
<OPIS>
   Podzia� zdarzenia kasowo-bankowego (zdarzenia KB)
</OPIS>
<Uruchomienie> W kodzie skryptu nale�y poda� ID dzielonego zdarzenia KB i formy p�atno�ci dla nowych zdarze�</Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IZdarzenieKB </Interfejs> 
<Interfejs> IFormaPlatnosci </Interfejs>
<Interfejs> SplitZdarzenieKB </Interfejs> 

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/




// Standardowe utworzenie Aplikacji i dokumentu
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji
var rZapis 		// Obiekt zapisu kasowego
var rRejestr		// Obiekt rejestru kasowego
var rRaport		// Obiekt raportu kasowego

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'


try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	PodzielZdazenieKB()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function PodzielZdazenieKB() {
try {
	var rZdazenie  = rSesja.CreateObject("CDN.ZdarzeniaKB").Item("BZd_BZdID=16")   
	var rFormaPlat = rSesja.CreateObject("CDN.FormYPlatnosci").Item("FPl_FPlID=3")   
	var rPodzial   = rSesja.CreateObject("CDN.SplitZdarzeniaKB")

	var Kwota = 10.23 
	var Termin = '2006-09-25'

	rPodzial.Wykonaj(rZdazenie, Kwota  , Termin , rFormaPlat) 
	rSesja.Save()

	}
catch (e) {
	WScript.Echo("Wyst�pi� b��d " + e.description)
	}

}
