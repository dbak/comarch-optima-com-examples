//--------------------------------------------------------------------------------------------
// Korekta dokumentu FA
//

/*<COM_DOK>
<OPIS>
   Utworzenie korekt do dokumentu FA (warto�ci, ilo�ci, vat )
</OPIS>
<Uruchomienie> W kodzie skryptu nale�y poda� ID korygowanego dokumentu FA</Uruchomienie>
<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IDokumentHaMag </Interfejs> 
<Interfejs> IElementHaMag </Interfejs>
<Interfejs> IDefinicjaDokumentu </Interfejs> 

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/


var rApp  // Obiekt aplikacji
var rLogin  // Obiekt loginu
var rSesja  // Obiekt sesji
var rFaktura // Obiekt faktury
var rPozycje // Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO' 

try {
 // Utworzenie obiektu aplikacji:
 rApp  = new ActiveXObject("CDNBASE.Application") 
 //Utworzenie obiektu loginu:
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

 // Wyci�gni�cie sesji z loginu:
 rSesja = rLogin.CreateSession()
 }
catch (e) {
 WScript.Echo("B��d inicjalizacji programu " + e.description)
 }
 
if (!rLogin) {
 WScript.Echo("Nie zalogowano si� poprawnie do programu")
 }
else {
 DodajKorekteFakture()
 rApp.UnLockApp()
 WScript.Echo("KONIEC")
 }
 
//-----------------------------------------
function DodajKorekteFakture() {
try {
 
 rFaktura = rSesja.CreateObject("CDN.DokumentyHaMag").AddNew()
 
// FAKI 302001; 
// FAKW 302002; 
// FAKV 302003;

 rFaktura.Rodzaj  = 302001
 rFaktura.TypDokumentu = 302
 
 var rNumerator = rFaktura.Numerator
 var rDokDef = rSesja.CreateObject("CDN.DefinicjeDokumentow").Item("DDf_Symbol='FKOR'")
 rNumerator.DefinicjaDokumentu = rDokDef
 
    rFaktura.Korekta          = 1
    rFaktura.Bufor            = 1
    rFaktura.OryginalID       = 27  // ID korygowanego dokumentu


for(var i=0; i< rFaktura.Elementy.Count; i++)
{
 var element = rFaktura.Elementy.Item(i)
 WScript.Echo( element.Towar.Akronim )
}

rSesja.Save()


 }
catch (e) {
 WScript.Echo("Wyst�pi� b��d podczas wystawiania faktury " + e.description)
 }
}