

/*<COM_DOK>
<OPIS>
	Przekszta�cenie paragonu do faktury
</OPIS>
<Uruchomienie>W kodzie skryptu nale�y poda� ID paragonu kojarzonego z generowan� faktur� </Uruchomienie>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IDokumentHaMag </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2014</OPT_VER>
</COM_DOK>*/

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

var app = WScript.CreateObject("CDNBase.Application");
try
{
    app.LockApp(1);
    login=   app.Login( OPERATOR,HASLO,BAZA )
    WScript.Echo("Zalogowany do bazy:"+login.Firm.Name )
    var session            = login.CreateSession();
    var coll               = session.CreateObject("CDN.DokumentyHaMag");
    var HaMag              = coll.AddNew();

    HaMag.FaID             = 45		// ID paragoun skojazonego na podstawie kt�rego genruje sie FA
    HaMag.Rodzaj           = 302006
    HaMag.TypDokumentu     = 302
    HaMag.FaIDLink         = 45 
    HaMag.DataDok          = '2004-05-27'
    HaMag.DataSprzedazy    = '2004-05-27'
    HaMag.FaIDLink         = 45		// ID paragoun skojazonego na podstawie kt�rego genruje sie FA

    HaMag.BlokadaPlatnosci = 1
	HaMag.GenerujWZ	=-1
    HaMag.Bufor            = 1
    var Elementy           = HaMag.Elementy

    session.Save();
    WScript.Echo('Utworzono');

}
catch (e)
{
    WScript.Echo('Wyj�tek ' + e.description);
}

app.unlockApp()