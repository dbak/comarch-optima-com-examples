//////////////////////////////////////////
// Rozliczenie dwuch dokument�w KB
// "BZp_BZpID=4" oraz "BZd_BZdID=19"



/*<COM_DOK>
<OPIS>
	Rozliczenie dwuch dokument�w KB
</OPIS>
<Uruchomienie>ID rozliczanych dokument�w zdarzenia i zapisu kasowo bankowego  </Uruchomienie>

<Interfejs> IApplication </Interfejs>
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IKontrahent </Interfejs> 
<Interfejs> IRozliczenieKB</Interfejs> 
<Interfejs> IZapisKB</Interfejs> 
<Interfejs> IZdarzenieKB</Interfejs> 

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

// Standardowe utworzenie Aplikacji i dokumentu
var rApp		// Obiekt aplikacji
var rLogin		// Obiekt loginu
var rSesja		// Obiekt sesji

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

try {
	// Utworzenie obiektu aplikacji
	rApp		= new ActiveXObject("CDNBASE.Application") 

	//Utworzenie obiektu loginu
	rApp.LockApp(1)	
	rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )

	// Wyci�gni�cie sesji z loginu
	rSesja = rLogin.CreateSession()
	}
catch (e) {
	WScript.Echo("B��d inicjalizacji programu " + e.description)
	}

if (!rLogin) {
	WScript.Echo("Nie zalogowano si� poprawnie do programu")
	}
else {
	//	RozliczZap_KB ()
	RozliczZap_KB ()
	rApp.UnLockApp()
	WScript.Echo("KONIEC")
	}

//-----------------------------------------
function RozliczZap_KB ()
{
try {

var rRozliczenie = rSesja.CreateObject("CDN.RozliczeniaKB").AddNew()


 var rZapis     = rSesja.CreateObject("CDN.ZapisyKB").Item("BZp_BZpID=4")
 var rZdarzenie = rSesja.CreateObject("CDN.ZdarzeniaKB").Item("BZd_BZdID=19")

rRozliczenie.UstawDokumentyKB(rZapis,rZdarzenie,1)

rSesja.Save()

     }
	catch (e) {WScript.Echo("Rozliczenie " + e.description)}

}

//-----------------------------------------
