//--------------------------------------------------------------------------------------------
// W wybranej bazie musz� istnie� dokumenty dla kontrahenta o kodzie LAS


/*<COM_DOK>
<OPIS>
	Generacja ponagle� 
	Generacja not odsetkowych
	Generacja potwierdze� salda
</OPIS>
<Uruchomienie> W wybranej bazie musz� istnie� dokumenty dla kontrahenta o kodzie LAS </Uruchomienie>
<Interfejs> IApplication </Interfejs> 
<Interfejs> ILogin </Interfejs>
<Interfejs> IAdoSession </Interfejs>
<Interfejs> IGenWsadOds </Interfejs>

<Osoba>MB</Osoba>
<OPT_VER>2010</OPT_VER>
</COM_DOK>*/

var rApp  	// Obiekt aplikacji
var rLogin  	// Obiekt loginu
var rSesja  	// Obiekt sesji
var rFaktura 	// Obiekt faktury
var rPozycje 	// Kolekcja element�w faktury

// parametry logowania
var OPERATOR = 'ADMIN'
var HASLO    = ''
var BAZA     = 'DEMO'

 
try {
 // Utworzenie obiektu aplikacji:
 rApp  = new ActiveXObject("CDNBASE.Application") 
 //Utworzenie obiektu loginu:
rApp.LockApp(1)	
rLogin=   rApp.Login( OPERATOR,HASLO,BAZA )
WScript.Echo("Zalogowany do bazy:"+rLogin.Firm.Name )
 // Wyci�gni�cie sesji z loginu:
 rSesja = rLogin.CreateSession()
 }
catch (e) {
 WScript.Echo("B��d inicjalizacji programu " + e.description)
 }
 
if (!rLogin) {
 WScript.Echo("Nie zalogowano si� poprawnie do programu")
 }
else {
 GenerujPonaglenia()
 rApp.UnLockApp()
 WScript.Echo("KONIEC")
 }
 
//-----------------------------------------
function GenerujPonaglenia() {

 

 rGenerator = rSesja.CreateObject("CDN.GenWsadOds")

// Ustalenie dla jakiego podmiotu maj� by� generowane dokumenty
 rGenerator.PodmiotTyp =1
 rGenerator.FiltrSQL = "Knt_kod like 'LAS'"

// Rodzaj generowanych dokument�w:

rGenerator.DokumentTyp = 223 //Potwierdzenie Salda
//rGenerator.DokumentTyp = 221 //Noty odsetkowe 
//rGenerator.DokumentTyp = 222 //Ponaglenie zap�aty


// Generacja dokument�w
 rGenerator.Generuj()

 rSesja.Save()
try {
 }
catch (e) {
 WScript.Echo("Wyst�pi� b��d podczas generacji " + e.description)
 }
 
}


